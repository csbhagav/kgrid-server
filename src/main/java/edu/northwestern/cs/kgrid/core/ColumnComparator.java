package edu.northwestern.cs.kgrid.core;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;

import weka.core.Instance;
import weka.core.Instances;
import edu.northwestern.cs.kgrid.ml.LogRegModel;
import edu.northwestern.cs.kgrid.server.models.AddedColumn;
import edu.northwestern.cs.kgrid.server.utils.KGridModelUtils;
import edu.northwestern.cs.kgrid.server.utils.KGridUtils;

public class ColumnComparator implements Comparator<AddedColumn> {

	// Double matchPercWt = 0.5;
	//
	// Double srcDistinctValWt = 1.0;
	// Double srcAvgLenWt = 0.02;
	// Double srcIsNumWt = 0.5;
	// Double srcRowsWt = 0.1;
	//
	// Double tgtDistinctValWt = 1.0;
	// Double tgtAvgLenWt = 0.02;
	// Double tgtIsNumWt = 0.5;
	// Double tgtRowsWt = 0.1;
	//
	// Double inLinkWt = 2.0;
	// Double outLinkWt = 2.0;
	//
	// Double srRelateWt = 2.0;

	Double matchPercWt = 0.5;

	Double srcDistinctValWt = 0.5;
	Double srcAvgLenWt = 0.5;
	Double srcIsNumWt = 0.5;
	Double srcRowsWt = 0.5;

	Double tgtDistinctValWt = 0.5;
	Double tgtAvgLenWt = 0.5;
	Double tgtIsNumWt = 0.5;
	Double tgtRowsWt = 0.5;

	Double inLinkWt = 0.5;
	Double outLinkWt = 0.5;

	Double srRelateWt = 0.5;

	KGridUtils kUtils;

	Instances data;

	public ColumnComparator(Instances instances) {
		kUtils = new KGridUtils();
		this.data = instances;
	}

	public int compare(AddedColumn arg0, AddedColumn arg1) {

		try {
			if (score(arg0) > score(arg1))
				return -1;
			else if (score(arg0) < score(arg1))
				return 1;
			else
				return 0;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return 0;

	}

	private Double score(AddedColumn ac) throws SQLException {
		Double score = -1.0;
		try {
			score = LogRegModel.getLogReg().distributionForInstance(
					ac.getInst())[1];
		} catch (Exception e) {
			System.out.println("Failed to classify instances correctly .. ");
			e.printStackTrace();
		}

		ac.setScore(score);
		return score;

	}
}
