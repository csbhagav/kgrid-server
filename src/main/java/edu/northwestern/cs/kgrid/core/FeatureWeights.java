package edu.northwestern.cs.kgrid.core;

public class FeatureWeights {

	private Double matchPercWt = 0.5;

	private Double srcDistinctValWt = 0.5;
	private Double srcAvgLenWt = 0.5;
	private Double srcIsNumWt = 0.5;
	private Double srcRowsWt = 0.5;
	private Double tgtDistinctValWt = 0.5;
	private Double tgtAvgLenWt = 0.5;
	private Double tgtIsNumWt = 0.5;
	private Double tgtRowsWt = 0.5;

	private Double inLinkWt = 0.5;
	private Double outLinkWt = 0.5;

	private Double srRelateWt = 0.5;

	public FeatureWeights(Double matchPercWt, Double srcDistinctValWt,
			Double srcAvgLenWt, Double srcIsNumWt, Double srcRowsWt,
			Double tgtDistinctValWt, Double tgtAvgLenWt, Double tgtIsNumWt,
			Double tgtRowsWt, Double inLinkWt, Double outLinkWt,
			Double srRelateWt) {
		super();
		this.matchPercWt = matchPercWt;
		this.srcDistinctValWt = srcDistinctValWt;
		this.srcAvgLenWt = srcAvgLenWt;
		this.srcIsNumWt = srcIsNumWt;
		this.srcRowsWt = srcRowsWt;
		this.tgtDistinctValWt = tgtDistinctValWt;
		this.tgtAvgLenWt = tgtAvgLenWt;
		this.tgtIsNumWt = tgtIsNumWt;
		this.tgtRowsWt = tgtRowsWt;
		this.inLinkWt = inLinkWt;
		this.outLinkWt = outLinkWt;
		this.srRelateWt = srRelateWt;
	}

	public Double getMatchPercWt() {
		return matchPercWt;
	}

	public void setMatchPercWt(Double matchPercWt) {
		this.matchPercWt = matchPercWt;
	}

	public Double getSrcDistinctValWt() {
		return srcDistinctValWt;
	}

	public void setSrcDistinctValWt(Double srcDistinctValWt) {
		this.srcDistinctValWt = srcDistinctValWt;
	}

	public Double getSrcAvgLenWt() {
		return srcAvgLenWt;
	}

	public void setSrcAvgLenWt(Double srcAvgLenWt) {
		this.srcAvgLenWt = srcAvgLenWt;
	}

	public Double getSrcIsNumWt() {
		return srcIsNumWt;
	}

	public void setSrcIsNumWt(Double srcIsNumWt) {
		this.srcIsNumWt = srcIsNumWt;
	}

	public Double getSrcRowsWt() {
		return srcRowsWt;
	}

	public void setSrcRowsWt(Double srcRowsWt) {
		this.srcRowsWt = srcRowsWt;
	}

	public Double getTgtDistinctValWt() {
		return tgtDistinctValWt;
	}

	public void setTgtDistinctValWt(Double tgtDistinctValWt) {
		this.tgtDistinctValWt = tgtDistinctValWt;
	}

	public Double getTgtAvgLenWt() {
		return tgtAvgLenWt;
	}

	public void setTgtAvgLenWt(Double tgtAvgLenWt) {
		this.tgtAvgLenWt = tgtAvgLenWt;
	}

	public Double getTgtIsNumWt() {
		return tgtIsNumWt;
	}

	public void setTgtIsNumWt(Double tgtIsNumWt) {
		this.tgtIsNumWt = tgtIsNumWt;
	}

	public Double getTgtRowsWt() {
		return tgtRowsWt;
	}

	public void setTgtRowsWt(Double tgtRowsWt) {
		this.tgtRowsWt = tgtRowsWt;
	}

	public Double getInLinkWt() {
		return inLinkWt;
	}

	public void setInLinkWt(Double inLinkWt) {
		this.inLinkWt = inLinkWt;
	}

	public Double getOutLinkWt() {
		return outLinkWt;
	}

	public void setOutLinkWt(Double outLinkWt) {
		this.outLinkWt = outLinkWt;
	}

	public Double getSrRelateWt() {
		return srRelateWt;
	}

	public void setSrRelateWt(Double srRelateWt) {
		this.srRelateWt = srRelateWt;
	}

}
