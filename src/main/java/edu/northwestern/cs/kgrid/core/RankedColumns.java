package edu.northwestern.cs.kgrid.core;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Random;
import java.util.Map.Entry;

import org.codehaus.jettison.json.JSONException;

import weka.core.Instances;
import ciir.umass.edu.learning.DataPoint;
import ciir.umass.edu.learning.RankList;
import ciir.umass.edu.learning.Ranker;
import edu.northwestern.cs.WIG.lib.WeightedRandPerm;
import edu.northwestern.cs.kgrid.DAO.KeyColumnData;
import edu.northwestern.cs.kgrid.lib.KgridColumnUtils;
import edu.northwestern.cs.kgrid.server.models.AddableColumnData;
import edu.northwestern.cs.kgrid.server.models.AddedColumn;
import edu.northwestern.cs.kgrid.server.models.ColumnAttributes;
import edu.northwestern.cs.kgrid.server.models.ColumnChoiceDetails;
import edu.northwestern.cs.kgrid.server.models.ColumnMatchAttributes;
import edu.northwestern.cs.kgrid.server.models.RankingType;
import edu.northwestern.cs.kgrid.server.utils.KGridModelUtils;
import edu.northwestern.cs.kgrid.server.utils.KGridUtils;

public class RankedColumns {

	private final Double CLASSIFIER_THRESHOLD = 0.5;

	public ArrayList<AddedColumn> getRankedListOfAddableColumnsFilteredByPgId(
			Connection streamConn, Connection conn, Integer pgId,
			Integer tableId, Integer mPgId, Instances data, Ranker columnRanker)
			throws SQLException, IOException, JSONException {

		HashMap<Integer, Double> srMeasureMap = KGridUtils.getSRMeasureForPage(
				conn, pgId);
		HashMap<Integer, HashMap<Integer, Double>> sourcePageSRMeasures = new HashMap<Integer, HashMap<Integer, Double>>();
		sourcePageSRMeasures.put(pgId, srMeasureMap);

		HashMap<Integer, ColumnAttributes> originalColumnAttributes = getOriginalColumnAttributes(
				conn, pgId, tableId);
		ArrayList<AddedColumn> addedColumns = getListOfCandidateColumnsFilteredByPage(
				streamConn, conn, pgId, tableId, originalColumnAttributes,
				mPgId);

		HashSet<Integer> tgtPgIds = new HashSet<Integer>(addedColumns.size());
		for (int i = 0; i < addedColumns.size(); i++) {
			tgtPgIds.add(addedColumns.get(i).getTgtCol().getPgId());
		}

		HashMap<Integer, Integer> idsMap = KGridUtils.kgridIdToWikiId(conn,
				tgtPgIds);

		for (int i = 0; i < addedColumns.size(); i++) {
			AddedColumn ac = addedColumns.get(i);

			ac.setOrigPgTargetPgSRVal(srMeasureMap.get(idsMap.get(ac
					.getTgtCol().getPgId())));
			ac.setInst(KGridModelUtils.convertAddedColumnToInstance(ac, data));

		}

		Collections.sort(addedColumns, new ColumnComparator(data));

		// ArrayList<AddedColumn> filteredColumnsList = columnsClassifierFilter(
		// addedColumns, data);
		addedColumns = rankFilteredColumns(addedColumns, columnRanker, data);

		return addedColumns;
	}

	public ArrayList<AddedColumn> getRankedListOfAddableColumnsFilteredByKeyCol(
			Connection streamConn, Connection conn, Connection graphConn,
			Integer pgId, Integer tableId, Integer keyColId, Instances data,
			Ranker columnRanker) throws SQLException, IOException,
			JSONException {

		HashMap<Integer, Double> srMeasureMap = KGridUtils.getSRMeasureForPage(
				conn, pgId);
		HashMap<Integer, HashMap<Integer, Double>> sourcePageSRMeasures = new HashMap<Integer, HashMap<Integer, Double>>();
		sourcePageSRMeasures.put(pgId, srMeasureMap);

		HashMap<Integer, ColumnAttributes> originalColumnAttributes = getOriginalColumnAttributes(
				conn, pgId, tableId);
		ArrayList<AddedColumn> addedColumns = getListOfCandidateColumnsFilteredByKeyCol(
				streamConn, conn, pgId, tableId, originalColumnAttributes,
				keyColId);

		HashSet<Integer> tgtPgIds = new HashSet<Integer>(addedColumns.size());
		for (int i = 0; i < addedColumns.size(); i++) {
			tgtPgIds.add(addedColumns.get(i).getTgtCol().getPgId());
			
		}

		HashMap<Integer, Integer> idsMap = KGridUtils.kgridIdToWikiId(conn,
				tgtPgIds);

		for (int i = 0; i < addedColumns.size(); i++) {
			AddedColumn ac = addedColumns.get(i);

			ac.setOrigPgTargetPgSRVal(srMeasureMap.get(idsMap.get(ac
					.getTgtCol().getPgId())));
			ac.setInst(KGridModelUtils.convertAddedColumnToInstance(ac, data));


		}

		Collections.sort(addedColumns, new ColumnComparator(data));

		ArrayList<AddedColumn> filteredColumnsList = columnsClassifierFilter(
				addedColumns, data);
		addedColumns.clear();
		addedColumns = rankFilteredColumns(filteredColumnsList, columnRanker,
				data);

		return addedColumns;
	}

	public ArrayList<AddedColumn> getRankedListOfAddableColumnsAll(
			Connection streamConn, Connection conn, Integer pgId,
			Integer tableId, Instances data, Ranker columnRanker,
			RankingType type) throws SQLException, IOException, JSONException {

		HashMap<Integer, Double> srMeasureMap = KGridUtils.getSRMeasureForPage(
				conn, pgId);
		HashMap<Integer, HashMap<Integer, Double>> sourcePageSRMeasures = new HashMap<Integer, HashMap<Integer, Double>>();
		sourcePageSRMeasures.put(pgId, srMeasureMap);

		HashMap<Integer, ColumnAttributes> originalColumnAttributes = getOriginalColumnAttributes(
				conn, pgId, tableId);
		ArrayList<AddedColumn> addedColumns = getListOfCandidateColumnsAll(
				streamConn, conn, pgId, tableId, originalColumnAttributes, type);

		HashSet<Integer> tgtPgIds = new HashSet<Integer>(addedColumns.size());
		for (int i = 0; i < addedColumns.size(); i++) {
			tgtPgIds.add(addedColumns.get(i).getTgtCol().getPgId());
		}

		HashMap<Integer, Integer> idsMap = KGridUtils.kgridIdToWikiId(conn,
				tgtPgIds);

		for (int i = 0; i < addedColumns.size(); i++) {
			AddedColumn ac = addedColumns.get(i);

			ac.setOrigPgTargetPgSRVal(srMeasureMap.get(idsMap.get(ac
					.getTgtCol().getPgId())));

			ac.setInst(KGridModelUtils.convertAddedColumnToInstance(ac, data));

		}

		if (type == RankingType.RANDOM) {
			Collections.shuffle(addedColumns);

		} else if (type == RankingType.MATCH_PERC) {

		} else if (type == RankingType.CLSFR_AND_RANKER) {
			Collections.sort(addedColumns, new ColumnComparator(data));
			//
			// System.out.println("Column features :\n\n\n");
			// for (int i = 0; i < addedColumns.size(); i++) {
			// System.out.println("idx:" + i + "\t"
			// + addedColumns.get(i).getFeatureString() + "\t"
			// + addedColumns.get(i).getScore());
			// }
			//
			// for (int i = 0; i < addedColumns.size(); i++) {
			// AddedColumn ac = addedColumns.get(i);
			// System.out.println("idx:" + i + "\t"
			// + ac.getOriginalCol().getPgId() + "-"
			// + ac.getOriginalCol().getTableId() + "-"
			// + ac.getOriginalCol().getColIdx() + "-"
			// + ac.getSrcCol().getPgId() + "-"
			// + ac.getSrcCol().getTableId() + "-"
			// + ac.getSrcCol().getColIdx() + "-"
			// + ac.getTgtCol().getColIdx());
			// }

			System.out.println("\n\n\n **** Column features : ****\n\n\n");

			ArrayList<AddedColumn> filteredColumnsList = columnsClassifierFilter(
					addedColumns, data);
			addedColumns.clear();
			addedColumns = rankFilteredColumns(filteredColumnsList,
					columnRanker, data);
		}

		return addedColumns;
	}

	private ArrayList<AddedColumn> columnsClassifierFilter(
			ArrayList<AddedColumn> addedColumns, Instances data)
			throws SQLException {

		ArrayList<AddedColumn> filteredList = new ArrayList<AddedColumn>();
		for (int i = 0; i < addedColumns.size(); i++) {
			if (addedColumns.get(i).getScore() >= CLASSIFIER_THRESHOLD)
				filteredList.add(addedColumns.get(i));
			else {
				break;
			}

		}

		// for (int i = 0; i < addedColumns.size() * CLASSIFIER_THRESHOLD; i++)
		// {
		// filteredList.add(addedColumns.get(i));
		//
		// }

		return filteredList;
	}

	private ArrayList<AddedColumn> rankFilteredColumns(
			ArrayList<AddedColumn> columnsList, Ranker columnRanker,
			Instances data) {
		String fStr = "";
		int fNum = 0;
		int size = data.numAttributes();
		ArrayList<AddedColumn> finalList = new ArrayList<AddedColumn>(
				columnsList.size());
		RankList rl = new RankList();
		for (int i = 0; i < columnsList.size(); i++) {

			String[] features = KGridModelUtils.dataToFeatureValues(
					columnsList.get(i), data);
			features[size - 1] = "0";

			fStr = features[size - 1] + " " + "qid:"
					+ columnsList.get(i).getOriginalCol().getTid() + " ";
			for (int j = 0; j < features.length - 1; j++) {
				fNum = j + 1;
				fStr += fNum + ":" + features[j] + " ";
			}
			fStr += "#idx:" + i;
			rl.add(new DataPoint(fStr));
		}
		if (rl.size() > 0) {
			RankList rankedRl = columnRanker.rank(rl);
			int id = 0;
			System.out.println("Ranked IDs");
			for (int i = 0; i < rankedRl.size(); i++) {
				id = Integer
						.valueOf(rankedRl.get(i).toString().split("#idx:")[1]);
				finalList.add(columnsList.get(id));

				System.out.println(id);
			}
		}
		return finalList;
	}

	private ArrayList<AddedColumn> getListOfCandidateColumnsFilteredByPage(
			Connection streamConn, Connection conn, Integer pgId,
			Integer tableId,
			HashMap<Integer, ColumnAttributes> originalColumnAttributes,
			Integer mPgId) throws SQLException, UnsupportedEncodingException {

		String matchedColumnSelectQ = "SELECT DISTINCT cm.src_col_idx as srcCid , td.pg_id pid, td.table_id tid, td.col_idx cid, "
				+ "ca.isNumeric isNumeric , ca.dataAvgLen dataAvgLen , ca.numUniqVals numUniqVals , ca.numRows numRows , "
				+ "cm.numEntityMatches numEntityMatches, cm.numValueMatches numValueMatches, "
				+ "cm.totalMatches totalMatches, cm.totalMatchPercent totalMatchPercent, "
				+ "cm.entityMatchPercent entityMatchPercent , cm.valueMatchPercentage valueMatchPercentage, "
				+ "GROUP_CONCAT(cd.text ORDER BY td.row_idx SEPARATOR '\\t' ) vals , "
				+ "GROUP_CONCAT(cd.entityIDs ORDER BY td.row_idx SEPARATOR '\\t' ) entities, "
				+ "wlc.inlinks inlinks , wlc.outlinks outlinks , tim.id timId , "
				+ "numUniqVals/numRows orderVal1 , cm.valueMatchPercentage orderVal2 "
				+ "FROM  table_data td , column_matches cm , column_attrs ca  , cell_details  cd,"
				+ "wikiarticles_link_counts wlc, table_id_map tim "
				+ "WHERE cm.src_pg_id = ?  AND cm.src_table_id= ? AND cm.tgt_pg_id = td.pg_id "
				+ "AND cm.tgt_table_id = td.table_id AND cm.tgt_col_idx = td.col_idx "
				+ "AND (cm.tgt_pg_id != cm.src_pg_id  OR cm.tgt_table_id != cm.src_table_id) "
				+ "AND ca.pg_id = td.pg_id AND ca.table_id = td.table_id AND ca.col_idx = td.col_idx "
				+ "AND td.cellID = cd.cellID AND cm.tgt_pg_id = ? AND wlc.pg_id = td.pg_id "
				+ "AND tim.pg_id = td.pg_id AND tim.table_id = td.table_id "
				+ "GROUP BY td.pg_id , td.table_id , td.col_idx "
				+ "ORDER BY (orderVal1 * orderVal2) / (orderVal1 + orderVal2) DESC LIMIT 100";
		PreparedStatement matchedColumnsPS = streamConn.prepareStatement(
				matchedColumnSelectQ, ResultSet.TYPE_FORWARD_ONLY,
				ResultSet.CONCUR_READ_ONLY);
		matchedColumnsPS.setFetchSize(Integer.MIN_VALUE);

		matchedColumnsPS.setInt(1, pgId);
		matchedColumnsPS.setInt(2, tableId);
		matchedColumnsPS.setInt(3, mPgId);

		return getListOfCandidateColumns(streamConn, conn, pgId, tableId,
				originalColumnAttributes, matchedColumnsPS);

	}

	private ArrayList<AddedColumn> getListOfCandidateColumnsFilteredByKeyCol(
			Connection streamConn, Connection conn, Integer pgId,
			Integer tableId,
			HashMap<Integer, ColumnAttributes> originalColumnAttributes,
			Integer keyColId) throws SQLException, UnsupportedEncodingException {

		String matchedColumnSelectQ = "SELECT DISTINCT cm.src_col_idx as srcCid , td.pg_id pid, td.table_id tid, td.col_idx cid, "
				+ "ca.isNumeric isNumeric , ca.dataAvgLen dataAvgLen , ca.numUniqVals numUniqVals , ca.numRows numRows , "
				+ "cm.numEntityMatches numEntityMatches, cm.numValueMatches numValueMatches, "
				+ "cm.totalMatches totalMatches, cm.totalMatchPercent totalMatchPercent, "
				+ "cm.entityMatchPercent entityMatchPercent , cm.valueMatchPercentage valueMatchPercentage, "
				+ "GROUP_CONCAT(cd.text ORDER BY td.row_idx SEPARATOR '\\t' ) vals , "
				+ "GROUP_CONCAT(cd.entityIDs ORDER BY td.row_idx SEPARATOR '\\t' ) entities, "
				+ "wlc.inlinks inlinks , wlc.outlinks outlinks , tim.id timId ,  "
				+ "numUniqVals/numRows orderVal1 , cm.valueMatchPercentage orderVal2 "
				+ "FROM  table_data td , column_matches cm , column_attrs ca  , cell_details  cd,"
				+ " wikiarticles_link_counts wlc, table_id_map tim "
				+ "WHERE cm.src_pg_id = ?  AND cm.src_table_id= ? AND cm.src_col_idx = ? AND cm.tgt_pg_id = td.pg_id "
				+ "AND cm.tgt_table_id = td.table_id AND cm.tgt_col_idx = td.col_idx "
				+ "AND (cm.tgt_pg_id != cm.src_pg_id  OR cm.tgt_table_id != cm.src_table_id) "
				+ "AND ca.pg_id = td.pg_id AND ca.table_id = td.table_id AND ca.col_idx = td.col_idx "
				+ "AND td.cellID = cd.cellID AND wlc.pg_id = td.pg_id "
				+ "AND tim.pg_id = td.pg_id AND tim.table_id = td.table_id "
				+ "GROUP BY td.pg_id , td.table_id , td.col_idx "
				+ "ORDER BY (orderVal1 * orderVal2) / (orderVal1 + orderVal2) DESC LIMIT 100";

		PreparedStatement matchedColumnsPS = streamConn.prepareStatement(
				matchedColumnSelectQ, ResultSet.TYPE_FORWARD_ONLY,
				ResultSet.CONCUR_READ_ONLY);
		matchedColumnsPS.setFetchSize(Integer.MIN_VALUE);

		matchedColumnsPS.setInt(1, pgId);
		matchedColumnsPS.setInt(2, tableId);
		matchedColumnsPS.setInt(3, keyColId);

		return getListOfCandidateColumns(streamConn, conn, pgId, tableId,
				originalColumnAttributes, matchedColumnsPS);

	}

	private ArrayList<AddedColumn> getListOfCandidateColumnsAll(
			Connection streamConn, Connection conn, Integer pgId,
			Integer tableId,
			HashMap<Integer, ColumnAttributes> originalColumnAttributes,
			RankingType type) throws SQLException, UnsupportedEncodingException {

		String queryOrderByStr = KGridUtils.getOrderByStr(type);

		String matchedColumnSelectQ = "SELECT DISTINCT cm.src_col_idx as srcCid , td.pg_id pid, td.table_id tid, td.col_idx cid, "
				+ "ca.isNumeric isNumeric , ca.dataAvgLen dataAvgLen , ca.numUniqVals numUniqVals , "
				+ "cm.numEntityMatches numEntityMatches, cm.numValueMatches numValueMatches, "
				+ "cm.totalMatches totalMatches, cm.totalMatchPercent totalMatchPercent, "
				+ "cm.entityMatchPercent entityMatchPercent , cm.valueMatchPercentage valueMatchPercentage, "
				+ "ca.numRows numRows, "
				+ "GROUP_CONCAT(cd.text ORDER BY td.row_idx SEPARATOR '\\t' ) vals , "
				+ "GROUP_CONCAT(cd.entityIDs ORDER BY td.row_idx SEPARATOR '\\t' ) entities ,"
				+ "wlc.inlinks inlinks , wlc.outlinks outlinks , tim.id timId,  "
				+ "numUniqVals/numRows orderVal1 , cm.valueMatchPercentage orderVal2 "
				+ "FROM  table_data td , column_matches cm , column_attrs ca  , cell_details  cd ,"
				+ " wikiarticles_link_counts wlc , table_id_map tim "
				+ "WHERE cm.src_pg_id = ?  AND cm.src_table_id= ? AND cm.tgt_pg_id = td.pg_id "
				+ "AND cm.tgt_table_id = td.table_id AND cm.tgt_col_idx = td.col_idx "
				+ "AND (cm.tgt_pg_id != cm.src_pg_id  OR cm.tgt_table_id != cm.src_table_id) "
				+ "AND ca.pg_id = td.pg_id AND ca.table_id = td.table_id AND ca.col_idx = td.col_idx "
				+ "AND td.cellID = cd.cellID AND wlc.pg_id = td.pg_id "
				+ "AND tim.pg_id = td.pg_id AND tim.table_id = td.table_id "
				+ "GROUP BY td.pg_id , td.table_id , td.col_idx "
				+ queryOrderByStr;

		PreparedStatement matchedColumnsPS = streamConn.prepareStatement(
				matchedColumnSelectQ, ResultSet.TYPE_FORWARD_ONLY,
				ResultSet.CONCUR_READ_ONLY);
		matchedColumnsPS.setFetchSize(Integer.MIN_VALUE);

		matchedColumnsPS.setInt(1, pgId);
		matchedColumnsPS.setInt(2, tableId);

		return getListOfCandidateColumns(streamConn, conn, pgId, tableId,
				originalColumnAttributes, matchedColumnsPS);

	}

	private ArrayList<AddedColumn> getListOfCandidateColumns(
			Connection streamConn, Connection conn, Integer pgId,
			Integer tableId,
			HashMap<Integer, ColumnAttributes> originalColumnAttributes,
			PreparedStatement matchedColumnsPS) throws SQLException,
			UnsupportedEncodingException {

		ArrayList<AddedColumn> addableCandidateColumns = new ArrayList<AddedColumn>();
		Double numericColumnsInTableFraction = KGridUtils
				.getNumericColumnFraction(conn, pgId, tableId);

		ResultSet matchedColumnsRS = matchedColumnsPS.executeQuery();

		boolean isRedundant = false;
		int totalCount = 0;
		while (matchedColumnsRS.next()) {

			Integer srcCid = matchedColumnsRS.getInt("srcCid");
			// Original source column attributes
			ColumnAttributes origColAttr = originalColumnAttributes.get(srcCid);

			ColumnAttributes matchedColumnAttr = KGridUtils
					.getColumnAttributes(matchedColumnsRS,
							numericColumnsInTableFraction);

			ColumnMatchAttributes matchAttr = KGridUtils
					.getColumnMatchAttributes(matchedColumnsRS, KGridUtils
							.getAvgOneToMany(conn, pgId, tableId,
									origColAttr.getColIdx(),
									matchedColumnAttr.getPgId(),
									matchedColumnAttr.getTableId(),
									matchedColumnAttr.getColIdx()));
			matchAttr.setTgtColMatchesOtherCol(KGridUtils.isColRedundant(conn,
					pgId, tableId, matchedColumnAttr.getPgId(),
					matchedColumnAttr.getTableId(),
					matchedColumnAttr.getColIdx(),
					matchedColumnAttr.getNumRows()));

			ArrayList<ColumnAttributes> candidateColumns = getOtherColumnAttributes(
					conn, matchedColumnAttr.getPgId(),
					matchedColumnAttr.getTableId(),
					matchedColumnAttr.getColIdx());

			for (int i = 0; i < candidateColumns.size(); i++) {

				isRedundant = KGridUtils.isColRedundant(conn, origColAttr
						.getPgId(), origColAttr.getTableId(), candidateColumns
						.get(i).getPgId(),
						candidateColumns.get(i).getTableId(), candidateColumns
								.get(i).getColIdx(), candidateColumns.get(i)
								.getNumRows());

				if (isRedundant
						|| candidateColumns.get(i).getAvgLen() == 0
						|| isLowEntropy(conn, origColAttr.getPgId(),
								origColAttr.getTableId(),
								origColAttr.getColIdx(),
								matchedColumnAttr.getPgId(),
								matchedColumnAttr.getTableId(),
								matchedColumnAttr.getColIdx(), candidateColumns
										.get(i).getColIdx()))
					continue;

				addableCandidateColumns.add(new AddedColumn(origColAttr,
						matchedColumnAttr, candidateColumns.get(i), matchAttr));
			}

			totalCount++;

			if (totalCount >= 100)
				break;
		}

		matchedColumnsPS.close();
		matchedColumnsRS.close();

		int originalTablePgId = 0;
		int originalTableTableId = 0;
		int targetColPgId = 0;
		int targetColTableId = 0;
		int targetColId = 0;

		PreparedStatement corelPs = conn
				.prepareStatement("SELECT cim1.col_idx cid , corel_coeff FROM corels c , col_id_map cim1 , col_id_map cim2 "
						+ "WHERE (cim1.pg_id = ? AND cim1.table_id = ? AND cim1.id = c.src_col_id "
						+ "AND cim2.pg_id = ? AND cim2.table_id = ? AND cim2.col_idx = ? "
						+ "AND cim2.id = c.tgt_col_id) "
						+ "OR (cim1.pg_id = ? AND cim1.table_id = ? AND cim1.col_idx = ? AND cim1.id = c.src_col_id "
						+ "AND cim2.pg_id = ? AND cim2.table_id = ? AND cim2.id = c.tgt_col_id)  "
						+ "ORDER BY ABS(corel_coeff) DESC LIMIT 1;");
		Double corel = 0.0;
		for (int i = 0; i < addableCandidateColumns.size(); i++) {

			originalTablePgId = addableCandidateColumns.get(i).getOriginalCol()
					.getPgId();
			originalTableTableId = addableCandidateColumns.get(i)
					.getOriginalCol().getTableId();
			targetColPgId = addableCandidateColumns.get(i).getTgtCol()
					.getPgId();
			targetColTableId = addableCandidateColumns.get(i).getTgtCol()
					.getTableId();
			targetColId = addableCandidateColumns.get(i).getTgtCol()
					.getColIdx();

			corel = KGridUtils.getCorelCoeff(conn, corelPs, originalTablePgId,
					originalTableTableId, targetColPgId, targetColTableId,
					targetColId);
			if (corel < -1) {
				addableCandidateColumns.get(i).setHasCorel(false);
			} else {
				addableCandidateColumns.get(i).setCorelCoeff(corel);
			}

		}

		return addableCandidateColumns;
	}

	private boolean isLowEntropy(Connection conn, Integer origPgId,
			Integer origTableId, Integer origColId, Integer pgId,
			Integer tableId, Integer keyColId, Integer valColId)
			throws UnsupportedEncodingException, SQLException {

		KeyColumnData sCol = KgridColumnUtils.getKeyColumn(conn, origPgId,
				origTableId, origColId);

		sCol.setCellIdRowIdx(KGridUtils.getCellIdRowIdxMap(conn, origPgId,
				origTableId, origColId));

		KeyColumnData tCol = KgridColumnUtils.getKeyColumn(conn, pgId, tableId,
				keyColId);
		tCol.setCellIdRowIdx(KGridUtils.getCellIdRowIdxMap(conn, pgId, tableId,
				keyColId));

		HashMap<Integer, ArrayList<Integer>> indexMap = KgridColumnUtils
				.createKeyColumnsMap(sCol, tCol);

		HashMap<Integer, String> data = KGridUtils.rowWiseColumnData(conn,
				pgId, tableId, valColId);
		HashMap<Integer, String> keyData = KGridUtils.rowWiseColumnData(conn,
				origPgId, origTableId, origColId);

		AddableColumnData acd = new AddableColumnData();
		for (Entry<Integer, ArrayList<Integer>> e : indexMap.entrySet()) {
			for (int i = 0; i < e.getValue().size(); i++) {
				acd.addKeyValPair(keyData.get(e.getKey()),
						data.get(e.getValue().get(i)));
			}
		}

		HashSet<String> cellUniqValues = new HashSet<String>();
		String cellValStr = "";
		for (Entry<String, HashSet<String>> e : acd.getKeyValuePairs()
				.entrySet()) {
			Iterator<String> it = e.getValue().iterator();
			cellValStr = "";
			while (it.hasNext()) {
				cellValStr += it.next() + "-";
			}

			if (cellValStr.equalsIgnoreCase(""))
				continue;
			cellValStr = cellValStr.substring(0, cellValStr.length() - 1);
			cellUniqValues.add(cellValStr);

		}

		if ((double) cellUniqValues.size()
				/ (double) acd.getKeyValuePairs().size() < 0.2)
			return true;

		return false;
	}

	private ArrayList<ColumnAttributes> getOtherColumnAttributes(
			Connection conn, Integer pgId, Integer tableId, Integer colIdx)
			throws SQLException, UnsupportedEncodingException {

		Double numericColumnsInTableFraction = KGridUtils
				.getNumericColumnFraction(conn, pgId, tableId);
		ArrayList<ColumnAttributes> otherColumnsInTable = new ArrayList<ColumnAttributes>();
		PreparedStatement candidateColumnsPS = conn
				.prepareStatement("SELECT td.pg_id pid, td.table_id tid, td.col_idx cid, "
						+ "ca.isNumeric isNumeric , ca.dataAvgLen dataAvgLen , ca.numUniqVals numUniqVals ,  ca.numRows numRows , "
						+ "GROUP_CONCAT(cd.text ORDER BY td.row_idx SEPARATOR '\\t') vals, "
						+ "GROUP_CONCAT(cd.entityIds ORDER BY td.row_idx SEPARATOR '\\t') entities, "
						+ "wlc.inlinks inlinks , wlc.outlinks outlinks , tim.id timId "
						+ "FROM table_data td , cell_details cd , column_attrs ca ,"
						+ " wikiarticles_link_counts wlc , table_id_map tim  "
						+ "WHERE td.pg_id =  ?  AND td.table_id = ? and td.col_idx != ? "
						+ "AND td.cellID  = cd.cellID AND ca.pg_id = td.pg_id AND ca.table_id = td.table_id "
						+ "AND ca.col_idx = td.col_idx AND wlc.pg_id = td.pg_id "
						+ "AND tim.pg_id = td.pg_id AND tim.table_id = td.table_id "
						+ "GROUP BY td.pg_id , td.table_id , td.col_idx");

		candidateColumnsPS.setInt(1, pgId);
		candidateColumnsPS.setInt(2, tableId);
		candidateColumnsPS.setInt(3, colIdx);
		ResultSet candidateColumnsRS = candidateColumnsPS.executeQuery();

		while (candidateColumnsRS.next()) {
			otherColumnsInTable.add(KGridUtils.getColumnAttributes(
					candidateColumnsRS, numericColumnsInTableFraction));
		}

		return otherColumnsInTable;
	}

	public ArrayList<ColumnChoiceDetails> getColumnChoiceDetails(
			Connection conn, ArrayList<AddedColumn> rankedListOfColumns,
			Integer numColumn, Integer numRandomColumns) throws Exception {

		PreparedStatement columnDetailsPS = conn
				.prepareStatement("select wa.title pgTitle, ta.caption tableTitle, "
						+ "cd.text columnTitle , cd2.text bkpColTitle , ca.isNumeric isNumeric "
						+ "FROM wikiarticles wa , table_attrs ta , table_headers th , cell_details cd , "
						+ "table_data td , cell_details cd2 , column_attrs ca "
						+ "WHERE wa.pg_id = ta.pg_id AND ta.pg_id = th.pg_id AND th.pg_id = ? "
						+ "AND ta.id = ? AND th.table_id = ta.id AND th.col_idx = ? AND th.cellID = cd.cellID "
						+ "AND td.pg_id = th.pg_id AND td.table_id = th.table_id AND td.col_idx = th.col_idx "
						+ "AND td.row_idx = 1 AND td.cellID = cd2.cellID AND ca.pg_id = td.pg_id "
						+ "AND ca.table_id = td.table_id AND ca.col_idx = td.col_idx ");
		ArrayList<ColumnChoiceDetails> colChoices = new ArrayList<ColumnChoiceDetails>();
		for (int i = 0; i < numColumn && i < rankedListOfColumns.size(); i++) {

			AddedColumn ac = rankedListOfColumns.get(i);
			System.out.println("i = " + i + " Score = " + ac.getScore());
			ColumnChoiceDetails ccd = getColumnChoiceFromColumnAttr(ac,
					columnDetailsPS);
			ccd.setSource("ranked");
			ccd.setOrder(i);
			colChoices.add(ccd);
		}

		if (numColumn < rankedListOfColumns.size()) {

			HashSet<Integer> randomIndices = getRandomColumnIndices(numColumn,
					numRandomColumns, rankedListOfColumns.size());
			for (Integer idx : randomIndices) {
				AddedColumn ac = rankedListOfColumns.get(idx);
				ColumnChoiceDetails ccd = getColumnChoiceFromColumnAttr(ac,
						columnDetailsPS);
				ccd.setSource("random");
				ccd.setOrder(idx);
				colChoices.add(ccd);
				ccd.setInstance(ac.getInst());
			}
		}

		columnDetailsPS.close();
		return colChoices;
	}

	private HashSet<Integer> getRandomColumnIndices(Integer numColumn,
			Integer numRandomColumns, int maxIndex) throws Exception {
		HashSet<Integer> randomIndices = new HashSet<Integer>();
		int count = 0;

		int numElements = maxIndex - numColumn;
		double[] weights = new double[numElements];
		for (int i = 0; i < numElements; i++) {
			weights[i] = 1;
		}

		WeightedRandPerm wrp = new WeightedRandPerm(new Random(), weights);
		Integer[] indexes = wrp.getWeightedPermutation(weights);

		for (int i = 0; i < indexes.length
				&& randomIndices.size() < numRandomColumns; i++) {
			randomIndices.add(indexes[i] + numColumn);
		}
		return randomIndices;
	}

	private ColumnChoiceDetails getColumnChoiceFromColumnAttr(AddedColumn ac,
			PreparedStatement columnDetailsPS) throws SQLException,
			UnsupportedEncodingException {
		ColumnAttributes tgtColDetails = ac.getTgtCol();
		columnDetailsPS.setInt(1, tgtColDetails.getPgId());
		columnDetailsPS.setInt(2, tgtColDetails.getTableId());
		columnDetailsPS.setInt(3, tgtColDetails.getColIdx());

		String pgTitle = "";
		String tableTitle = "";
		String columnTitle = "";

		ResultSet rs = columnDetailsPS.executeQuery();
		byte[] bytes;
		Boolean isNumeric = false;
		while (rs.next()) {
			bytes = rs.getBytes("pgTitle");
			if (pgTitle.trim().equalsIgnoreCase(""))
				pgTitle += new String(bytes, "UTF-8") + " ";
			bytes = rs.getBytes("tableTitle");
			tableTitle += new String(bytes, "UTF-8") + " ";
			bytes = rs.getBytes("columnTitle");
			columnTitle += new String(bytes, "UTF-8") + " ";
			if (columnTitle.trim().equalsIgnoreCase("")) {
				bytes = rs.getBytes("bkpColTitle");
				columnTitle += new String(bytes, "UTF-8") + " ";
			}
			isNumeric = rs.getBoolean("isNumeric");
		}

		rs.close();
		if (tableTitle.length() > 100) {
			tableTitle = "";
		}
		ColumnChoiceDetails ccd = new ColumnChoiceDetails(pgTitle, tableTitle,
				columnTitle, ac.getSrcCol());
		ccd.setPgId(tgtColDetails.getPgId());
		ccd.setTableId(tgtColDetails.getTableId());
		ccd.setColIdx(tgtColDetails.getColIdx());
		ccd.setMatchedColumnIndex(ac.getSrcCol().getColIdx());
		ccd.setOriginalColumnIndex(ac.getOriginalCol().getColIdx());
		ccd.setIsNumeric(isNumeric);
		return ccd;

	}

	private HashMap<Integer, ColumnAttributes> getOriginalColumnAttributes(
			Connection conn, Integer pgId, Integer tableId)
			throws SQLException, UnsupportedEncodingException {

		Double numericColumnsInTableFraction = KGridUtils
				.getNumericColumnFraction(conn, pgId, tableId);

		PreparedStatement originalAttrsPS = conn
				.prepareStatement("select td.pg_id pid, td.table_id tid, td.col_idx cid, "
						+ "ca.isNumeric isNumeric , ca.dataAvgLen dataAvgLen , ca.numUniqVals numUniqVals, ca.numRows numRows , "
						+ "GROUP_CONCAT(cd.text ORDER BY td.row_idx SEPARATOR '\\t') vals, "
						+ "GROUP_CONCAT(cd.entityIds ORDER BY td.row_idx SEPARATOR '\\t') entities , "
						+ "wlc.inlinks inlinks , wlc.outlinks outlinks , tim.id timId "
						+ "FROM column_attrs ca , table_data td , cell_details cd , "
						+ " wikiarticles_link_counts wlc , table_id_map tim "
						+ "WHERE ca.pg_id = ?  AND ca.table_id = ? AND td.pg_id = ca.pg_id "
						+ "AND td.table_id = ca.table_id AND td.col_idx = ca.col_idx "
						+ "AND td.cellID = cd.cellID AND wlc.pg_id = td.pg_id "
						+ "AND tim.pg_id = td.pg_id AND tim.table_id = td.table_id "
						+ "GROUP BY td.pg_id , td.table_id , td.col_idx");

		HashMap<Integer, ColumnAttributes> colAttrs = new HashMap<Integer, ColumnAttributes>();
		originalAttrsPS.setInt(1, pgId);
		originalAttrsPS.setInt(2, tableId);

		ResultSet originalAttrsRS = originalAttrsPS.executeQuery();
		while (originalAttrsRS.next()) {

			Integer tgtPgId = originalAttrsRS.getInt("pid");
			Integer tgtTableId = originalAttrsRS.getInt("tid");
			Integer tgtColIdx = originalAttrsRS.getInt("cid");
			Boolean isNumeric = originalAttrsRS.getBoolean("isNumeric");
			Double avgLen = originalAttrsRS.getDouble("dataAvgLen");
			Integer numUniqVals = originalAttrsRS.getInt("numUniqVals");
			Integer numRows = originalAttrsRS.getInt("numRows");
			String[] vals = new String(originalAttrsRS.getBytes("vals"),
					"UTF-8").split("\t");
			String[] entities = new String(
					originalAttrsRS.getBytes("entities"), "UTF-8").split("\t");
			Integer inlinks = originalAttrsRS.getInt("inlinks");
			Integer outlinks = originalAttrsRS.getInt("outlinks");
			Integer timId = originalAttrsRS.getInt("timId");

			colAttrs.put(tgtColIdx, new ColumnAttributes(tgtPgId, tgtTableId,
					tgtColIdx, isNumeric, avgLen, numUniqVals, numRows, vals,
					entities, inlinks, outlinks, timId,
					numericColumnsInTableFraction));

		}

		originalAttrsPS.close();
		originalAttrsRS.close();

		return colAttrs;
	}

}
