package edu.northwestern.cs.kgrid.core.ml;

public class FeatureNormalizer {

	final static Double MATCH_PERC_STDEV = 0.25;
	final static Double MATCH_PERC_MEAN = 0.75;

	final static Double INLINKS_STDEV = 1351.0;
	final static Double INLINKS_MEAN = 113.14;

	final static Double OUTLINKS_STDEV = 129.72;
	final static Double OUTLINKS_MEAN = 105.48;

	final static Double AVERAGE_LEN_STDEV = 49.22;
	final static Double AVERAGE_LEN_MEAN = 13.92;

	public static Double matchPerc(Double val) {
		return (val - MATCH_PERC_MEAN) / MATCH_PERC_STDEV;

	}

	public static Integer categoryDistance(Integer d) {
		return (d - 1) / (10 - 1);
	}

	public static Double tgtAvgLen(Double len) {

		if (len > 4 && len < 10)
			return 1.0;
		else
			return 0.5;

	}

	public static Double inLinks(Integer inlinks) {
		return ((double) inlinks - INLINKS_MEAN) / INLINKS_STDEV;
	}

	public static Double outLinks(Integer outlinks) {
		return ((double) outlinks - OUTLINKS_MEAN) / OUTLINKS_STDEV;
	}

	public static Double averageLen(Double avgLen) {
		return (avgLen - AVERAGE_LEN_MEAN) / AVERAGE_LEN_STDEV;
	}

}
