package edu.northwestern.cs.kgrid.ml;

import edu.northwestern.cs.kgrid.server.models.AddedColumn;

public class DataLabels {

	AddedColumn ac;
	Integer vote;

	public DataLabels(AddedColumn ac, Integer vote) {
		super();
		this.ac = ac;
		this.vote = vote;
	}

	public AddedColumn getAc() {
		return ac;
	}

	public void setAc(AddedColumn ac) {
		this.ac = ac;
	}

	public Integer getVote() {
		return vote;
	}

	public void setVote(Integer vote) {
		this.vote = vote;
	}

}
