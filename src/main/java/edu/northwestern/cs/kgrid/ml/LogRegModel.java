package edu.northwestern.cs.kgrid.ml;

import java.sql.Connection;
import java.util.ArrayList;

import weka.classifiers.functions.Logistic;
import weka.core.Instance;
import weka.core.Instances;
import edu.northwestern.cs.kgrid.server.utils.KGridModelUtils;

public class LogRegModel {

	static Logistic logReg = new Logistic();
	public static Boolean isModelReady = true;
	static Integer numTrainingData = 0;

	public static void initModel(Connection conn, ArrayList<String[]> features,
			Instances data) throws Exception {
		logReg = new Logistic();

		data.setClassIndex(data.numAttributes() - 1);

		int size = data.numAttributes();
		data.delete();

		for (int i = 0; i < features.size(); i++) {

			Instance inst = new Instance(size);
			inst.setDataset(data);

			KGridModelUtils.setFeaturesForInstance(inst, features.get(i));
			data.add(inst);
		}

		data.setClassIndex(data.numAttributes() - 1);

		System.out.println("Table correl features .. ");
		System.out.println("ARFF -- \n\n" + data + "\n\n\n");
		logReg.buildClassifier(data);
		numTrainingData = data.numInstances();

		System.out.println("Trained model ---- ");
		System.out.println(logReg);
		System.out.println("\n\n");

	}

	public static Logistic getLogReg() {
		return logReg;
	}

	public static void setLogReg(Logistic logReg) {
		LogRegModel.logReg = logReg;
	}

	public static Integer getNumTrainingData() {
		return numTrainingData;
	}

	public static void setNumTrainingData(Integer numTrainingData) {
		LogRegModel.numTrainingData = numTrainingData;
	}

}
