package edu.northwestern.cs.kgrid.server;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.HttpMethod;
import org.json.JSONException;

import edu.northwestern.cs.WIG.lib.MySQLQueryHandler;
import edu.northwestern.cs.kgrid.config.ConfigReader;
import edu.northwestern.cs.kgrid.server.utils.ArticlesUtils;
import edu.northwestern.cs.kgrid.server.utils.MysqlUtils;

@Path("/articleService/")
public class ArticleService {

	@Context
	ServletContext context;

	@Context
	HttpServletRequest req;

	@GET
	@Produces("application/json; charset=UTF-8;")
	@Path("/addArticle/{title}")
	public String addArticle(@PathParam("title") String title) {
		HttpClient client = new HttpClient();
		HttpMethod method = null;
		try {
			ConfigReader conf = (ConfigReader) context
					.getAttribute("configReader");

			MySQLQueryHandler handler = MysqlUtils.getMysqlHandler(conf, "*");
			Connection conn = handler.getConnection();

			Integer kgridWaID = ArticlesUtils.getArticleID(conn, title);

			if (kgridWaID == -1) {

				PreparedStatement insWikiArticlesPS = conn
						.prepareStatement("INSERT INTO wikiarticles (title) VALUES (?)");
				insWikiArticlesPS.setBytes(1, title.getBytes());
				insWikiArticlesPS.execute();
				kgridWaID = ArticlesUtils.getArticleID(conn, title);

			}

			ArticlesUtils.insertWikiarticle(conn, kgridWaID, client, method);

		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (HttpException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return "";
	}

}
