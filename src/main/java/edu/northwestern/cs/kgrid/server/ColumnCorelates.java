package edu.northwestern.cs.kgrid.server;

import java.io.UnsupportedEncodingException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.TreeSet;

import javax.servlet.ServletContext;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;

import weka.classifiers.functions.Logistic;
import weka.core.Instance;
import weka.core.Instances;
import edu.northwestern.cs.WIG.lib.MySQLQueryHandler;
import edu.northwestern.cs.kgrid.config.ConfigReader;
import edu.northwestern.cs.kgrid.server.utils.MysqlUtils;

@Path("/corelates/")
public class ColumnCorelates {

	class RandomList {
		CorelDetails srcCol;
		CorelDetails tgtCol;
		Double sr;
		Double corel;

		public Double getSr() {
			return sr;
		}

		public void setSr(Double sr) {
			this.sr = sr;
		}

		public Double getCorel() {
			return corel;
		}

		public void setCorel(Double corel) {
			this.corel = corel;
		}

		public RandomList(CorelDetails srcCol, CorelDetails tgtCol, Double sr,
				Double corel) {
			super();
			this.srcCol = srcCol;
			this.tgtCol = tgtCol;
			this.sr = sr;
			this.corel = corel;
		}

		public CorelDetails getSrcCol() {
			return srcCol;
		}

		public void setSrcCol(CorelDetails srcCol) {
			this.srcCol = srcCol;
		}

		public CorelDetails getTgtCol() {
			return tgtCol;
		}

		public void setTgtCol(CorelDetails tgtCol) {
			this.tgtCol = tgtCol;
		}

	}

	class CorelDetailsComparator implements Comparator<CorelDetails> {

		public int compare(CorelDetails o1, CorelDetails o2) {

			return (int) Math.signum(o2.getScore() - o1.getScore());

		}

	}

	class CorelDetails {
		Integer id;
		String pageTitle;
		Integer pageId;
		String tableTitle;
		Integer tableId;
		String colTitle;
		Double corelCoeff;
		String keyColTitle;
		Integer keyColId;
		Double sr;
		Integer numSamples;
		Double score;

		public CorelDetails(Integer id, String pageTitle, Integer pageId,
				String tableTitle, Integer tableId, String colTitle,
				Double corelCoeff, String keyColTitle, Integer keyColId,
				Double sr, Integer numSamples, Double score) {
			super();
			this.id = id;
			this.pageTitle = pageTitle;
			this.pageId = pageId;
			this.tableTitle = tableTitle;
			this.tableId = tableId;
			this.colTitle = colTitle;
			this.corelCoeff = corelCoeff;
			this.keyColTitle = keyColTitle;
			this.keyColId = keyColId;
			this.sr = sr;
			this.numSamples = numSamples;
			this.score = score;
		}

		public CorelDetails(String pgTitle, int pgId, String tableTitle,
				int tableId, String colTitle2, double corel, double sr,
				int numSamples) {
			this.pageTitle = pgTitle;
			this.pageId = pgId;
			this.tableTitle = tableTitle;
			this.tableId = tableId;
			this.colTitle = colTitle2;
			this.corelCoeff = corel;
			this.sr = sr;
			this.numSamples = numSamples;
		}

		public CorelDetails(Integer id, String pgTitle, Integer pgId,
				String tableCaption, Integer tableId2, String header) {

			this.id = id;
			this.pageTitle = pgTitle;
			this.pageId = pgId;
			this.tableTitle = tableCaption;
			this.tableId = tableId2;
			this.colTitle = header;
		}

		public Double getScore() {
			return score;
		}

		public void setScore(Double score) {
			this.score = score;
		}

		public Double getSr() {
			return sr;
		}

		public void setSr(Double sr) {
			this.sr = sr;
		}

		public Integer getNumSamples() {
			return numSamples;
		}

		public void setNumSamples(Integer numSamples) {
			this.numSamples = numSamples;
		}

		public String getPageTitle() {
			return pageTitle;
		}

		public void setPageTitle(String pageTitle) {
			this.pageTitle = pageTitle;
		}

		public String getTableTitle() {
			return tableTitle;
		}

		public void setTableTitle(String tableTitle) {
			this.tableTitle = tableTitle;
		}

		public String getColTitle() {
			return colTitle;
		}

		public void setColTitle(String colTitle) {
			this.colTitle = colTitle;
		}

		public Double getCorelCoeff() {
			return corelCoeff;
		}

		public void setCorelCoeff(Double corelCoeff) {
			this.corelCoeff = corelCoeff;
		}

		public String getKeyColTitle() {
			return keyColTitle;
		}

		public void setKeyColTitle(String keyColTitle) {
			this.keyColTitle = keyColTitle;
		}

		public Integer getKeyColId() {
			return keyColId;
		}

		public void setKeyColId(Integer keyColId) {
			this.keyColId = keyColId;
		}

		public Integer getId() {
			return id;
		}

		public void setId(Integer id) {
			this.id = id;
		}

		public Integer getPageId() {
			return pageId;
		}

		public void setPageId(Integer pageId) {
			this.pageId = pageId;
		}

		public Integer getTableId() {
			return tableId;
		}

		public void setTableId(Integer tableId) {
			this.tableId = tableId;
		}

	}

	@Context
	ServletContext context;

	@GET
	@Produces("application/json; charset=UTF-8;")
	@Path("view/colID/{colID}")
	public TreeSet<CorelDetails> viewCorelates(@PathParam("colID") Integer colID)
			throws Exception {

		TreeSet<CorelDetails> results = new TreeSet<CorelDetails>(
				new CorelDetailsComparator());
		ConfigReader conf = (ConfigReader) context.getAttribute("configReader");

		MySQLQueryHandler handler = MysqlUtils.getMysqlHandler(conf, "*");
		Connection conn = handler.getConnection();

		PreparedStatement ps = conn
				.prepareStatement("SELECT c.src_col_id scid, c.tgt_col_id tcid, "
						+ "wa.title t, wa.pg_id pageId , ta.caption c, ta.id tableId , cd.text h, "
						+ "c.corel_coeff corel , c.sr sr , c.se se , c.numSamples numSamples "
						+ "FROM wikiarticles wa , table_attrs ta , table_headers th , "
						+ "cell_details cd , col_id_map  cim , corels_attrs c  "
						+ "WHERE (cim.pg_id = wa.pg_id AND ta.pg_id = cim.pg_id AND ta.id = cim.table_id "
						+ "AND th.pg_id = cim.pg_id AND th.table_id = cim.table_id AND th.col_idx = cim.col_idx "
						+ "AND th.cellID = cd.cellID AND cim.id = c.tgt_col_id AND c.src_col_id = ? ) "
						+ "UNION "
						+ "SELECT c.src_col_id scid, c.tgt_col_id tcid, "
						+ "wa.title t, wa.pg_id pageId ,  ta.caption c, ta.id tableId ,  cd.text h, c.corel_coeff corel , "
						+ "c.sr sr , c.se se , c.numSamples numSamples "
						+ "FROM wikiarticles wa , table_attrs ta , table_headers th , "
						+ "cell_details cd , col_id_map  cim , corels_attrs c  "
						+ "WHERE (cim.pg_id = wa.pg_id AND ta.pg_id = cim.pg_id AND ta.id = cim.table_id "
						+ "AND th.pg_id = cim.pg_id AND th.table_id = cim.table_id AND th.col_idx = cim.col_idx "
						+ "AND th.cellID = cd.cellID AND cim.id = c.src_col_id AND c.tgt_col_id = ? ) "
						+ "ORDER BY ABS(corel) DESC LIMIT 500");
		ps.setInt(1, colID);
		ps.setInt(2, colID);

		ResultSet rs = ps.executeQuery();
		ResultSet overlapCounrRs;
		String pgTitle, tableTitle, colTitle;
		Integer scid, tcid;
		boolean isRelevant = true;

		Instances instances = (Instances) context
				.getAttribute("interestingnessModelInstances");

		Logistic l = new Logistic();
		l.buildClassifier(instances);
		int numAttr = instances.instance(0).numAttributes();
		Instance inst;
		while (rs.next()) {
			inst = new Instance(numAttr);
			inst.setDataset(instances);
			inst.setClassMissing();
			inst.setValue(0, rs.getDouble("corel"));
			inst.setValue(1, rs.getDouble("sr"));
			inst.setValue(2, rs.getDouble("se"));
			inst.setValue(3, rs.getInt("numSamples"));
			inst.setValue(4,
					getZScore(conn, rs.getInt("scid"), rs.getDouble("corel")));
			isRelevant = false;
			double score = l.distributionForInstance(inst)[1];
			if (score > 0.5)
				isRelevant = true;

			if (!isRelevant)
				continue;

			scid = rs.getInt("scid");
			tcid = rs.getInt("tcid");

			pgTitle = new String(rs.getBytes("t"), "UTF-8");
			tableTitle = new String(rs.getBytes("c"), "UTF-8");
			colTitle = new String(rs.getBytes("h"), "UTF-8");
			CorelDetails c = new CorelDetails(pgTitle, rs.getInt("pageId"),
					tableTitle, rs.getInt("tableId"), colTitle,
					rs.getDouble("corel"), rs.getDouble("sr"),
					rs.getInt("numSamples"));
			c.setScore(score);
			results.add(c);

		}

		return results;
	}

	@GET
	@Produces("application/json; charset=UTF-8;")
	@Path("/random/limit/{limit}/user/{user}")
	public ArrayList<RandomList> viewRandomCorelates(
			@PathParam("limit") Integer limit, @PathParam("user") String user)
			throws SQLException, UnsupportedEncodingException,
			InstantiationException, IllegalAccessException,
			ClassNotFoundException {

		ArrayList<RandomList> results = new ArrayList<RandomList>();
		ConfigReader conf = (ConfigReader) context.getAttribute("configReader");

		MySQLQueryHandler handler = MysqlUtils.getMysqlHandler(conf, "*");
		Connection conn = handler.getConnection();

		// PreparedStatement ps = conn
		// .prepareStatement("SELECT * FROM sr_corel_test_set");
		PreparedStatement keyColumnPS = conn
				.prepareStatement("SELECT cim3.id sId , cim4.id tId "
						+ "FROM corels_attrs_subset_testSet t , col_id_map cim1 , "
						+ "col_id_map cim2 , col_id_map cim3 , col_id_map cim4 , column_matches_list cml "
						+ "WHERE t.src_col_id = cim1.id AND t.tgt_col_id = cim2.id "
						+ "AND t.src_col_id = ? AND t.tgt_col_id = ? AND cim3.pg_id = cim1.pg_id "
						+ "AND cim3.table_id = cim1.table_id AND cim4.pg_id = cim2.pg_id "
						+ "AND cim2.table_id = cim4.table_id AND cml.src_col_id = cim3.id "
						+ "AND cml.tgt_col_id = cim4.id");

		PreparedStatement ps = conn
				.prepareStatement("select * from corels_attrs_subset_testSet t "
						+ "LEFT JOIN corels_attrs_ratings r "
						+ "ON t.src_col_id = r.src_col_id AND t.tgt_col_id = r.tgt_col_id "
						// +
						// "WHERE r.src_col_id is null AND r.tgt_col_id is null "
						+ "WHERE user != ? OR user is null LIMIT " + limit);
		ps.setString(1, user);
		ResultSet rs = ps.executeQuery();

		while (rs.next()) {

			CorelDetails srcCol = getColDetails(conn, rs.getInt("src_col_id"));
			CorelDetails tgtCol = getColDetails(conn, rs.getInt("tgt_col_id"));

			setKeyColumns(conn, keyColumnPS, srcCol, tgtCol);

			results.add(new RandomList(srcCol, tgtCol, rs.getDouble("sr"), rs
					.getDouble("corel_coeff")));

		}

		conn.close();
		keyColumnPS.close();
		return results;

	}

	private void setKeyColumns(Connection conn, PreparedStatement keyColumnPS,
			CorelDetails srcCol, CorelDetails tgtCol) throws SQLException,
			UnsupportedEncodingException {

		keyColumnPS.setInt(1, srcCol.getId());
		keyColumnPS.setInt(2, tgtCol.getId());
		ResultSet rs = keyColumnPS.executeQuery();

		while (rs.next()) {

			CorelDetails sc = getColDetails(conn, rs.getInt("sId"));
			CorelDetails tc = getColDetails(conn, rs.getInt("tId"));

			srcCol.setKeyColTitle(sc.getColTitle());
			srcCol.setKeyColId(sc.getId());
			tgtCol.setKeyColTitle(tc.getColTitle());
			tgtCol.setKeyColId(tc.getId());

		}

		rs.close();

	}

	private CorelDetails getColDetails(Connection conn, Integer id)
			throws SQLException, UnsupportedEncodingException {
		PreparedStatement ps = conn
				.prepareStatement("SELECT wa.title p , wa.pg_id pageId,  ta.caption t , ta.id tableId , cd.text h "
						+ "FROM col_id_map cim , wikiarticles wa , table_attrs ta , "
						+ "table_headers th , cell_details cd   "
						+ "WHERE cim.id = ? AND cim.pg_id = wa.pg_id  "
						+ "AND cim.pg_id = ta.pg_id AND cim.table_id = ta.id "
						+ "AND cim.pg_id = th.pg_id AND cim.table_id = th.table_id "
						+ "AND cim.col_idx = th.col_idx AND th.cellID = cd.cellID ");

		ps.setInt(1, id);
		ResultSet rs = ps.executeQuery();

		String pgTitle = "";
		String tableCaption = "";
		String header = "";
		Integer pgId = 0;
		Integer tableId = 0;
		while (rs.next()) {

			pgTitle = new String(rs.getBytes("p"), "UTF-8");
			tableCaption = new String(rs.getBytes("t"), "UTF-8");
			header += new String(rs.getBytes("h"), "UTF-8") + " - ";
			pgId = rs.getInt("pageId");
			tableId = rs.getInt("tableId");

		}

		return new CorelDetails(id, pgTitle, pgId, tableCaption, tableId,
				header);

	}

	private static Double getZScore(Connection conn, int srcColId,
			Double correlation) throws SQLException {

		PreparedStatement avgCorPS = conn
				.prepareStatement("SELECT AVG(corel_coeff) avgCor , STDDEV(corel_coeff) stddevCor  FROM corels_attrs WHERE src_col_id = ?");
		avgCorPS.setInt(1, srcColId);
		ResultSet rs = avgCorPS.executeQuery();
		double res = 0.0;
		while (rs.next()) {
			res = (correlation - rs.getDouble("avgcor"))
					/ rs.getDouble("stddevCor");
		}

		return res;
	}
}
