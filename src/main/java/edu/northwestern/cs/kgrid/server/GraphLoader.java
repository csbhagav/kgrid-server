package edu.northwestern.cs.kgrid.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashSet;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import weka.classifiers.functions.Logistic;
import weka.core.Instances;
import ciir.umass.edu.learning.Ranker;
import edu.northwestern.cs.WIG.lib.MySQLQueryHandler;
import edu.northwestern.cs.kgrid.config.ConfigReader;
import edu.northwestern.cs.kgrid.ml.DataLabels;
import edu.northwestern.cs.kgrid.ml.LogRegModel;
import edu.northwestern.cs.kgrid.server.models.LabeledDataType;
import edu.northwestern.cs.kgrid.server.models.RankingType;
import edu.northwestern.cs.kgrid.server.utils.KGridModelUtils;
import edu.northwestern.cs.kgrid.server.utils.MysqlUtils;

public class GraphLoader implements ServletContextListener {

	static Integer testVariable = 1;

	public void contextDestroyed(ServletContextEvent sce) {
		Enumeration<Driver> drivers = DriverManager.getDrivers();
		while (drivers.hasMoreElements()) {
			Driver driver = drivers.nextElement();
			try {
				DriverManager.deregisterDriver(driver);

			} catch (SQLException e) {
			}

		}

	}

	@SuppressWarnings("unchecked")
	public void contextInitialized(ServletContextEvent sce) {

		ServletContext context = sce.getServletContext();
		try {
			storeConfigInContext(context);

			InputStream is = this.getClass().getResourceAsStream(
					"/features.arff");
			KGridModelUtils.storeFeatureSetInContext(context, is);
			storeRankerFeatures(context);
			InputStream colRankFeaturesIs = this.getClass()
					.getResourceAsStream("/columRankFeatures");
			KGridModelUtils.storeColumnRankerFeatures(context,
					colRankFeaturesIs);
			is.close();
			colRankFeaturesIs.close();

			ConfigReader conf = (ConfigReader) context
					.getAttribute("configReader");

			MySQLQueryHandler handler = MysqlUtils.getMysqlHandler(conf, "*");

			// **** Table Search Ranker
			HashSet<String> rankerFeatures = (HashSet<String>) context
					.getAttribute("rankerFeatures");

			Ranker r = KGridModelUtils.learnTableSearchRanker(
					handler.getConnection(), rankerFeatures);
			System.out.println("Model details \n\n");
			r.printParameters();
			System.out.println(r.model() + "\n\nModel details \n\n");
			context.setAttribute("rankingLib", r);
			// **** Table Search Ranker

			Instances data = (Instances) context.getAttribute("featureSet");

			// Get labelled data
			ArrayList<DataLabels> labelledData = KGridModelUtils
					.getLabeledData(handler.getConnection(),
							LabeledDataType.TRAIN);

			// Convert data to features for classifier
			ArrayList<String[]> classifierFeatures = KGridModelUtils
					.getColumnFeaturesForClassifer(handler.getConnection(),
							data, labelledData);

			// Initialize classifier
			LogRegModel.initModel(handler.getConnection(), classifierFeatures,
					data);

			// Convert data to features for ranker
			ArrayList<ArrayList<String>> columnRankerTrainData = KGridModelUtils
					.getColumnFeaturesForRanker(handler.getConnection(), data,
							labelledData);
			HashSet<String> columRankFeatures = (HashSet<String>) context
					.getAttribute("columRankFeatures");
			Ranker columnRanker = KGridModelUtils.learnColumnRankerRanker(
					handler.getConnection(), columRankFeatures,
					columnRankerTrainData);

			System.out.println("Column ranker Model details \n\n");
			columnRanker.printParameters();
			System.out.println(columnRanker.model() + "\n\nModel details \n\n");

			context.setAttribute("columnRanker", columnRanker);

			context.setAttribute("rankingType", RankingType.CLSFR_AND_RANKER);

			learnModelForFindingInterestingCorrelations(context, handler);
			handler.getConnection().close();

		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private void learnModelForFindingInterestingCorrelations(
			ServletContext context, MySQLQueryHandler handler)
			throws IOException, Exception {
		InputStream is = this.getClass().getResourceAsStream(
				"/interesting-training.arff");
		BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		Instances instances = new Instances(reader);
		instances.setClassIndex(instances.numAttributes() - 1);
		context.setAttribute("interestingnessModelInstances", instances);
	}

	private void storeRankerFeatures(ServletContext context) throws IOException {
		InputStream is = this.getClass().getResourceAsStream("/rankerFeatures");
		BufferedReader reader = new BufferedReader(new InputStreamReader(is));

		String line = "";
		HashSet<String> rankerFeatures = new HashSet<String>();
		while ((line = reader.readLine()) != null) {
			if (!line.startsWith("#"))
				rankerFeatures.add(line);
			System.out.println(line);
		}
		context.setAttribute("rankerFeatures", rankerFeatures);
	}

	private void storeConfigInContext(ServletContext context) {
		try {
			InputStream is = this.getClass().getResourceAsStream(
					"/mysql.config");
			ConfigReader conf = new ConfigReader(is);
			context.setAttribute("configReader", conf);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
