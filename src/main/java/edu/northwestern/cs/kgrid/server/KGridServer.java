package edu.northwestern.cs.kgrid.server;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;

import org.codehaus.jettison.json.JSONException;

import weka.core.Instances;
import ciir.umass.edu.learning.Ranker;

import com.google.gson.Gson;

import edu.northwestern.cs.WIG.lib.MySQLQueryHandler;
import edu.northwestern.cs.kgrid.config.ConfigReader;
import edu.northwestern.cs.kgrid.core.RankedColumns;
import edu.northwestern.cs.kgrid.server.controller.WikiTableManager;
import edu.northwestern.cs.kgrid.server.models.AddableColumnData;
import edu.northwestern.cs.kgrid.server.models.AddedColumn;
import edu.northwestern.cs.kgrid.server.models.ColumnChoiceDetails;
import edu.northwestern.cs.kgrid.server.models.PageTableItem;
import edu.northwestern.cs.kgrid.server.models.RankingType;
import edu.northwestern.cs.kgrid.server.models.WikiTable;
import edu.northwestern.cs.kgrid.server.utils.KGridUtils;
import edu.northwestern.cs.kgrid.server.utils.MysqlUtils;

@Path("/table/")
public class KGridServer {

	@Context
	ServletContext context;

	@Context
	HttpServletRequest req;

	@GET
	@Produces("application/json; charset=UTF-8;")
	@Path("/pgId/{pgId}/tableId/{tableId}")
	public WikiTable getTable(@PathParam("pgId") Integer pgId,
			@PathParam("tableId") Integer tableId) {

		try {
			System.out.println("LOGGER:" + req.getRemoteAddr() + " - "
					+ req.getPathInfo());
			ConfigReader conf = (ConfigReader) context
					.getAttribute("configReader");

			MySQLQueryHandler handler = MysqlUtils.getMysqlHandler(conf, "*");
			Connection conn = handler.getConnection();

			WikiTableManager wkManager = new WikiTableManager(conn, pgId,
					tableId);
			wkManager.loadOriginalData();

			conn.close();

			return wkManager.getTable();

		} catch (IOException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return new WikiTable();
	}

	@GET
	@Produces("application/json; charset=UTF-8;")
	@Path("/pgId/{pgId}")
	public ArrayList<PageTableItem> getTablesOnPage(
			@PathParam("pgId") Integer pgId) {
		ArrayList<PageTableItem> tableList = new ArrayList<PageTableItem>();

		try {
			System.out.println("LOGGER:" + req.getRemoteAddr() + " - "
					+ req.getPathInfo());

			ConfigReader conf = (ConfigReader) context
					.getAttribute("configReader");

			MySQLQueryHandler handler = MysqlUtils.getMysqlHandler(conf, "*");
			Connection conn = handler.getConnection();

			PreparedStatement tablesOnPagePS = conn
					.prepareStatement("SELECT concat(t.pg_id,'-',t.id) as table_id, t.caption as title, "
							+ "t.numDataRows as rowcount,  t.numColumns as colcount, "
							+ "GROUP_CONCAT(c.text SEPARATOR ' | ') as columns "
							+ "FROM table_attrs t "
							+ "JOIN (SELECT th.pg_id, th.table_id, th.col_idx, th.row_idx, "
							+ "GROUP_CONCAT(cd.text ORDER BY row_idx SEPARATOR ' - ') as text "
							+ "FROM table_headers th JOIN cell_details cd ON th.cellID = cd.cellID "
							+ "WHERE th.pg_id = ? GROUP BY table_id, col_idx ORDER BY col_idx) as c "
							+ "ON t.id = c.table_id and t.pg_id = c.pg_id "
							+ "WHERE t.pg_id = ? GROUP BY t.id ORDER BY rowcount DESC");

			tablesOnPagePS.setInt(1, pgId);
			tablesOnPagePS.setInt(2, pgId);

			ResultSet tablesOnPageRS = tablesOnPagePS.executeQuery();
			while (tablesOnPageRS.next()) {
				tableList
						.add(new PageTableItem(tablesOnPageRS
								.getString("table_id"), new String(
								tablesOnPageRS.getBytes("title"), "UTF-8"),
								tablesOnPageRS.getInt("rowcount"),
								tablesOnPageRS.getInt("colcount"), new String(
										tablesOnPageRS.getBytes("columns"),
										"UTF-8")));

			}

			conn.close();

		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return tableList;
	}

	@GET
	@Produces("application/json; charset=UTF-8;")
	@Path("/searchColumnTitle/{pgId}/tableId/{tableId}/suggestionLimit/{columnCount}/randomLimit/{randomCount}")
	public ArrayList<ColumnChoiceDetails> getRelevantColumns(
			@PathParam("pgId") Integer pgId,
			@PathParam("tableId") Integer tableId,
			@PathParam("columnCount") Integer numColumn,
			@PathParam("randomCount") Integer randomColumnCount)
			throws Exception {

		System.out.println("THIS THIS THIS");
		System.out.println("LOGGER:" + req.getRemoteAddr() + " - "
				+ req.getPathInfo());

		ArrayList<ColumnChoiceDetails> res = tryToGetFromCache(convertToKey(req
				.getPathInfo()));
		if (res != null) {
			System.out.println("RETURNING FROM CACHE");
			return res;
		}

		ConfigReader conf = (ConfigReader) context.getAttribute("configReader");

		MySQLQueryHandler handler = MysqlUtils.getMysqlHandler(conf, "*");
		Connection conn = handler.getConnection();

		ArrayList<AddedColumn> rankedListOfColumns = getAdditionalColumnsAll(
				pgId, tableId);

		if (rankedListOfColumns == null)
			return new ArrayList<ColumnChoiceDetails>();
		RankedColumns rankedColumns = new RankedColumns();

		ArrayList<ColumnChoiceDetails> columnChoices = rankedColumns
				.getColumnChoiceDetails(conn, rankedListOfColumns, numColumn,
						randomColumnCount);

		conn.close();

		putIntoCache(convertToKey(req.getPathInfo()), columnChoices);
		return columnChoices;

	}

	private void putIntoCache(String key,
			ArrayList<ColumnChoiceDetails> columnChoices) {
		System.out.println("key = " + key);

		try {
			System.out.println("Putting in cache ... ");

			URL url = new URL(
					"http://localhost:8085/ehcache/rest/sampleCache2/" + key);

			HttpURLConnection connection = (HttpURLConnection) url
					.openConnection();

			connection.setRequestProperty("Content-Type",
					"application/json; charset=UTF-8;");
			connection.setDoOutput(true);
			connection.setRequestMethod("PUT");
			connection.connect();

			Gson g = new Gson();
			String jsonStr = g.toJson(columnChoices);
			OutputStream os = connection.getOutputStream();
			os.write(jsonStr.getBytes("UTF-8"));
			os.flush();
			System.out.println("Write success ?? = "
					+ connection.getResponseCode());
		} catch (IOException e) {
			System.out.println("Unable to connect to cache server");

		}

	}

	private ArrayList<ColumnChoiceDetails> tryToGetFromCache(String key) {
		ArrayList<ColumnChoiceDetails> result = null;
		System.out.println("key = " + key);
		try {
			System.out.println("TRYING FROM CACHE .. ");
			HttpURLConnection connection = null;
			URL url = new URL(
					"http://localhost:8085/ehcache/rest/sampleCache2/" + key);

			connection = (HttpURLConnection) url.openConnection();

			connection.setRequestMethod("GET");
			connection.connect();
			if (connection.getResponseCode() != 200) {
				return null;
			}
			InputStream is = connection.getInputStream();

			InputStreamReader isr = new InputStreamReader(is);

			Gson g = new Gson();
			result = g.fromJson(isr, ArrayList.class);
			isr.close();
			

		} catch (Exception e) {
			System.out.println("Unable to connect to cache server");
		}

		return result;
	}

	private String convertToKey(String pathInfo) {
		return pathInfo.replaceAll("/", "-");
	}

	@GET
	@Produces("application/json; charset=UTF-8;")
	@Path("/searchColumnTitle/{pgId}/tableId/{tableId}/suggestionLimit/{columnCount}/randomLimit/{randomCount}/filter/matchingPgId/{mPgId}")
	public ArrayList<ColumnChoiceDetails> getRelevantColumnsFilteredByPage(
			@PathParam("pgId") Integer pgId,
			@PathParam("tableId") Integer tableId,
			@PathParam("columnCount") Integer numColumn,
			@PathParam("randomCount") Integer randomColumnCount,
			@PathParam("mPgId") Integer mPgId) throws Exception {

		System.out.println("LOGGER:" + req.getRemoteAddr() + " - "
				+ req.getPathInfo());

		ConfigReader conf = (ConfigReader) context.getAttribute("configReader");

		MySQLQueryHandler handler = MysqlUtils.getMysqlHandler(conf, "*");
		Connection conn = handler.getConnection();
		ArrayList<AddedColumn> rankedListOfColumns = getAdditionalColumnsFilteredByPage(
				pgId, tableId, mPgId);

		if (rankedListOfColumns == null)
			return new ArrayList<ColumnChoiceDetails>();

		RankedColumns rankedColumns = new RankedColumns();

		ArrayList<ColumnChoiceDetails> columnChoices = rankedColumns
				.getColumnChoiceDetails(conn, rankedListOfColumns, numColumn,
						randomColumnCount);

		conn.close();

		return columnChoices;
	}

	@GET
	@Produces("application/json; charset=UTF-8;")
	@Path("/searchColumnTitle/{pgId}/tableId/{tableId}/suggestionLimit/{columnCount}/randomLimit/{randomCount}/filter/keyColID/{keyColId}")
	public ArrayList<ColumnChoiceDetails> getRelevantColumnsFilteredByKeyColumn(
			@PathParam("pgId") Integer pgId,
			@PathParam("tableId") Integer tableId,
			@PathParam("columnCount") Integer numColumn,
			@PathParam("randomCount") Integer randomColumnCount,
			@PathParam("keyColId") Integer keyColId) throws Exception {

		System.out.println("LOGGER:" + req.getRemoteAddr() + " - "
				+ req.getPathInfo());

		ConfigReader conf = (ConfigReader) context.getAttribute("configReader");

		MySQLQueryHandler handler = MysqlUtils.getMysqlHandler(conf, "*");
		Connection conn = handler.getConnection();
		ArrayList<AddedColumn> rankedListOfColumns = getAdditionalColumnsFilteredByKeyCol(
				pgId, tableId, keyColId);

		if (rankedListOfColumns == null)
			return new ArrayList<ColumnChoiceDetails>();

		RankedColumns rankedColumns = new RankedColumns();

		ArrayList<ColumnChoiceDetails> columnChoices = rankedColumns
				.getColumnChoiceDetails(conn, rankedListOfColumns, numColumn,
						randomColumnCount);

		conn.close();

		return columnChoices;
	}

	@GET
	@Produces("application/json; charset=UTF-8;")
	@Path("/getAddableColumnData/origPgId/{origPgId}/origTableId/{origTableId}/origColId/{origColId}/pgId/{pgId}/tableId/{tableId}/keyColumn/{keyColumn}/addedColumn/{addedColumn}")
	public AddableColumnData getAddableColumnDataMatchingKeyColumn(
			@PathParam("origPgId") Integer origPgId,
			@PathParam("origTableId") Integer origTableId,
			@PathParam("origColId") Integer origColId,
			@PathParam("pgId") Integer pgId,
			@PathParam("tableId") Integer tableId,
			@PathParam("keyColumn") Integer keyColumn,
			@PathParam("addedColumn") Integer addedColumn) {

		System.out.println("LOGGER:" + req.getRemoteAddr() + " - "
				+ req.getPathInfo());

		try {
			ConfigReader conf = (ConfigReader) context
					.getAttribute("configReader");

			MySQLQueryHandler handler;
			handler = MysqlUtils.getMysqlHandler(conf, "*");
			Connection conn = handler.getConnection();

			KGridUtils kUtils = new KGridUtils();

			AddableColumnData acd = kUtils.getAddableColumnData(conn, origPgId,
					origTableId, origColId, pgId, tableId, keyColumn,
					addedColumn);

			PreparedStatement detailsPS = conn
					.prepareStatement("SELECT wa.title as pgTitle , ta.caption as tableTitle , cd.text as colTitle "
							+ "FROM wikiarticles wa , table_attrs ta , table_headers th , cell_details cd "
							+ "WHERE wa.pg_id = ? AND wa.pg_id = ta.pg_id AND ta.id = ? AND th.pg_id = wa.pg_id  "
							+ "AND th.table_id = ta.id AND th.col_idx = ? AND th.cellID = cd.cellID");
			detailsPS.setInt(1, pgId);
			detailsPS.setInt(2, tableId);
			detailsPS.setInt(3, addedColumn);

			ResultSet rs = detailsPS.executeQuery();
			while (rs.next()) {
				acd.setPageTitle(new String(rs.getBytes("pgTitle"), "UTF-8"));
				acd.setTableTitle(new String(rs.getBytes("tableTitle"), "UTF-8"));
				acd.setColumnTitle(new String(rs.getBytes("colTitle"), "UTF-8"));
			}

			conn.close();
			return acd;

		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return null;

	}

	private ArrayList<AddedColumn> getAdditionalColumnsAll(Integer pgId,
			Integer tableId) {

		try {

			System.out.println("LOGGER:" + req.getRemoteAddr() + " - "
					+ req.getPathInfo());

			Instances instanceData = (Instances) context
					.getAttribute("featureSet");

			Ranker columnRanker = (Ranker) context.getAttribute("columnRanker");

			ConfigReader conf = (ConfigReader) context
					.getAttribute("configReader");

			MySQLQueryHandler handler = MysqlUtils.getMysqlHandler(conf, "*");
			Connection conn = handler.getConnection();

			MySQLQueryHandler streamHandler = MysqlUtils.getMysqlHandler(conf,
					"*");
			Connection streamConn = streamHandler.getConnection();

			MySQLQueryHandler graphConnHandler = MysqlUtils.getMysqlHandler(
					conf, "graph");

			RankingType type = (RankingType) context
					.getAttribute("rankingType");

			RankedColumns rankedColumns = new RankedColumns();
			ArrayList<AddedColumn> rankedListOfColumns = rankedColumns
					.getRankedListOfAddableColumnsAll(streamConn, conn, pgId,
							tableId, instanceData, columnRanker, type);
			conn.close();
			streamConn.close();
			return rankedListOfColumns;

		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return null;
	}

	private ArrayList<AddedColumn> getAdditionalColumnsFilteredByPage(
			Integer pgId, Integer tableId, Integer mPgId) {

		try {

			Instances data = (Instances) context.getAttribute("featureSet");
			Ranker columnRanker = (Ranker) context.getAttribute("columnRanker");

			ConfigReader conf = (ConfigReader) context
					.getAttribute("configReader");

			MySQLQueryHandler handler = MysqlUtils.getMysqlHandler(conf, "*");
			Connection conn = handler.getConnection();

			MySQLQueryHandler streamHandler = MysqlUtils.getMysqlHandler(conf,
					"*");
			Connection streamConn = streamHandler.getConnection();

			RankedColumns rankedColumns = new RankedColumns();

			ArrayList<AddedColumn> rankedListOfColumns = rankedColumns
					.getRankedListOfAddableColumnsFilteredByPgId(streamConn,
							conn, pgId, tableId, mPgId, data, columnRanker);
			conn.close();
			streamConn.close();

			return rankedListOfColumns;

		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return null;
	}

	private ArrayList<AddedColumn> getAdditionalColumnsFilteredByKeyCol(
			Integer pgId, Integer tableId, Integer keyColId) {

		try {

			Instances data = (Instances) context.getAttribute("featureSet");
			Ranker columnRanker = (Ranker) context.getAttribute("columnRanker");

			ConfigReader conf = (ConfigReader) context
					.getAttribute("configReader");

			MySQLQueryHandler handler = MysqlUtils.getMysqlHandler(conf, "*");
			Connection conn = handler.getConnection();

			MySQLQueryHandler streamHandler = MysqlUtils.getMysqlHandler(conf,
					"*");
			Connection streamConn = streamHandler.getConnection();

			MySQLQueryHandler graphConnHandler = MysqlUtils.getMysqlHandler(
					conf, "graph");
			Connection graphConn = graphConnHandler.getConnection();

			RankedColumns rankedColumns = new RankedColumns();

			ArrayList<AddedColumn> rankedListOfColumns = rankedColumns
					.getRankedListOfAddableColumnsFilteredByKeyCol(streamConn,
							conn, graphConn, pgId, tableId, keyColId, data,
							columnRanker);
			conn.close();
			streamConn.close();

			return rankedListOfColumns;

		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return null;
	}

}
