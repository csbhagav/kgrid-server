package edu.northwestern.cs.kgrid.server;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.sql.Connection;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;

import weka.classifiers.Classifier;
import weka.classifiers.functions.Logistic;
import weka.core.Instances;
import ciir.umass.edu.learning.RankList;
import ciir.umass.edu.learning.Ranker;
import ciir.umass.edu.learning.RankerFactory;
import ciir.umass.edu.metric.METRIC;
import ciir.umass.edu.metric.MetricScorer;
import ciir.umass.edu.metric.MetricScorerFactory;
import edu.northwestern.cs.WIG.lib.MySQLQueryHandler;
import edu.northwestern.cs.kgrid.config.ConfigReader;
import edu.northwestern.cs.kgrid.ml.DataLabels;
import edu.northwestern.cs.kgrid.ml.LogRegModel;
import edu.northwestern.cs.kgrid.server.models.LabeledDataType;
import edu.northwestern.cs.kgrid.server.models.RankingType;
import edu.northwestern.cs.kgrid.server.utils.KGridModelUtils;
import edu.northwestern.cs.kgrid.server.utils.MysqlUtils;

@Path("/model/")
public class Model {

	@Context
	ServletContext context;

	@Context
	HttpServletRequest req;

	@SuppressWarnings("unchecked")
	@GET
	@Produces("application/json; charset=UTF-8;")
	@Path("reload")
	public String reloadModel() {

		System.out.println("LOGGER:" + req.getRemoteAddr() + " - "
				+ req.getPathInfo());

		try {
			ConfigReader conf = (ConfigReader) context
					.getAttribute("configReader");

			MySQLQueryHandler handler = MysqlUtils.getMysqlHandler(conf, "*");
			Connection conn = handler.getConnection();

			Instances data = (Instances) context.getAttribute("featureSet");

			ArrayList<DataLabels> labelledData = KGridModelUtils
					.getLabeledData(handler.getConnection(),
							LabeledDataType.TRAIN);
			ArrayList<String[]> features = KGridModelUtils
					.getColumnFeaturesForClassifer(handler.getConnection(),
							data, labelledData);
			LogRegModel.initModel(conn, features, data);

			String retStr = LogRegModel.getLogReg().toString() + "\n\n\n"
					+ "Num Training examples = "
					+ LogRegModel.getNumTrainingData() + "\n\n\n" + data
					+ "\n\n\n";

			ArrayList<ArrayList<String>> columnRankerTrainData = KGridModelUtils
					.getColumnFeaturesForRanker(handler.getConnection(), data,
							labelledData);
			HashSet<String> columRankFeatures = (HashSet<String>) context
					.getAttribute("columRankFeatures");
			Ranker columnRanker = KGridModelUtils.learnColumnRankerRanker(
					handler.getConnection(), columRankFeatures,
					columnRankerTrainData);
			context.setAttribute("columnRanker", columnRanker);
			retStr += "Column Ranker Model \n" + columnRanker.toString()
					+ "\n\n\n" + columnRanker.model() + "\n\n";

			HashSet<String> rankerFeatures = (HashSet<String>) context
					.getAttribute("rankerFeatures");
			Ranker r = KGridModelUtils.learnTableSearchRanker(
					handler.getConnection(), rankerFeatures);
			retStr += "Table Search Model details \n\n" + r.toString()
					+ "\n\n\n" + r.model() + "\n\n";
			context.setAttribute("rankingLib", r);

			conn.close();
			return retStr;

		} catch (Exception e) {
			e.printStackTrace();
		}
		return "Failed";
	}

	// @GET
	// @Produces("application/json; charset=UTF-8;")
	// @Path("reload/limit/{limit}/mode/{mode}")
	// public String reloadModelLimit(@PathParam("limit") Integer limit ,
	// @PathParam("mode") String mode) {
	//
	// System.out.println("LOGGER:" + req.getRemoteAddr() + " - "
	// + req.getPathInfo());
	//
	// try {
	// ConfigReader conf = (ConfigReader) context
	// .getAttribute("configReader");
	//
	// MySQLQueryHandler handler = MysqlUtils.getMysqlHandler(conf, "*");
	// Connection conn = handler.getConnection();
	//
	// Instances data = (Instances) context.getAttribute("featureSet");
	// EvalLogRegModel.initModelLimit(conn, data, limit , mode);
	//
	// LogRegModel.setLogReg(EvalLogRegModel.getLogReg());
	// LogRegModel
	// .setNumTrainingData(EvalLogRegModel.getNumTrainingData());
	//
	// return EvalLogRegModel.getLogReg().toString() + "\n\n\n"
	// + "Num Training examples = "
	// + EvalLogRegModel.getNumTrainingData() + "\n\n\n" + data;
	// } catch (Exception e) {
	// e.printStackTrace();
	// }
	// return "Failed";
	// }

	@GET
	@Produces("application/json; charset=UTF-8;")
	@Path("view")
	public String viewModel() {

		System.out.println("LOGGER:" + req.getRemoteAddr() + " - "
				+ req.getPathInfo());
		String retStr = LogRegModel.getLogReg().toString() + "\n\n\n"
				+ "Num Training examples = " + LogRegModel.getNumTrainingData();

		Ranker columnRanker = (Ranker) context.getAttribute("columnRanker");
		retStr += "\n\nColumn Ranker Model \n\n\n" + columnRanker.model();

		Ranker tableRanker = (Ranker) context.getAttribute("rankingLib");
		retStr += "\n\nTable Search Ranker Model \n\n\n" + tableRanker.model();

		return retStr;
	}

	@GET
	@Produces("application/json; charset=UTF-8;")
	@Path("save/columnRankerModel/")
	public String saveColumnRankerModel() {

		Ranker columnRanker = (Ranker) context.getAttribute("columnRanker");
		columnRanker.save("");

		return "";
	}

	@SuppressWarnings("unchecked")
	@GET
	@Produces("application/json; charset=UTF-8;")
	@Path("test/columnRankModel/")
	public String testColumnRankModel() {

		try {
			ConfigReader conf = (ConfigReader) context
					.getAttribute("configReader");

			MySQLQueryHandler handler = MysqlUtils.getMysqlHandler(conf, "*");
			Connection conn = handler.getConnection();

			Instances data = (Instances) context.getAttribute("featureSet");

			ArrayList<DataLabels> labelledData = KGridModelUtils
					.getLabeledData(conn, LabeledDataType.TEST);

			ArrayList<ArrayList<String>> columnRankerTestData = KGridModelUtils
					.getColumnFeaturesForRanker(handler.getConnection(), data,
							labelledData);

			Ranker columnRanker = (Ranker) context.getAttribute("columnRanker");

			List<RankList> lrl = KGridModelUtils
					.convertColumnRankerDataToRankList(columnRankerTestData);

			List<RankList> rankedByRanker = columnRanker.rank(lrl);
			MetricScorerFactory mFact = new MetricScorerFactory();

			MetricScorer scorer = mFact.createScorer("NDCG@4");
			scorer.setK(5);
			Double originalNdcgScore = scorer.score(lrl);
			Double ndcgTestSetScore = scorer.score(rankedByRanker);

			MetricScorer mapScorer = mFact.createScorer(METRIC.MAP);
			mapScorer.setK(5);
			Double originalMapScore = mapScorer.score(lrl);
			Double mapScore = mapScorer.score(rankedByRanker);

			MetricScorer dcgScorer = mFact.createScorer(METRIC.DCG);
			dcgScorer.setK(5);
			Double originalDcgScore = dcgScorer.score(lrl);
			Double dcgScore = dcgScorer.score(rankedByRanker);

			int numRankLists = lrl.size();
			int totalLabels = labelledData.size();
			// columnRanker.setTestSet(lrl);

			String originalScores = "\n\nNDCG@4 : " + originalNdcgScore + "\n"
					+ "MAP : " + originalMapScore + "\nDCG : "
					+ originalDcgScore + "\n\n";

			String testScores = "\n\nNDCG@4 : " + ndcgTestSetScore + "\n"
					+ "MAP : " + mapScore + "\nDCG : " + dcgScore + "\n\n";

			String retStr = "Original scores : " + originalScores
					+ "Test Results : " + testScores + numRankLists
					+ "\nTotal labelled data size:" + totalLabels
					+ "\nColumn Ranker Model \n\n\n" + columnRanker.model();

			System.out.println(retStr);
			conn.close();
			return retStr;

		} catch (Exception e) {
			e.printStackTrace();
		}
		return "Failed";

	}

	@GET
	@Produces("application/json; charset=UTF-8;")
	@Path("rankingType/{type}/")
	public String setRankingType(@PathParam("type") String type) {

		String ret = "";
		if (type.equalsIgnoreCase("RANDOM")
				|| type.equalsIgnoreCase("MATCH_PERC")
				|| type.equalsIgnoreCase("CLSFR_AND_RANKER")) {
			context.setAttribute("rankingType", RankingType.valueOf(type));
			ret = "Ranking type Set to - " + type;
		} else {

			ret = "Ranking type must be one of {" + RankingType.RANDOM + " , "
					+ RankingType.MATCH_PERC + " , "
					+ RankingType.CLSFR_AND_RANKER + "}";
		}

		return ret;
	}

	@GET
	@Produces("application/json; charset=UTF-8;")
	@Path("rankingType")
	public String getRankingType(@PathParam("type") String type) {

		return ((RankingType) context.getAttribute("rankingType")).toString();
	}

	@GET
	@Produces("application/json; charset=UTF-8;")
	@Path("save/columnClassifierModel/")
	public String saveColumnClassifierModel() {

		try {
			Logistic l = LogRegModel.getLogReg();
			ObjectOutputStream oos = new ObjectOutputStream(
					new FileOutputStream("classifier-"));

			oos.writeObject(l);
			oos.flush();
			oos.close();

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return "";
	}

	@GET
	@Produces("application/json; charset=UTF-8;")
	@Path("rip")
	public String viewRemoteAddr() {

		System.out.println("LOGGER:" + req.getRemoteAddr() + " - "
				+ req.getPathInfo());

		return req.getRemoteHost() + "-" + req.getRemoteAddr() + "-"
				+ req.getRemoteUser() + req.getQueryString()
				+ req.getPathInfo();
	}

	@GET
	@Produces("application/json; charset=UTF-8;")
	@Path("useEvaluatedModel")
	public String loadModelUsedForEval() {

		try {
			ConfigReader conf = (ConfigReader) context
					.getAttribute("configReader");

			MySQLQueryHandler handler = MysqlUtils.getMysqlHandler(conf, "*");
			Connection conn = handler.getConnection();

			Instances data = (Instances) context.getAttribute("featureSet");

			ArrayList<DataLabels> labelledData = KGridModelUtils
					.getLabeledData(handler.getConnection(),
							LabeledDataType.EVAL_DATA);
			ArrayList<String[]> features = KGridModelUtils
					.getColumnFeaturesForClassifer(handler.getConnection(),
							data, labelledData);
			LogRegModel.initModel(conn, features, data);

			String retStr = LogRegModel.getLogReg().toString() + "\n\n\n"
					+ "Num Training examples = "
					+ LogRegModel.getNumTrainingData() + "\n\n\n" + data
					+ "\n\n\n";

			RankerFactory rf = new RankerFactory();
			Ranker columnRanker = rf
					.loadRanker("/disk2/csbhagav/kgrid_aug12/testCheckPoints/models/modelUsedForEval");
			context.setAttribute("columnRanker", columnRanker);
			retStr += "Column Ranker Model \n" + columnRanker.toString()
					+ "\n\n\n" + columnRanker.model() + "\n\n\n";

			Ranker tableSearchRanker = rf
					.loadRanker("/disk2/csbhagav/kgrid_aug12/testCheckPoints/models/tableSearchModelEval");
			context.setAttribute("rankingLib", tableSearchRanker);
			retStr += "Table Search Ranker Model \n"
					+ tableSearchRanker.toString() + "\n\n\n"
					+ tableSearchRanker.model();

			conn.close();
			return retStr;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return "Failed";

	}

	@GET
	@Produces("application/json; charset=UTF-8;")
	@Path("saveCurrentModels")
	public String saveCurrentModels() throws FileNotFoundException, IOException {

		DateFormat dateFormat = new SimpleDateFormat("yy-MM-dd_HH-mm-");
		Date date = new Date();
		String logRegSuffix = dateFormat.format(date) + "logReg.model";
		String columnRankerSuffix = dateFormat.format(date) + "colRanker.model";
		String tableSearchModelSuffix = dateFormat.format(date)
				+ "tableSearch.model";
		String logRegModelName = "/disk2/csbhagav/kgrid_aug12/savedModels/"
				+ logRegSuffix;
		String columnRankerModelName = "/disk2/csbhagav/kgrid_aug12/savedModels/"
				+ columnRankerSuffix;
		String tableSearcgModelName = "/disk2/csbhagav/kgrid_aug12/savedModels/"
				+ tableSearchModelSuffix;

		ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(
				logRegModelName));
		oos.writeObject(LogRegModel.getLogReg());

		Ranker columnRanker = (Ranker) context.getAttribute("columnRanker");
		columnRanker.save(columnRankerModelName);

		Ranker tableRanker = (Ranker) context.getAttribute("rankingLib");
		tableRanker.save(tableSearcgModelName);

		return "Saved Models: \nLogistic Regression: " + logRegSuffix
				+ "\nColumn Ranker: " + columnRankerSuffix
				+ "\nTableSearch Ranker: " + tableRanker;

	}

	@GET
	@Produces("application/json; charset=UTF-8;")
	@Path("loadModel/prefix/{prefix}")
	public String loadSavedModels(@PathParam("prefix") String prefix)
			throws FileNotFoundException, IOException, ClassNotFoundException {

		String logRegModelName = "/disk2/csbhagav/kgrid_aug12/savedModels/"
				+ prefix + "-logReg.model";
		String colRankerModelName = "/disk2/csbhagav/kgrid_aug12/savedModels/"
				+ prefix + "-colRanker.model";
		String tableSearcgModelName = "/disk2/csbhagav/kgrid_aug12/savedModels/"
				+ prefix + "-tableSearch.model";
		ObjectInputStream ois = new ObjectInputStream(new FileInputStream(
				logRegModelName));
		Classifier cls = (Classifier) ois.readObject();
		ois.close();

		RankerFactory rf = new RankerFactory();
		Ranker columnRanker = rf.loadRanker(colRankerModelName);
		context.setAttribute("columnRanker", columnRanker);

		Ranker tableRanker = rf.loadRanker(tableSearcgModelName);
		context.setAttribute("rankingLib", tableRanker);

		return "Done";
	}

	@GET
	@Produces("application/json; charset=UTF-8;")
	@Path("removeFeature/name/{name}")
	public String removeFeatureAndReLoad(@PathParam("name") String name) {

		Instances data = (Instances) context.getAttribute("featureSet");
		if (data.attribute(name) != null) {
			data.deleteAttributeAt(data.attribute(name).index());
		}
		data.delete();

		// context.setAttribute("featureSet", data);

		HashSet<String> columnRankerFeatures = (HashSet<String>) context
				.getAttribute("columRankFeatures");
		columnRankerFeatures.remove(name);

		// reloadModel();

		return "Done. Goto URL:\n /model/reload\n\n\n" + "*** \n\n"
				+ "New Feature Set :\n\n" + data;
	}

	@GET
	@Produces("application/json; charset=UTF-8;")
	@Path("restoreAllFeatures")
	public String restoreFeaturesAndReLoad() throws IOException {

		InputStream is = this.getClass().getResourceAsStream("/features.arff");
		KGridModelUtils.storeFeatureSetInContext(context, is);

		InputStream colRankFeaturesIs = this.getClass().getResourceAsStream(
				"/columRankFeatures");
		KGridModelUtils.storeColumnRankerFeatures(context, colRankFeaturesIs);

		is.close();
		colRankFeaturesIs.close();

		reloadModel();

		return "Done";
	}

}
