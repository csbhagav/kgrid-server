package edu.northwestern.cs.kgrid.server;

import ciir.umass.edu.features.Normalizer;
import ciir.umass.edu.features.ZScoreNormalizor;
import ciir.umass.edu.learning.DataPoint;
import ciir.umass.edu.learning.RankList;
import ciir.umass.edu.learning.Ranker;
import ciir.umass.edu.learning.RankerFactory;
import ciir.umass.edu.metric.MetricScorerFactory;
import edu.northwestern.cs.WIG.lib.MySQLQueryHandler;
import edu.northwestern.cs.kgrid.config.ConfigReader;
import edu.northwestern.cs.kgrid.search.YHttpRequest;
import edu.northwestern.cs.kgrid.server.models.SearchResult;
import edu.northwestern.cs.kgrid.server.models.TitleSearchResult;
import edu.northwestern.cs.kgrid.server.utils.MysqlUtils;
import edu.northwestern.cs.kgrid.server.utils.SearchUtils;
import edu.northwestern.edu.cs.tableSearch.TableSearchFeatures;
import edu.northwestern.edu.cs.tableSearch.TableSearchUtils;
import oauth.signpost.OAuthConsumer;
import oauth.signpost.basic.DefaultOAuthConsumer;
import oauth.signpost.exception.OAuthCommunicationException;
import oauth.signpost.exception.OAuthExpectationFailedException;
import oauth.signpost.exception.OAuthMessageSignerException;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;

@Path("/searchTitle/")
public class PageTitleService {

    protected static String yahooServer = "http://yboss.yahooapis.com/ysearch/";
    private static String consumer_key = "dj0yJmk9ZWZjbHhEV2RWSXZBJmQ9WVdrOWFEYzFaMlpuTXpJbWNHbzlNVGMwTnpVM01qQTJNZy0tJnM9Y29uc3VtZXJzZWNyZXQmeD04NQ--";
    private static String consumer_secret = "d54f741704ef95166e836e2e6e17367acd744b44";

    private static YHttpRequest httpRequest = new YHttpRequest();

    private static final String ENCODE_FORMAT = "UTF-8";

    private static final String callType = "web";

    private static final int HTTP_STATUS_OK = 200;

    protected RankerFactory rFact = new RankerFactory();

    protected MetricScorerFactory mFact = new MetricScorerFactory();

    @Context
    ServletContext context;

    @Context
    HttpServletRequest req;

    @GET
    @Produces("application/json; charset=UTF-8;")
    @Path("/{text}")
    public TitleSearchResult searchTitle(@PathParam("text") String searchString) {

        if (req != null)
            System.out.println("LOGGER:" + req.getRemoteAddr() + " - "
                    + req.getPathInfo());

        TitleSearchResult tsr = new TitleSearchResult();
        tsr.setQuery(searchString);
        try {
            ConfigReader conf = (ConfigReader) context
                    .getAttribute("configReader");

            MySQLQueryHandler handler = MysqlUtils.getMysqlHandler(conf, "*");
            Connection conn = handler.getConnection();

            PreparedStatement titleIDPS = conn
                    .prepareStatement("SELECT pg_id , title FROM wikiarticles WHERE title = ? "
                            + "UNION "
                            + "SELECT w.pg_id pg_id , w.title title "
                            + "FROM redirect r , page p1 , wikiarticles w "
                            + "WHERE p1.page_title = ? AND p1.page_namespace = 0 "
                            + "AND p1.page_id = r.rd_from AND r.rd_title = w.title ");

            OAuthConsumer consumer = new DefaultOAuthConsumer(consumer_key,
                    consumer_secret);
            httpRequest.setOAuthConsumer(consumer);

            boolean done = false;
            int start = 0;
            int count = 50;
            int resultCount = 0;
            int numPagesWithTable = 0;

            do {

                String params = callType;
                params = params.concat("?q=");
                params = params
                        .concat(URLEncoder.encode(searchString, "UTF-8"));

                params = params
                        .concat("&sites=en.wikipedia.org&format=json&count=50&start="
                                + start);
                String url = yahooServer + params;

                int responseCode = httpRequest.sendGetRequest(url);

                if (responseCode == HTTP_STATUS_OK) {

                    String jsonResponse = httpRequest.getResponseBody();

                    JSONObject jo = new JSONObject(jsonResponse);
                    if (jo.has("bossresponse")) {
                        jo = jo.getJSONObject("bossresponse");
                        if (jo.has("web")) {
                            jo = jo.getJSONObject("web");
                            if (jo.has("results")) {
                                JSONArray ja = jo.getJSONArray("results");
                                resultCount = ja.length();
                                for (int i = 0; i < ja.length(); i++) {

                                    String clickURL = ja.getJSONObject(i)
                                            .getString("clickurl");
                                    String[] parts = clickURL.split("\\/");
                                    String pageTitle = URLDecoder.decode(
                                            parts[parts.length - 1],
                                            ENCODE_FORMAT);

                                    titleIDPS.setBytes(1, pageTitle.getBytes());
                                    titleIDPS.setBytes(2, pageTitle.getBytes());
                                    System.out.println(titleIDPS);
                                    ResultSet titleIDRS = titleIDPS
                                            .executeQuery();

                                    while (titleIDRS.next()) {

                                        tsr.addTitleDataPair(new String(
                                                        titleIDRS.getBytes("title"),
                                                        "UTF-8").replaceAll("_", " "),
                                                titleIDRS.getString("pg_id"));
                                        numPagesWithTable++;

                                        if (numPagesWithTable >= 30) {
                                            done = true;
                                            break;
                                        }

                                    }

                                    if (done)
                                        break;
                                }
                            }
                        }
                    }
                }

                if (resultCount < count || tsr.getSuggestions().size() >= 30) {

                    done = true;
                }

                start += count;
            } while (!done);

            conn.close();

        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (OAuthMessageSignerException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (OAuthExpectationFailedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (OAuthCommunicationException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (InstantiationException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return tsr;

    }

    @GET
    @Produces("application/json; charset=UTF-8;")
    @Path("/{text}/matchingPgId/{pgId}/matchingTableId/{tableId}")
    public TitleSearchResult searchTitle(
            @PathParam("text") String searchString,
            @PathParam("pgId") Integer pgId,
            @PathParam("tableId") Integer tableId) {

        if (req != null)
            System.out.println("LOGGER:" + req.getRemoteAddr() + " - "
                    + req.getPathInfo());

        TitleSearchResult tsr = searchTitle(searchString);
        TitleSearchResult finalTsr = new TitleSearchResult();
        try {
            ConfigReader conf = (ConfigReader) context
                    .getAttribute("configReader");

            MySQLQueryHandler handler = MysqlUtils.getMysqlHandler(conf, "*");

            Connection conn = handler.getConnection();

            PreparedStatement checkPS = conn
                    .prepareStatement("SELECT count(*) as c FROM column_matches WHERE "
                            + "(src_pg_id = ? AND src_table_id = ? AND tgt_pg_id = ? ) OR "
                            + "(tgt_pg_id = ? AND tgt_table_id = ? AND src_pg_id = ? )");

            ArrayList<String> data = tsr.getData();
            ArrayList<String> suggestion = tsr.getSuggestions();

            ResultSet rs;
            for (int i = 0; i < data.size(); i++) {

                checkPS.setInt(1, pgId);
                checkPS.setInt(2, tableId);
                checkPS.setInt(3, Integer.valueOf(data.get(i)));

                checkPS.setInt(4, pgId);
                checkPS.setInt(5, tableId);
                checkPS.setInt(6, Integer.valueOf(data.get(i)));

                rs = checkPS.executeQuery();

                while (rs.next()) {
                    int count = rs.getInt("c");
                    if (count > 0) {
                        finalTsr.addTitleDataPair(suggestion.get(i),
                                data.get(i));
                    }
                }

                finalTsr.setQuery(tsr.getQuery());
            }

        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return finalTsr;
    }

    @GET
    @Produces("application/json; charset=UTF-8;")
    @Path("/random/limit/{limit}")
    public TitleSearchResult getRandomTitles(@PathParam("limit") Integer limit) {

        System.out.println("LOGGER:" + req.getRemoteAddr() + " - "
                + req.getPathInfo());
        TitleSearchResult tsr = new TitleSearchResult();
        tsr.setQuery("random-set");
        ArrayList<String> articles = new ArrayList<String>();
        HashMap<String, ArrayList<String>> result = new HashMap<String, ArrayList<String>>();
        try {

            ConfigReader conf = (ConfigReader) context
                    .getAttribute("configReader");

            MySQLQueryHandler handler = MysqlUtils.getMysqlHandler(conf, "*");

            Connection conn = handler.getConnection();

            PreparedStatement ps = conn
                    .prepareStatement("SELECT wa.title title , wa.pg_id id FROM popular_articles p , wikiarticles wa "
                            + "WHERE p.title = wa.title ORDER BY RAND() LIMIT ?");

            ps.setInt(1, limit);
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {

                tsr.addTitleDataPair(new String(rs.getBytes("title"), "UTF-8"),
                        rs.getString("id"));

            }
            rs.close();
            ps.close();
            conn.close();

        } catch (ClassNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (InstantiationException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        result.put("randomPages", articles);
        return tsr;
    }

    @GET
    @Produces("application/json; charset=UTF-8;")
    @Path("/eval/limit/{limit}")
    public TitleSearchResult getTestSetTitles(@PathParam("limit") Integer limit) {

        System.out.println("LOGGER:" + req.getRemoteAddr() + " - "
                + req.getPathInfo());
        TitleSearchResult tsr = new TitleSearchResult();
        tsr.setQuery("random-set");
        ArrayList<String> articles = new ArrayList<String>();
        HashMap<String, ArrayList<String>> result = new HashMap<String, ArrayList<String>>();
        try {

            ConfigReader conf = (ConfigReader) context
                    .getAttribute("configReader");

            MySQLQueryHandler handler = MysqlUtils.getMysqlHandler(conf, "*");

            Connection conn = handler.getConnection();

            PreparedStatement ps = conn
                    .prepareStatement("SELECT wa.title title , wa.pg_id id FROM test_set p , wikiarticles wa "
                            + "WHERE p.title = wa.title ORDER BY wa.title LIMIT ?");

            ps.setInt(1, limit);
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {

                tsr.addTitleDataPair(new String(rs.getBytes("title"), "UTF-8"),
                        rs.getString("id"));

            }
            conn.close();
        } catch (ClassNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (InstantiationException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        result.put("randomPages", articles);
        return tsr;
    }

    @GET
    @Produces("application/json; charset=UTF-8;")
    @Path("/tableSearch/{text}")
    public ArrayList<SearchResult> performTableSearch(
            @PathParam("text") String searchString) {
        ArrayList<SearchResult> results = new ArrayList<SearchResult>();
        TitleSearchResult tsr = searchTitle(searchString);

        ArrayList<String> suggestions = tsr.getSuggestions();
        ArrayList<String> pageIds = tsr.getData();

        try {

            HashSet<String> rankerFeature = (HashSet<String>) context
                    .getAttribute("rankerFeatures");
            ConfigReader conf = (ConfigReader) context
                    .getAttribute("configReader");

            MySQLQueryHandler handler = MysqlUtils.getMysqlHandler(conf, "*");

            Connection conn = handler.getConnection();

            PreparedStatement tablesForPagePS = conn
                    .prepareStatement("SELECT wt.pg_id mwPgId , ta.id mwTableId , wt.table_id , wt.title "
                            + "FROM wikitables wt , table_attrs ta WHERE wt.pg_id = ?  "
                            + "AND ta.pg_id = wt.pg_id AND ta.wikitable_id  = wt.table_id  ORDER BY section ASC ;");
            ResultSet tablesForPageRs = null;
            String domain = "http://en.wikipedia.org/wiki/";
            String tableSection = "";
            Integer tableId = 0;

            HashMap<Integer, SearchResult> tidSearchResultMap = new HashMap<Integer, SearchResult>();
            RankList testRankList = new RankList();
            String featureStr = "";
            int idx = 1;
            boolean featuresPrinted = false;
            for (int i = 0; i < suggestions.size(); i++) {
                tablesForPagePS.setInt(1, Integer.valueOf(pageIds.get(i)));
                tablesForPageRs = tablesForPagePS.executeQuery();
                while (tablesForPageRs.next()) {
                    tableSection = new String(
                            tablesForPageRs.getBytes("title"), "UTF-8");
                    tableSection = tableSection.replaceAll(" ", "_");
                    tableId = tablesForPageRs.getInt("table_id");

                    // getTableDetails(conn, tableId);

                    tidSearchResultMap.put(tableId, new SearchResult(
                            suggestions.get(i) + " - " + tableSection, domain
                            + suggestions.get(i) + "#" + tableSection,
                            SearchUtils.getTableDetails(conn, tableId, 0),
                            tableId, tablesForPageRs.getInt("mwPgId"),
                            tablesForPageRs.getInt("mwTableId")));
                    featureStr = "0"
                            + " qid:0 "
                            + TableSearchUtils.generateFeatures(conn,
                            searchString, tableId, idx++)
                            .getFeatureString(rankerFeature)
                            + " # tid:" + tableId;
                    testRankList.add(new DataPoint(featureStr));

                }

            }
            if (tablesForPageRs != null)
                tablesForPageRs.close();
            tablesForPagePS.close();

            Ranker r = (Ranker) context.getAttribute("rankingLib");

//			int[] features = new int[testRankList.get(0).getFeatureCount()];
//			for (int i = 0; i < testRankList.get(0).getFeatureCount(); i++) {
//				features[i] = i + 1;
//			}
            int[] features = new int[r.getFeatures().length];
            for (int i = 0; i < r.getFeatures().length; i++) {
                features[i] = i;
            }


            Normalizer nml = new ZScoreNormalizor();
            nml.normalize(testRankList, features);
            RankList rankedTest = r.rank(testRankList);

            Integer tid = 0;
            for (int i = 0; i < rankedTest.size(); i++) {
                tid = Integer.valueOf(rankedTest.get(i).toString()
                        .split("tid:")[1]);
                results.add(tidSearchResultMap.get(tid));
            }

            conn.close();
        } catch (InstantiationException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return results;
    }

    // private Ranker learnRanker(Connection conn, String searchString)
    // throws UnsupportedEncodingException, SQLException {
    //
    // Ranker r = rFact
    // .createRanker(ciir.umass.edu.learning.RANKER_TYPE.COOR_ASCENT);
    // RankList rl = new RankList();
    //
    // PreparedStatement qidsPS = conn
    // .prepareStatement("SELECT qid, query FROM queries WHERE type = ?");
    // qidsPS.setString(1, "train");
    // ResultSet qidRS = qidsPS.executeQuery();
    // String query = "";
    // Integer qid = 0;
    // List<RankList> lrl = new ArrayList<RankList>();
    //
    // while (qidRS.next()) {
    // query = qidRS.getString("query");
    // qid = qidRS.getInt("qid");
    // lrl.add(SearchUtils.getTrainingRankList(conn, query, qid, "train"));
    // }
    //
    // int[] features = new int[lrl.get(0).get(0).getFeatureCount()];
    // System.out.println("length = " + lrl.get(0).get(0).getFeatureCount());
    // for (int i = 0; i < lrl.get(0).get(0).getFeatureCount(); i++) {
    // features[i] = i + 1;
    // }
    //
    // r.set(lrl, features);
    // r.set(mFact.createScorer(METRIC.MAP));
    // ERRScorer.MAX = 5.0;
    // r.init();
    // r.learn();
    //
    // return r;
    // }

    @GET
    @Produces("application/json; charset=UTF-8;")
    @Path("/generateFeatures/test")
    public String getTestingFeatures() {
        String testingSetFeatures = "";

        try {
            HashSet<String> rankerFeature = (HashSet<String>) context
                    .getAttribute("rankerFeature");
            ConfigReader conf = (ConfigReader) context
                    .getAttribute("configReader");
            MySQLQueryHandler handler = MysqlUtils.getMysqlHandler(conf, "*");
            Connection conn = handler.getConnection();
            PreparedStatement qidsPS = conn
                    .prepareStatement("SELECT qid, query FROM queries WHERE type = ?");
            qidsPS.setString(1, "test");
            ResultSet qidsRS = qidsPS.executeQuery();

            Integer qid = 0;
            String query = "";
            Integer tableId = 0;
            Double rating = 0.0;

            while (qidsRS.next()) {

                qid = qidsRS.getInt("qid");
                query = qidsRS.getString("query");

                ArrayList<SearchResult> results = performTableSearch(query
                        .replaceAll(" ", "+"));

                for (int i = 0; i < results.size(); i++) {
                    tableId = results.get(i).getWikitableId();

                    TableSearchFeatures tsf = TableSearchUtils
                            .generateFeatures(conn, query, tableId, 0);
                    testingSetFeatures += rating + " " + "qid:" + qid + " "
                            + tsf.getFeatureString(rankerFeature) + " # " + qid
                            + " " + tableId + "\n";

                }

            }

        } catch (InstantiationException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return testingSetFeatures;

    }

    @SuppressWarnings("unchecked")
    @GET
    @Produces("application/json; charset=UTF-8;")
    @Path("/tableSearch/getTables/entity/{entityName}/property/{property}")
    public ArrayList<SearchResult> getEntityPropertyTables(
            @PathParam("entityName") String entityName,
            @PathParam("property") String property) {

        ArrayList<SearchResult> results = new ArrayList<SearchResult>();

        String domain = "http://en.wikipedia.org/wiki/";

        // ArrayList<String> propList = new ArrayList<String>();

        HashMap<Integer, SearchResult> tidSearchResultMap = new HashMap<Integer, SearchResult>();
        HashSet<String> rankerFeature = (HashSet<String>) context
                .getAttribute("rankerFeatures");

        try {
            String searchString = entityName + "+" + property;

            entityName = entityName.replaceAll("\\+", " ");
            property = property.replaceAll("\\+", " ");
            ConfigReader conf = (ConfigReader) context
                    .getAttribute("configReader");
            MySQLQueryHandler handler = MysqlUtils.getMysqlHandler(conf, "*");
            Connection conn = handler.getConnection();

            TitleSearchResult tsr = searchTitle(searchString);
            ArrayList<String> suggestions = tsr.getSuggestions();
            HashMap<String, Integer> suggestionRank = new HashMap<String, Integer>();
            for (int i = 0; i < suggestions.size(); i++) {
                suggestionRank.put(suggestions.get(i), i);
            }

            RankList testRankList = new RankList();

            // PreparedStatement getTablesPS = conn
            // .prepareStatement("SELECT "
            // + "td.pg_id , td.table_id , td.row_idx  , th.col_idx , "
            // + "wa.title , ta.caption , ta.wikitable_id  "
            // + "FROM tokens t , text_revidx tr , table_data td , "
            // + "table_headers th , cell_details cd  , wikiarticles wa , "
            // + "table_attrs ta "
            // + "WHERE t.text = ? AND t.id = tr.textID "
            // + "AND tr.cellID = td.cellID AND td.pg_id = th.pg_id "
            // + "AND td.table_id = th.table_id AND cd.cellID = th.cellID "
            // + "AND LOWER(CONVERT(cd.text USING utf8)) LIKE ? "
            // + "AND wa.pg_id = td.pg_id AND ta.pg_id = wa.pg_id "
            // + "AND ta.id = td.table_id "
            // + "GROUP BY th.pg_id , th.table_id , th.col_idx");

            PreparedStatement getTablesPS = conn
                    .prepareStatement("SELECT "
                            + "td.pg_id , td.table_id , td.row_idx  , th.col_idx , "
                            + "wa.title , ta.caption , wikitable_id , cd2.text  "
                            + "FROM tokens t , text_revidx tr , table_data td , "
                            + "table_headers th , cell_details cd  , wikiarticles wa , "
                            + "table_attrs ta , table_data td2 , cell_details cd2  "
                            + "WHERE t.text = ? AND t.id = tr.textID AND tr.cellID = td.cellID "
                            + "AND td.pg_id = th.pg_id AND td.table_id = th.table_id "
                            + "AND cd.cellID = th.cellID AND LOWER(CONVERT(cd.text USING utf8)) LIKE ? "
                            + "AND wa.pg_id = td.pg_id AND ta.pg_id = wa.pg_id AND ta.id = td.table_id "
                            + "AND td2.pg_id = td.pg_id AND td2.table_id = td.table_id "
                            + "AND td2.row_idx = td.row_idx AND td2.col_idx = th.col_idx "
                            + "AND td2.cellID = cd2.cellID AND cd2.text != ? "
                            + "GROUP BY th.pg_id , th.table_id");

            getTablesPS.setBytes(1, entityName.getBytes("utf-8"));
            getTablesPS.setBytes(2,
                    ("%" + property.toLowerCase() + "%").getBytes("utf-8"));
            getTablesPS.setString(3, "");

            ResultSet getTablesRs = getTablesPS.executeQuery();
            Integer pgId = 0;
            Integer tableId = 0;
            Integer rowIdx = 0;
            Integer colIdx = 0;
            String text = "";
            Integer wtId = 0;
            String featureStr = "";
            String pgTitle = "";

            while (getTablesRs.next()) {
                pgId = getTablesRs.getInt("pg_id");
                tableId = getTablesRs.getInt("table_id");
                rowIdx = getTablesRs.getInt("row_idx");
                colIdx = getTablesRs.getInt("col_idx");
                wtId = getTablesRs.getInt("wikitable_id");
                pgTitle = new String(getTablesRs.getBytes("title"));
                text = pgTitle + "#"
                        + new String(getTablesRs.getBytes("caption"), "utf-8");

                SearchResult r = new SearchResult(text, domain + text,
                        SearchUtils.getTableDetails(conn, wtId, rowIdx), wtId,
                        pgId, tableId);
                r.setMwColIdx(colIdx);

                tidSearchResultMap.put(wtId, r);

                if (suggestionRank.containsKey(pgTitle)) {
                    featureStr = "0"
                            + " qid:0 "
                            + TableSearchUtils.generateFeatures(conn,
                            searchString, tableId,
                            suggestionRank.get(pgTitle))
                            .getFeatureString(rankerFeature)
                            + " # tid:" + wtId;
                    testRankList.add(new DataPoint(featureStr));
                } else {
                    featureStr = "0"
                            + " qid:0 "
                            + TableSearchUtils.generateFeatures(conn,
                            searchString, tableId, 100)
                            .getFeatureString(rankerFeature)
                            + " # tid:" + wtId;
                    testRankList.add(new DataPoint(featureStr));
                }

            }
            if (getTablesRs != null)
                getTablesRs.close();
            getTablesPS.close();

            if (testRankList.size() == 0)
                return results;

            Ranker r = (Ranker) context.getAttribute("rankingLib");

            int[] features = new int[testRankList.get(0).getFeatureCount()];
            for (int i = 0; i < testRankList.get(0).getFeatureCount(); i++) {
                features[i] = i + 1;
            }
            Normalizer nml = new ZScoreNormalizor();
            nml.normalize(testRankList, features);
            RankList rankedTest = r.rank(testRankList);

            Integer tid = 0;
            for (int i = 0; i < rankedTest.size(); i++) {
                tid = Integer.valueOf(rankedTest.get(i).toString()
                        .split("tid:")[1]);
                results.add(tidSearchResultMap.get(tid));
            }

            // for (int i = 0; i < results.size(); i++) {
            // results.get(i).setTempScore(
            // tempScoringTsResults(i + 1, results.get(i)
            // .getMwColIdx()));
            // }
            //
            // Collections.sort(results, new AdHocTsComparator());

            conn.close();

        } catch (IllegalAccessException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (InstantiationException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return results;
    }

    private double tempScoringTsResults(int rank, int colId) {
        double r1 = 1.0 / (double) rank;
        double r2 = 1.0 / (double) colId;
        return 2 * (double) r1 * r2 / (double) r1 + r2;
    }

}

class AdHocTsComparator implements Comparator<SearchResult> {

    public int compare(SearchResult o1, SearchResult o2) {
        if (o1.getTempScore() > o2.getTempScore())
            return -1;
        else if (o2.getTempScore() > o1.getTempScore())
            return 1;
        else
            return 0;

    }

}
