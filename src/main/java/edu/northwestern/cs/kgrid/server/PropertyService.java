package edu.northwestern.cs.kgrid.server;

import java.io.UnsupportedEncodingException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;

import edu.northwestern.cs.WIG.lib.MySQLQueryHandler;
import edu.northwestern.cs.kgrid.config.ConfigReader;
import edu.northwestern.cs.kgrid.server.models.EntityProperties;
import edu.northwestern.cs.kgrid.server.utils.MysqlUtils;

@Path("/properties/")
public class PropertyService {

	@Context
	ServletContext context;

	@Context
	HttpServletRequest req;
	
	@GET
	@Produces("application/json; charset=UTF-8;")
	@Path("/entity/{entity}")
	public ArrayList<EntityProperties> getProperties(
			@PathParam("entity") String entityString) {

		System.out.println("LOGGER:" + req.getRemoteAddr() + " - " + req.getPathInfo());

		ArrayList<EntityProperties> properties = new ArrayList<EntityProperties>();
		HashSet<Integer> cellIds = new HashSet<Integer>();

		HashMap<Integer, String> cellPageTitles = new HashMap<Integer, String>();
		entityString = entityString.replace("_", " ");
		try {
			ConfigReader conf = (ConfigReader) context
					.getAttribute("configReader");

			MySQLQueryHandler handler = MysqlUtils.getMysqlHandler(conf, "*");

			Connection conn = handler.getConnection();

			getEntityCellIDs(conn, entityString, cellIds, cellPageTitles);

			populateProperties(conn, cellIds, properties);

		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return properties;

	}

	private void populateProperties(Connection conn, HashSet<Integer> cellIds,
			ArrayList<EntityProperties> properties) throws SQLException,
			UnsupportedEncodingException {

		Iterator<Integer> it = cellIds.iterator();
		Integer cellId;

		while (it.hasNext()) {
			cellId = it.next();
			getPropertiesFromCellID(conn, properties, cellId);
		}

	}

	private void getPropertiesFromCellID(Connection conn,
			ArrayList<EntityProperties> properties, Integer cellId)
			throws SQLException, UnsupportedEncodingException {

		PreparedStatement metaDataPS = conn
				.prepareStatement("SELECT wa.title pgTitle, ta.caption caption "
						+ "FROM table_data td , wikiarticles wa , table_attrs ta "
						+ "WHERE td.cellID = ? AND td.pg_id = wa.pg_id "
						+ "AND td.pg_id = ta.pg_id and td.table_id = ta.id");
		metaDataPS.setInt(1, cellId);

		ResultSet rs = metaDataPS.executeQuery();
		String pageTitle = "";
		String tableCaption = "";
		while (rs.next()) {
			pageTitle = new String(rs.getBytes("pgTitle"), "UTF-8");
			tableCaption = new String(rs.getBytes("caption"), "UTF-8");
		}

		if (tableCaption.length() > 25)
			tableCaption = "";
		metaDataPS.close();
		rs.close();

		PreparedStatement keyValPS = conn
				.prepareStatement("SELECT cd1.text k , cd2.text v "
						+ "FROM table_data td1 , table_data td2 , "
						+ "table_headers th  , cell_details cd1 , cell_details cd2 "
						+ "WHERE td1.cellID = ? AND td1.pg_id = td2.pg_id "
						+ "AND td1.table_id = td2.table_id AND td1.row_idx = td2.row_idx "
						+ "AND td2.pg_id = th.pg_id AND td2.table_id = th.table_id "
						+ "AND td2.col_idx = th.col_idx AND th.cellID = cd1.cellID "
						+ "AND td2.cellID = cd2.cellID");
		keyValPS.setInt(1, cellId);
		rs = keyValPS.executeQuery();
		String k = "";
		String v = "";
		while (rs.next()) {
			k = new String(rs.getBytes("k"), "UTF-8");
			v = new String(rs.getBytes("v"), "UTF-8");
			if (k.equalsIgnoreCase("") || v.equalsIgnoreCase("")
					|| v.length() > 50)
				continue;
			properties.add(new EntityProperties(pageTitle, tableCaption, k, v));
		}

	}

	private void getEntityCellIDs(Connection conn, String entityString,
			HashSet<Integer> cellIds, HashMap<Integer, String> cellPageTitles)
			throws SQLException, UnsupportedEncodingException {

		String eStr = entityString.toLowerCase();
		PreparedStatement ps = conn
				.prepareStatement("SELECT er.cellID cellID, wa.title title FROM "
						+ "entity_revidx er , entities e , wikiarticles wa , table_data td  "
						+ "WHERE e.entity = ? AND e.entityID = er.entityID "
						+ "AND td.cellID = er.cellID and td.pg_id = wa.pg_id AND td.col_idx < 3 LIMIT 2000");

		ps.setBytes(1, eStr.getBytes("UTF-8"));
		ResultSet rs = ps.executeQuery();

		while (rs.next()) {
			cellIds.add(rs.getInt("cellID"));
			cellPageTitles.put(rs.getInt("cellID"),
					new String(rs.getBytes("title"), "UTF-8"));
		}
		rs.close();
		ps.close();

	}
}
