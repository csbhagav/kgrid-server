package edu.northwestern.cs.kgrid.server;

import java.io.UnsupportedEncodingException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;

import ciir.umass.edu.features.Normalizer;
import ciir.umass.edu.features.ZScoreNormalizor;
import ciir.umass.edu.learning.DataPoint;
import ciir.umass.edu.learning.RankList;
import ciir.umass.edu.learning.Ranker;

import edu.northwestern.cs.WIG.lib.MySQLQueryHandler;
import edu.northwestern.cs.kgrid.config.ConfigReader;
import edu.northwestern.cs.kgrid.server.models.SearchResult;
import edu.northwestern.cs.kgrid.server.models.TitleSearchResult;
import edu.northwestern.cs.kgrid.server.utils.MysqlUtils;
import edu.northwestern.cs.kgrid.server.utils.SearchUtils;
import edu.northwestern.edu.cs.tableSearch.TableSearchFeatures;
import edu.northwestern.edu.cs.tableSearch.TableSearchUtils;

@Path("/tableSearch/")
public class TableSearch {

	@Context
	ServletContext context;

	@Context
	HttpServletRequest req;

	@GET
	@Produces("application/json; charset=UTF-8;")
	@Path("/getEntityProperties/{entityName}")
	public ArrayList<String> getEntityProperties(
			@PathParam("entityName") String entityName) {

		ArrayList<String> propList = new ArrayList<String>();
		try {

			ConfigReader conf = (ConfigReader) context
					.getAttribute("configReader");
			MySQLQueryHandler handler = MysqlUtils.getMysqlHandler(conf, "*");
			Connection conn = handler.getConnection();

			conn.close();

		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return propList;
	}

//	@SuppressWarnings("unchecked")
//	@GET
//	@Produces("application/json; charset=UTF-8;")
//	@Path("/getTables/entity/{entityName}/property/{property}")
//	public ArrayList<SearchResult> getEntityPropertyTables(
//			@PathParam("entityName") String entityName,
//			@PathParam("property") String property) {
//
//		ArrayList<SearchResult> results = new ArrayList<SearchResult>();
//
//		String domain = "http://en.wikipedia.org/wiki/";
//
//		// ArrayList<String> propList = new ArrayList<String>();
//
//		HashMap<Integer, SearchResult> tidSearchResultMap = new HashMap<Integer, SearchResult>();
//		HashSet<String> rankerFeature = (HashSet<String>) context
//				.getAttribute("rankerFeatures");
//
//		try {
//
//			entityName = entityName.replaceAll("\\+", " ");
//			property = property.replaceAll("\\+", " ");
//			ConfigReader conf = (ConfigReader) context
//					.getAttribute("configReader");
//			MySQLQueryHandler handler = MysqlUtils.getMysqlHandler(conf, "*");
//			Connection conn = handler.getConnection();
//
//			String searchString = entityName + " " + property;
//			PageTitleService pts = new PageTitleService();
//			TitleSearchResult tsr = pts.searchTitle(searchString);
//			ArrayList<String> suggestions = tsr.getSuggestions();
//			HashMap<String, Integer> suggestionRank = new HashMap<String, Integer>();
//			for (int i = 0; i < suggestions.size(); i++) {
//				suggestionRank.put(suggestions.get(i), i);
//			}
//
//			RankList testRankList = new RankList();
//
//			PreparedStatement getTablesPS = conn
//					.prepareStatement("SELECT "
//							+ "td.pg_id , td.table_id , td.row_idx  , th.col_idx , "
//							+ "wa.title , ta.caption , ta.wikitable_id  "
//							+ "FROM tokens t , text_revidx tr , table_data td , "
//							+ "table_headers th , cell_details cd  , wikiarticles wa , "
//							+ "table_attrs ta "
//							+ "WHERE t.text = ? AND t.id = tr.textID "
//							+ "AND tr.cellID = td.cellID AND td.pg_id = th.pg_id "
//							+ "AND td.table_id = th.table_id AND cd.cellID = th.cellID "
//							+ "AND LOWER(CONVERT(cd.text USING utf8)) LIKE ? "
//							+ "AND wa.pg_id = td.pg_id AND ta.pg_id = wa.pg_id "
//							+ "AND ta.id = td.table_id "
//							+ "GROUP BY th.pg_id , th.table_id , th.col_idx");
//
//			getTablesPS.setBytes(1, entityName.getBytes("utf-8"));
//			getTablesPS.setBytes(2,
//					("%" + property.toLowerCase() + "%").getBytes("utf-8"));
//
//			ResultSet getTablesRs = getTablesPS.executeQuery();
//			Integer pgId = 0;
//			Integer tableId = 0;
//			Integer rowIdx = 0;
//			String text = "";
//			Integer wtId = 0;
//			String featureStr = "";
//			String pgTitle = "";
//
//			while (getTablesRs.next()) {
//				pgId = getTablesRs.getInt("pg_id");
//				tableId = getTablesRs.getInt("table_id");
//				rowIdx = getTablesRs.getInt("row_idx");
//				wtId = getTablesRs.getInt("wikitable_id");
//				pgTitle = new String(getTablesRs.getBytes("title"));
//				text = pgTitle + "#"
//						+ new String(getTablesRs.getBytes("caption"), "utf-8");
//
//				SearchResult r = new SearchResult(text, domain + text,
//						SearchUtils.getTableDetails(conn, wtId, rowIdx), wtId,
//						pgId, tableId);
//
//				tidSearchResultMap.put(wtId, r);
//
//				if (suggestionRank.containsKey(pgTitle)) {
//					featureStr = "0"
//							+ " qid:0 "
//							+ TableSearchUtils.generateFeatures(conn,
//									searchString, tableId,
//									suggestionRank.get(text)).getFeatureString(
//									rankerFeature) + " # tid:" + tableId;
//					testRankList.add(new DataPoint(featureStr));
//
//				}
//
//			}
//			if (getTablesRs != null)
//				getTablesRs.close();
//			getTablesPS.close();
//
//			Ranker r = (Ranker) context.getAttribute("rankingLib");
//
//			int[] features = new int[testRankList.get(0).getFeatureCount()];
//			for (int i = 0; i < testRankList.get(0).getFeatureCount(); i++) {
//				features[i] = i + 1;
//			}
//			Normalizer nml = new ZScoreNormalizor();
//			nml.normalize(testRankList, features);
//			RankList rankedTest = r.rank(testRankList);
//
//			Integer tid = 0;
//			for (int i = 0; i < rankedTest.size(); i++) {
//				tid = Integer.valueOf(rankedTest.get(i).toString()
//						.split("tid:")[1]);
//				results.add(tidSearchResultMap.get(tid));
//			}
//
//			conn.close();
//
//		} catch (IllegalAccessException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (ClassNotFoundException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (InstantiationException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (SQLException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (UnsupportedEncodingException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//
//		return results;
//	}

	@SuppressWarnings("unchecked")
	@GET
	@Produces("application/json; charset=UTF-8;")
	@Path("/generateFeatures/{type}")
	public String getTrainingFeatures(@PathParam("type") String type) {
		String trainingSetFeatures = "";

		try {
			HashSet<String> rankerFeature = (HashSet<String>) context
					.getAttribute("rankerFeatures");
			ConfigReader conf = (ConfigReader) context
					.getAttribute("configReader");
			MySQLQueryHandler handler = MysqlUtils.getMysqlHandler(conf, "*");
			Connection conn = handler.getConnection();

			PreparedStatement qidsPS = conn
					.prepareStatement("SELECT qid, query FROM queries WHERE type = ?");

			PreparedStatement tableIdsPS = conn
					.prepareStatement("SELECT rank , table_id , rating FROM ts_train WHERE qid = ? ");
			ResultSet tableIdsRS;

			qidsPS.setString(1, type);

			ResultSet qidsRS = qidsPS.executeQuery();
			Integer qid = 0;
			String query = "";
			Integer tableId = 0;
			Double rating = 0.0;
			Integer origRank = 0;
			while (qidsRS.next()) {

				qid = qidsRS.getInt("qid");
				query = qidsRS.getString("query");

				tableIdsPS.setInt(1, qid);
				tableIdsRS = tableIdsPS.executeQuery();
				while (tableIdsRS.next()) {
					tableId = tableIdsRS.getInt("table_id");
					origRank = tableIdsRS.getInt("rank");
					rating = tableIdsRS.getDouble("rating");
					TableSearchFeatures tsf = TableSearchUtils
							.generateFeatures(conn, query, tableId, origRank);
					trainingSetFeatures += rating + " " + "qid:" + qid + " "
							+ tsf.getFeatureString(rankerFeature) + " # "
							+ tableId + "\n";

				}

			}
			tableIdsPS.close();
			qidsPS.close();
			conn.close();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return trainingSetFeatures;

	}

}
