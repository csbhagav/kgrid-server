package edu.northwestern.cs.kgrid.server;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;

import edu.northwestern.cs.WIG.lib.MySQLQueryHandler;
import edu.northwestern.cs.kgrid.config.ConfigReader;
import edu.northwestern.cs.kgrid.server.utils.KGridUtils;
import edu.northwestern.cs.kgrid.server.utils.MysqlUtils;

@Path("/vote/")
public class Voter {

	@Context
	ServletContext context;

	@Context
	HttpServletRequest req;

	@GET
	@Produces("application/json; charset=UTF-8;")
	@Path("/pgId/{pgId}/tableId/{tableId}/colId/{colId}/matchPgId/{matchPgId}/matchTableId/{matchTableId}/matchColId/{matchColId}/addedColId/{addedColId}/vote/{vote}")
	public Boolean postVoteOld(@PathParam("pgId") Integer pgId,
			@PathParam("tableId") Integer tableId,
			@PathParam("colId") Integer colId,
			@PathParam("matchPgId") Integer matchPgId,
			@PathParam("matchTableId") Integer matchTableId,
			@PathParam("matchColId") Integer matchColId,
			@PathParam("addedColId") Integer addedColId,
			@PathParam("vote") Integer vote) {
		System.out.println("LOGGER:" + req.getRemoteAddr() + " - "
				+ req.getPathInfo());

		try {

			if (req.getRemoteAddr().equalsIgnoreCase("165.124.181.52")
					|| req.getRemoteAddr().equalsIgnoreCase("165.124.180.98")) {
				System.out.println("INFO: Voter =" + req.getRemoteAddr()
						+ " REQ = " + req.getPathInfo());
			}
			ConfigReader conf = (ConfigReader) context
					.getAttribute("configReader");

			MySQLQueryHandler handler;
			handler = MysqlUtils.getMysqlHandler(conf, "*");
			Connection conn = handler.getConnection();

			KGridUtils kUtils = new KGridUtils();
			kUtils.addVoteOld(conn, pgId, tableId, colId, matchPgId,
					matchTableId, matchColId, addedColId, vote,
					req.getRemoteAddr());

			conn.close();

		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return true;
	}

	@GET
	@Produces("application/json; charset=UTF-8;")
	@Path("/pgId/{pgId}/tableId/{tableId}/colId/{colId}/matchPgId/{matchPgId}/matchTableId/{matchTableId}/matchColId/{matchColId}/addedColId/{addedColId}/order/{order}/vote/{vote}")
	public Boolean postVote(@PathParam("pgId") Integer pgId,
			@PathParam("tableId") Integer tableId,
			@PathParam("colId") Integer colId,
			@PathParam("matchPgId") Integer matchPgId,
			@PathParam("matchTableId") Integer matchTableId,
			@PathParam("matchColId") Integer matchColId,
			@PathParam("addedColId") Integer addedColId,
			@PathParam("order") Integer order, @PathParam("vote") Integer vote) {
		System.out.println("LOGGER:" + req.getRemoteAddr() + " - "
				+ req.getPathInfo());

		try {

			if (req.getRemoteAddr().equalsIgnoreCase("165.124.181.52")
					|| req.getRemoteAddr().equalsIgnoreCase("165.124.180.98")) {
				System.out.println("INFO: Voter =" + req.getRemoteAddr()
						+ " REQ = " + req.getPathInfo());
			}
			ConfigReader conf = (ConfigReader) context
					.getAttribute("configReader");

			MySQLQueryHandler handler;
			handler = MysqlUtils.getMysqlHandler(conf, "*");
			Connection conn = handler.getConnection();

			KGridUtils kUtils = new KGridUtils();
			kUtils.addVote(conn, pgId, tableId, colId, matchPgId, matchTableId,
					matchColId, addedColId, vote, req.getRemoteAddr(), order);

			conn.close();

		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return true;
	}

	@GET
	@Produces("application/json; charset=UTF-8;")
	@Path("/pgId/{pgId}/tableId/{tableId}/colId/{colId}/matchPgId/{matchPgId}/matchTableId/{matchTableId}/matchColId/{matchColId}/addedColId/{addedColId}/order/{order}/rate/{rating}")
	public Boolean postRating(@PathParam("pgId") Integer pgId,
			@PathParam("tableId") Integer tableId,
			@PathParam("colId") Integer colId,
			@PathParam("matchPgId") Integer matchPgId,
			@PathParam("matchTableId") Integer matchTableId,
			@PathParam("matchColId") Integer matchColId,
			@PathParam("addedColId") Integer addedColId,
			@PathParam("order") Integer order,
			@PathParam("rating") Integer rating) {
		System.out.println("LOGGER:" + req.getRemoteAddr() + " - "
				+ req.getPathInfo());

		try {

			if (req.getRemoteAddr().equalsIgnoreCase("165.124.181.52")
					|| req.getRemoteAddr().equalsIgnoreCase("165.124.180.98")) {
				System.out.println("INFO: Voter =" + req.getRemoteAddr()
						+ " REQ = " + req.getPathInfo());
			}
			ConfigReader conf = (ConfigReader) context
					.getAttribute("configReader");

			MySQLQueryHandler handler;
			handler = MysqlUtils.getMysqlHandler(conf, "*");
			Connection conn = handler.getConnection();

			KGridUtils kUtils = new KGridUtils();
			kUtils.addRating(conn, pgId, tableId, colId, matchPgId,
					matchTableId, matchColId, addedColId, rating,
					req.getRemoteAddr(), order);

			conn.close();

		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return true;
	}

	@GET
	@Produces("application/json; charset=UTF-8;")
	@Path("/corelates/rating/sCol/{sCol}/tCol/{tCol}/rating/{rating}/user/{user}")
	public Boolean postCorelationRating(@PathParam("sCol") Integer sCol,
			@PathParam("tCol") Integer tCol,
			@PathParam("rating") Integer rating, @PathParam("user") String user) {

		try {
			ConfigReader conf = (ConfigReader) context
					.getAttribute("configReader");

			MySQLQueryHandler handler;
			handler = MysqlUtils.getMysqlHandler(conf, "*");
			Connection conn = handler.getConnection();

			PreparedStatement ps = conn
					.prepareStatement("INSERT INTO corels_attrs_ratings "
							+ "(src_col_id , tgt_col_id ,rating , user ) "
							+ "VALUES ( ? , ? , ? , ? ) ");

			ps.setInt(1, sCol);
			ps.setInt(2, tCol);
			ps.setInt(3, rating);
			ps.setString(4, user);
			ps.execute();
			conn.close();

		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return true;

	}

	@GET
	@Produces("application/json; charset=UTF-8;")
	@Path("/tableSearch/type/{type}/query/{query}/tableId/{tableId}/rank/{rank}/rating/{rating}")
	public Boolean postTableSearchRating(@PathParam("type") String type,
			@PathParam("query") String query,
			@PathParam("tableId") Integer tableId,
			@PathParam("rank") Integer rank, @PathParam("rating") Double rating) {

		try {
			ConfigReader conf = (ConfigReader) context
					.getAttribute("configReader");

			MySQLQueryHandler handler;
			handler = MysqlUtils.getMysqlHandler(conf, "*");
			Connection conn = handler.getConnection();

			PreparedStatement ps = conn
					.prepareStatement("INSERT INTO ts_comparion_rating "
							+ "(type, query , tableId ,rank, rating , src_ip) "
							+ "VALUES (?, ? , ? , ? , ? , ? ) ");

			ps.setString(1, type);
			ps.setString(2, query);
			ps.setInt(3, tableId);
			ps.setInt(4, rank);
			ps.setDouble(5, rating);
			ps.setString(6, req.getRemoteAddr());
			ps.execute();
			conn.close();

		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return true;

	}

}
