package edu.northwestern.cs.kgrid.server.controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

import edu.northwestern.cs.kgrid.server.models.WikiTable;
import edu.northwestern.cs.kgrid.server.models.WikiTableAddedCell;
import edu.northwestern.cs.kgrid.server.models.WikiTableCell;
import edu.northwestern.cs.kgrid.server.models.WikiTableColumn;

public class WikiTableManager {

	private WikiTable table;
	private Connection connect;

	public WikiTableManager(Connection conn, Integer pg_id, Integer id) {
		table = new WikiTable();
		table.setTableId(pg_id, id);
		this.connect = conn;
	}

	public void loadOriginalData() throws SQLException, IOException {
		// get the table attr
		ResultSet tableAttr = this.fetchTableAttr(this.table.getPageId(),
				this.table.getId());
		while (tableAttr.next()) {
			this.table.setTableTitle(new String(tableAttr.getBytes("caption") , "UTF-8"));
			this.table.setNumColumns(tableAttr.getInt("numColumns"));
			this.table.setNumData(tableAttr.getInt("numDataRows"));
			this.table.setNumHeaders(tableAttr.getInt("numHeaderRows"));
		}
		tableAttr.close();

		// get the header
		ResultSet headerRecords = this.fectchTableHeaders(
				this.table.getPageId(), this.table.getId());
		table.setHeaders(this.formatTableRecords(headerRecords,
				table.getNumColumns()));
		headerRecords.close();
		
		// get the data
		ResultSet dataRecords = this.fetchTableData(this.table.getPageId(),
				this.table.getId());
		table.setData(this.formatTableRecords(dataRecords,
				table.getNumColumns()));
		dataRecords.close();

	}

	public ResultSet fetchTableAttr(Integer pg_id, Integer id)
			throws SQLException, IOException {

		String query = "SELECT * FROM table_attrs WHERE pg_id=" + pg_id
				+ " AND id=" + id + ";";

		PreparedStatement preparedStatement = connect.prepareStatement(query);
		ResultSet rs = preparedStatement.executeQuery();
		return rs;
	}
	
	public ResultSet fetchTitles(Integer pageId, Integer id) throws SQLException, IOException{
		String query = "select a.title as page_title, t.title as table_title "
				+"from table_attrs ta join wikiarticles a on ta.pg_id = a.pg_id "
				+"join wikitables t on ta.wikitable_id = t.table_id "
				+"where ta.pg_id = "
				+ pageId
				+ " and id = "
				+ id
				+" limit 1;"; 
		PreparedStatement preparedStatement = connect.prepareStatement(query);
		ResultSet rs = preparedStatement.executeQuery();
		return rs;
	}

	public ResultSet fectchTableHeaders(Integer pg_id, Integer id)
			throws SQLException, IOException {
		return this.fetchTableDetails(true, pg_id, id);
	}

	public ResultSet fetchTableData(Integer pg_id, Integer id)
			throws SQLException, IOException {
		return this.fetchTableDetails(false, pg_id, id);
	}

	private ResultSet fetchTableDetails(boolean isHeader, Integer pg_id,
			Integer id) throws SQLException, IOException {
		return this.fetchTableDetails(isHeader, pg_id, id, new Integer[0]);
	}

	private ResultSet fetchTableDetails(boolean isHeader, Integer pg_id,
			Integer id, Integer colIndex) throws SQLException, IOException {
		Integer[] colIdx = new Integer[1];
		colIdx[0] = colIndex;
		return this.fetchTableDetails(isHeader, pg_id, id, colIdx);
	}

	private ResultSet fetchTableDetails(boolean isHeader, Integer pg_id,
			Integer id, Integer[] colIdx) throws SQLException, IOException {
		String tableName = "table_data";
		if (isHeader)
			tableName = "table_headers";
		String columnIds = "";
		if (colIdx.length > 0) {
			columnIds = " AND (col_idx=" + colIdx[0] + " ";
			for (int i = 1; i < colIdx.length; i++) {
				columnIds += " OR col_idx=" + colIdx[i] + " ";
			}
			columnIds += ") ";
		}
		String sql = " SELECT th.*, cd.subtable_id, CONVERT(cd.display_text USING utf8) "
				+ " as display_text, cd.entityIDs, cd.links, CONVERT(cd.text USING utf8) as text "
				+ " FROM "
				+ tableName
				+ " th "
				+ " JOIN cell_details cd ON th.cellID = cd.cellID "
				+ " WHERE th.pg_id = "
				+ pg_id
				+ " AND th.table_id = "
				+ id
				+ columnIds + " ORDER BY row_idx, col_idx;";
		// System.out.println(sql);
		PreparedStatement preparedStatement = connect.prepareStatement(sql);
		ResultSet rs = preparedStatement.executeQuery();

		return rs;
	}

	public ArrayList<WikiTableColumn> formatTableRecords(ResultSet records,
			int numColumns) throws SQLException {
		ArrayList<WikiTableColumn> columns = new ArrayList<WikiTableColumn>();

		// init columns
		for (int i = 0; i < numColumns; i++) {
			columns.add(new WikiTableColumn(i + 1));
		}

		// each record is one cell
		while (records.next()) {
			WikiTableCell cell = this.getCellFromRecord(records);
			columns.get(cell.getColIdx() - 1).getData()
					.add(cell.getRowIdx() - 1, cell);
		}
		
		
		
		return columns;
	}

	public ArrayList<WikiTableCell> formatColumnRecords(ResultSet records)
			throws SQLException {
		ArrayList<WikiTableCell> cells = new ArrayList<WikiTableCell>();

		// each record is one cell
		while (records.next()) {
			WikiTableCell cell = this.getCellFromRecord(records);
			cells.add(cell.getRowIdx() - 1, cell);
		}
		return cells;
	}

	public ArrayList<WikiTableColumn> format2ColumnsRecords(ResultSet records,
			int keyColIdx, int dataColIdx) throws SQLException {
		ArrayList<WikiTableColumn> columns = new ArrayList<WikiTableColumn>();

		// init columns
		for (int i = 0; i < 2; i++) {
			columns.add(new WikiTableColumn(i + 1));
		}

		// each record is one cell
		while (records.next()) {
			int colIdx = 0;
			WikiTableCell cell = this.getCellFromRecord(records);
			if (cell.getColIdx() == dataColIdx)
				colIdx = 1;
			columns.get(colIdx).getData().add(cell.getRowIdx() - 1, cell);
		}
		return columns;
	}

	private WikiTableCell getCellFromRecord(ResultSet records)
			throws SQLException {

		int row = records.getInt("row_idx");
		int col = records.getInt("col_idx");
		WikiTableCell cell = new WikiTableCell();
		cell.setAdded(false);
		cell.setCellID(records.getString("cellID"));
		cell.setColIdx(col);
		cell.setDisplay(records.getString("display_text"));
		cell.setEntityIds(records.getString("entityIDs"));
		cell.setRowIdx(row);
		cell.setSubtableId(records.getString("subtable_id"));
		cell.setText(records.getString("text"));

		return cell;
	}

	public void addColumn(Integer pageId, Integer tableId, Integer colIndex,
			WikiTableColumn keyColumn, Integer keyPageId, Integer keyTableId, Integer keyColIndex) throws SQLException,
			IOException {
		// get page and table title
		ResultSet titles = this.fetchTitles(pageId, tableId);
		String pageTitle = "";
		String tableTitle = "";
		while(titles.next()) {
			pageTitle = titles.getString("page_title");
			tableTitle = titles.getString("table_title");
		}
		titles.close();
		
		// get the header
		ResultSet headerRecords = this.fetchTableDetails(true, pageId, tableId,
				colIndex);
		ArrayList<WikiTableCell> headers = this
				.formatColumnRecords(headerRecords);
		headerRecords.close();

		// convert to WikiTableAddedCell
		ArrayList<WikiTableCell> addedHeaders = new ArrayList<WikiTableCell>();
		for(WikiTableCell origin : headers){
			addedHeaders.add(new WikiTableAddedCell(origin, keyPageId, keyTableId, keyColIndex, pageTitle, tableTitle));
		}
		
		// get the data
		Integer[] colIdx = new Integer[2];
		colIdx[0] = keyColIndex;
		colIdx[1] = colIndex;
		ResultSet dataRecords = this
				.fetchTableDetails(false, pageId, tableId, colIdx);
		ArrayList<WikiTableColumn> key_data = this.format2ColumnsRecords(
				dataRecords, keyColIndex, colIndex);
		dataRecords.close();
		

		// create hashmap for data
		HashMap<String, WikiTableCell> dataMap = new HashMap<String, WikiTableCell>();
		for (int i = 0; i < key_data.get(1).getData().size(); i++) {
			WikiTableCell keyCell = key_data.get(0).getData().get(i);
			WikiTableCell dataCell = key_data.get(1).getData().get(i);
			dataMap.put(keyCell.getText(), dataCell);
		}
		this.addColumn(keyColumn, addedHeaders, dataMap);
	}

	public void addColumn(WikiTableColumn keyColumn,
			ArrayList<WikiTableCell> headers,
			HashMap<String, WikiTableCell> data) {

		// header
		WikiTableColumn newHeaderColumn = new WikiTableColumn(
				this.table.getNumColumns() + 1);
		newHeaderColumn.setData(headers);
		newHeaderColumn.setAdded(true);
		for (WikiTableCell hCell : newHeaderColumn.getData()) {
			hCell.setAdded(true);
		}
		this.table.getHeaders().add(newHeaderColumn);
		// data
		WikiTableColumn newDataColumn = new WikiTableColumn(
				this.table.getNumColumns() + 1);
		newDataColumn.setAdded(true);
		this.table.getData().add(newDataColumn);
		this.table.setNumColumns(this.table.getNumColumns() + 1);

		Set<String> allKeys = data.keySet();

		for (WikiTableCell keyCell : keyColumn.getData()) {
			WikiTableCell newCell = data.get(closestKeyMatch(allKeys,
					keyCell.getText()));
			if (newCell == null) {
				newCell = new WikiTableCell();
				newCell.setCellID("-1");
				newCell.setDisplay("");
				newCell.setEntityIds("[]");
				newCell.setSubtableId("-1");
				newCell.setText("");
			}
			newCell.setAdded(true);
			newCell.setColIdx(newDataColumn.getColumnIndex());
			newCell.setRowIdx(keyCell.getRowIdx());
			newDataColumn.getData().add(newCell);
		}
	}

	private String closestKeyMatch(Set<String> allKeys, String needle) {

		for (String key : allKeys) {
			if (key.equalsIgnoreCase(needle))
				return key;

		}

		for (String key : allKeys) {
			if (key.contains(needle) || needle.contains(key))
				return key;

		}

		return "";
	}

	public WikiTable getTable() {
		return this.table;
	}
}
