package edu.northwestern.cs.kgrid.server.filters;

import java.io.IOException;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

public class LoggingFilter implements Filter {

	private final static Logger LOGGER = Logger.getLogger(LoggingFilter.class
			.getName());

	public void destroy() {
		// TODO Auto-generated method stub

	}

	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {

		String paramLogStr = "Params:";
		HttpServletRequest httpServletReq = (HttpServletRequest) request;

		Map<String, String[]> parameterMap = request.getParameterMap();
		Set<String> s = parameterMap.keySet();
		Iterator<String> i = s.iterator();
		while (i.hasNext()) {
			String key = i.next().toString();
			paramLogStr += "|" + key + "="
					+ Arrays.toString(parameterMap.get(key));
		}

		String attribLogStr = "Attributes:";
		for (Enumeration e = request.getAttributeNames(); e.hasMoreElements();) {
			attribLogStr += "|" + e.nextElement().toString();

		}

		
		String logStr = httpServletReq.getRemoteAddr() + " | "
				+ request.getLocale().toString() + " | " + paramLogStr + " | "
				+ attribLogStr;

		LOGGER.info(logStr);
		LOGGER.info("testing-" + request.toString() + "  " + request.getContentType() );

		chain.doFilter(request, response);

	}

	public void init(FilterConfig arg0) throws ServletException {
		// TODO Auto-generated method stub

	}

}
