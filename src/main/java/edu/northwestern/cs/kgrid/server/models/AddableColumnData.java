package edu.northwestern.cs.kgrid.server.models;

import java.util.HashMap;
import java.util.HashSet;

public class AddableColumnData {

	String pageTitle;
	String tableTitle;
	String columnTitle;
	boolean isNumeric = false;
	HashMap<Integer, Double> correlCoeff = new HashMap<Integer, Double>();
	HashMap<String, HashSet<String> > keyValuePairs = new HashMap<String, HashSet<String> >();

	public void addKeyValPair(String key, String value) {
		if (!keyValuePairs.containsKey(key))
			keyValuePairs.put(key, new HashSet<String>());

		if (!value.equalsIgnoreCase(""))
			keyValuePairs.get(key).add(value);
	}

	public HashMap<String, HashSet<String>> getKeyValuePairs() {
		return keyValuePairs;
	}

	public boolean isNumeric() {
		return isNumeric;
	}

	public void setNumeric(boolean isNumeric) {
		this.isNumeric = isNumeric;
	}

	public HashMap<Integer, Double> getCorrelCoeff() {
		return correlCoeff;
	}

	public void setCorrelCoeff( HashMap<Integer, Double> correlCoeff) {
		this.correlCoeff = correlCoeff;
			
	}
	
	public void addColCorelPair(Integer colId , Double corel){
		correlCoeff.put(colId, corel);
	}

	public String getPageTitle() {
		return pageTitle;
	}

	public void setPageTitle(String pageTitle) {
		this.pageTitle = pageTitle;
	}

	public String getTableTitle() {
		return tableTitle;
	}

	public void setTableTitle(String tableTitle) {
		this.tableTitle = tableTitle;
	}

	public String getColumnTitle() {
		return columnTitle;
	}

	public void setColumnTitle(String columnTitle) {
		this.columnTitle = columnTitle;
	}

}
