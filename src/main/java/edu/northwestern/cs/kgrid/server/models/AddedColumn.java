package edu.northwestern.cs.kgrid.server.models;

import weka.core.Instance;

public class AddedColumn {

	Instance inst;

	// Integer matchedColIdx;

	// HashMap<String, ArrayList<String>> keyValuePairs = new HashMap<String,
	// ArrayList<String>>();

	ColumnAttributes originalCol; // Original table's column attributes

	ColumnAttributes srcCol; // New Table's column that matches the keys in
								// matchedColIdx column of the original table
	ColumnAttributes tgtCol; // Other columns from the new table.

	ColumnMatchAttributes matchAttribs;

	Double origPgTargetPgSRVal = 0.0;

	Double score;

	Boolean hasCorel;
	Double corelCoeff;

	String featureString = "";

	public AddedColumn(ColumnAttributes originalCol, ColumnAttributes srcCol,
			ColumnAttributes tgtCol, ColumnMatchAttributes matchAttribs) {
		super();
		this.originalCol = originalCol;
		this.srcCol = srcCol;
		this.tgtCol = tgtCol;
		this.matchAttribs = matchAttribs;

		String[] keys = srcCol.getVals();
		String[] vals = srcCol.getVals();
		corelCoeff = 0.0;
		// for (int i = 0; i < srcCol.getVals().length; i++) {
		// if (!keyValuePairs.containsKey(keys[i]))
		// keyValuePairs.put(keys[i], new ArrayList<String>());
		// keyValuePairs.get(keys[i]).add(vals[i]);
		// }

	}

	// public Integer getMatchedColIdx() {
	// return matchedColIdx;
	// }
	//
	// public void setMatchedColIdx(Integer matchedColIdx) {
	// this.matchedColIdx = matchedColIdx;
	// }

	// public HashMap<String, ArrayList<String>> getKeyValuePairs() {
	// return keyValuePairs;
	// }
	//
	// public void setKeyValuePairs(
	// HashMap<String, ArrayList<String>> keyValuePairs) {
	// this.keyValuePairs = keyValuePairs;
	// }

	public ColumnAttributes getSrcCol() {
		return srcCol;
	}

	public void setSrcCol(ColumnAttributes srcCol) {
		this.srcCol = srcCol;
	}

	public ColumnAttributes getTgtCol() {
		return tgtCol;
	}

	public void setTgtCol(ColumnAttributes tgtCol) {
		this.tgtCol = tgtCol;
	}

	public ColumnAttributes getOriginalCol() {
		return originalCol;
	}

	public void setOriginalCol(ColumnAttributes originalCol) {
		this.originalCol = originalCol;
	}

	public ColumnMatchAttributes getMatchAttribs() {
		return matchAttribs;
	}

	public void setMatchAttribs(ColumnMatchAttributes matchAttribs) {
		this.matchAttribs = matchAttribs;
	}

	public Double getScore() {
		return score;
	}

	public void setScore(Double score) {
		this.score = score;
	}

	public Double getOrigPgTargetPgSRVal() {
		return origPgTargetPgSRVal;
	}

	public void setOrigPgTargetPgSRVal(Double origPgTargetPgSRVal) {
		if (origPgTargetPgSRVal != null)
			this.origPgTargetPgSRVal = origPgTargetPgSRVal;
	}

	public Boolean getHasCorel() {
		return hasCorel;
	}

	public void setHasCorel(Boolean hasCorel) {
		this.hasCorel = hasCorel;
	}

	public Double getCorelCoeff() {
		return corelCoeff;
	}

	public void setCorelCoeff(Double corelCoeff) {
		setHasCorel(true);
		this.corelCoeff = corelCoeff;
	}

	public String getFeatureString() {
		return featureString;
	}

	public void setFeatureString(String[] features) {
		String fStr = "";
		for (int i = 0; i < features.length; i++) {
			fStr += features[i] + ",";
		}
		fStr = fStr.substring(0, fStr.length() - 1);
		this.featureString = fStr;
	}

	public Instance getInst() {
		return inst;
	}

	public void setInst(Instance inst) {
		this.inst = inst;
	}

	public void setFeatureString(String featureString) {
		this.featureString = featureString;
	}

}
