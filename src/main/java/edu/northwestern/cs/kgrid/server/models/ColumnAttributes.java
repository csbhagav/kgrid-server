package edu.northwestern.cs.kgrid.server.models;

import java.util.ArrayList;
import java.util.List;

public class ColumnAttributes extends ColumnId {

	Boolean isNumeric;
	Double avgLen;
	Integer numUniqVals;
	Integer numRows;

	String[] vals;
	String[] entities;

	Integer pageInlinks;
	Integer pageOutlinks;

	List<String> categoryIds = new ArrayList<String>();

	Integer tid;

	Double fractionNumericInTable = 0.0;

	public ColumnAttributes() {
		super();
	}

	public ColumnAttributes(Integer pgId, Integer tableId, Integer colIdx,
			Boolean isNumeric, Double avgLen, Integer numUniqVals,
			Integer numRows, String[] vals, String[] entities, Integer inlinks,
			Integer outlinks, Integer tid, Double fractionNumericInTable) {
		super();
		this.pgId = pgId;
		this.tableId = tableId;
		this.colIdx = colIdx;
		this.isNumeric = isNumeric;
		this.avgLen = avgLen;
		this.numUniqVals = numUniqVals;
		this.vals = vals;
		this.entities = entities;
		this.numRows = numRows;

		this.pageInlinks = inlinks;
		this.pageOutlinks = outlinks;

		this.tid = tid;

		this.fractionNumericInTable = fractionNumericInTable;

	}

	public Boolean getIsNumeric() {
		return isNumeric;
	}

	public void setIsNumeric(Boolean isNumeric) {
		this.isNumeric = isNumeric;
	}

	public Double getAvgLen() {
		return avgLen;
	}

	public void setAvgLen(Double avgLen) {
		this.avgLen = avgLen;
	}

	public Integer getNumUniqVals() {
		return numUniqVals;
	}

	public void setNumUniqVals(Integer numUniqVals) {
		this.numUniqVals = numUniqVals;
	}

	public String[] getVals() {
		return vals;
	}

	public void setVals(String[] vals) {
		this.vals = vals;
	}

	public String[] getEntities() {
		return entities;
	}

	public void setEntities(String[] entities) {
		this.entities = entities;
	}

	public Integer getNumRows() {
		return numRows;
	}

	public void setNumRows(Integer numRows) {
		this.numRows = numRows;
	}

	public Integer getPageInlinks() {
		return pageInlinks;
	}

	public void setPageInlinks(Integer pageInlinks) {
		this.pageInlinks = pageInlinks;
	}

	public Integer getPageOutlinks() {
		return pageOutlinks;
	}

	public void setPageOutlinks(Integer pageOutlinks) {
		this.pageOutlinks = pageOutlinks;
	}

	public Integer getTid() {
		return tid;
	}

	public void setTid(Integer tid) {
		this.tid = tid;
	}

	public Double getFractionNumericInTable() {
		return fractionNumericInTable;
	}

	public void setFractionNumericInTable(Double fractionNumericInTable) {
		this.fractionNumericInTable = fractionNumericInTable;
	}

}
