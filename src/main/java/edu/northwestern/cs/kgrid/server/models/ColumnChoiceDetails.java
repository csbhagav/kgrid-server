package edu.northwestern.cs.kgrid.server.models;

import java.util.HashMap;

import weka.core.Instance;

public class ColumnChoiceDetails extends ColumnId {

	String pgTitle;
	String tableTitle;
	String columnTitle;

	Integer originalColumnIndex;
	Integer matchedColumnIndex;

	ColumnAttributes srcColDetails;

	String source;
	Integer order;

	Boolean isNumeric;

	HashMap<String, Double> features = new HashMap<String, Double>();

	public ColumnChoiceDetails(String pgTitle, String tableTitle,
			String columnTitle, ColumnAttributes srcColDetails) {
		super();
		this.pgTitle = pgTitle.trim();
		this.tableTitle = tableTitle.trim();
		this.columnTitle = columnTitle.trim();
		this.srcColDetails = srcColDetails;
	}

	public String getPgTitle() {
		return pgTitle;
	}

	public void setPgTitle(String pgTitle) {
		this.pgTitle = pgTitle;
	}

	public String getTableTitle() {
		return tableTitle;
	}

	public void setTableTitle(String tableTitle) {
		this.tableTitle = tableTitle;
	}

	public String getColumnTitle() {
		return columnTitle;
	}

	public void setColumnTitle(String columnTitle) {
		this.columnTitle = columnTitle;
	}

	public Integer getMatchedColumnIndex() {
		return matchedColumnIndex;
	}

	public void setMatchedColumnIndex(Integer matchedColumnIndex) {
		this.matchedColumnIndex = matchedColumnIndex;
	}

	public Integer getOriginalColumnIndex() {
		return originalColumnIndex;
	}

	public void setOriginalColumnIndex(Integer originalColumnIndex) {
		this.originalColumnIndex = originalColumnIndex;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public Integer getOrder() {
		return order;
	}

	public void setOrder(Integer order) {
		this.order = order;
	}

	public Boolean getIsNumeric() {
		return isNumeric;
	}

	public void setIsNumeric(Boolean isNumeric) {
		this.isNumeric = isNumeric;
	}

	public void setInstance(Instance inst) {
		for (int i = 0; i < inst.numAttributes(); i++) {
			features.put(inst.attribute(i).name(), inst.value(i));
		}

	}

}
