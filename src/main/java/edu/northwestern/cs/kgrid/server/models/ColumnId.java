package edu.northwestern.cs.kgrid.server.models;

public class ColumnId {

	Integer pgId;
	Integer tableId;
	Integer colIdx;

	public Integer getPgId() {
		return pgId;
	}

	public void setPgId(Integer pgId) {
		this.pgId = pgId;
	}

	public Integer getTableId() {
		return tableId;
	}

	public void setTableId(Integer tableId) {
		this.tableId = tableId;
	}

	public Integer getColIdx() {
		return colIdx;
	}

	public void setColIdx(Integer colIdx) {
		this.colIdx = colIdx;
	}

}
