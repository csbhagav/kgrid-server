package edu.northwestern.cs.kgrid.server.models;

public class ColumnMatchAttributes {

	Integer numEntityMatches;
	Integer numValueMatches;
	Integer totalMatches;

	Double totalMatchPercent;
	Double entityMatchPercent;
	Double valueMatchPercentage;

	Double entityMatchFraction;
	Boolean tgtColMatchesOtherCol;

	Double avgNumOfKeyMappings;

	public ColumnMatchAttributes(Integer numEntityMatches,
			Integer numValueMatches, Integer totalMatches,
			Double totalMatchPercent, Double entityMatchPercent,
			Double valueMatchPercentage, Double avgNumOfKeyMappings) {
		super();
		this.numEntityMatches = numEntityMatches;
		this.numValueMatches = numValueMatches;
		this.totalMatches = totalMatches;
		this.totalMatchPercent = totalMatchPercent;
		this.entityMatchPercent = entityMatchPercent;
		this.valueMatchPercentage = valueMatchPercentage; // temporary change
		if (entityMatchPercent == 0)
			this.entityMatchFraction = 0.0;
		else
			this.entityMatchFraction = entityMatchPercent / entityMatchPercent;

		this.avgNumOfKeyMappings = avgNumOfKeyMappings;
	}

	public Integer getNumEntityMatches() {
		return numEntityMatches;
	}

	public void setNumEntityMatches(Integer numEntityMatches) {
		this.numEntityMatches = numEntityMatches;
	}

	public Integer getNumValueMatches() {
		return numValueMatches;
	}

	public void setNumValueMatches(Integer numValueMatches) {
		this.numValueMatches = numValueMatches;
	}

	public Integer getTotalMatches() {
		return totalMatches;
	}

	public void setTotalMatches(Integer totalMatches) {
		this.totalMatches = totalMatches;
	}

	public Double getTotalMatchPercent() {
		return totalMatchPercent;
	}

	public void setTotalMatchPercent(Double totalMatchPercent) {
		this.totalMatchPercent = totalMatchPercent;
	}

	public Double getEntityMatchPercent() {
		return entityMatchPercent;
	}

	public void setEntityMatchPercent(Double entityMatchPercent) {
		this.entityMatchPercent = entityMatchPercent;
	}

	public Double getValueMatchPercentage() {
		return valueMatchPercentage;
	}

	public Double getValueMatchFraction() {
		return valueMatchPercentage;
	}

	public void setValueMatchPercentage(Double valueMatchPercentage) {
		this.valueMatchPercentage = valueMatchPercentage;
	}

	public Double getEntityMatchFraction() {
		return entityMatchFraction;
	}

	public void setEntityMatchFraction(Double entityMatchFraction) {
		this.entityMatchFraction = entityMatchFraction;
	}

	public Boolean getTgtColMatchesOtherCol() {
		return tgtColMatchesOtherCol;
	}

	public void setTgtColMatchesOtherCol(Boolean tgtColMatchesOtherCol) {
		this.tgtColMatchesOtherCol = tgtColMatchesOtherCol;
	}

	public Double getAvgNumOfKeyMappings() {
		return avgNumOfKeyMappings;
	}

	public void setAvgNumOfKeyMappings(Double avgNumOfKeyMappings) {
		this.avgNumOfKeyMappings = avgNumOfKeyMappings;
	}

}
