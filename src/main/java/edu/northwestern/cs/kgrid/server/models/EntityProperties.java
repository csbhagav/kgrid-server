package edu.northwestern.cs.kgrid.server.models;

public class EntityProperties {

	String pageTitle;
	String tableCaption;
	String key;
	String value;

	public EntityProperties(String pageTitle, String tableCaption, String key,
			String value) {
		super();
		this.pageTitle = pageTitle;
		this.tableCaption = tableCaption;
		this.key = key;
		this.value = value;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getPageTitle() {
		return pageTitle;
	}

	public void setPageTitle(String pageTitle) {
		this.pageTitle = pageTitle;
	}

	public String getTableCaption() {
		return tableCaption;
	}

	public void setTableCaption(String tableCaption) {
		this.tableCaption = tableCaption;
	}
	

}
