package edu.northwestern.cs.kgrid.server.models;

public enum FeatureForModelType {
	CLASSIFIER, RANKER
}
