package edu.northwestern.cs.kgrid.server.models;

public class FeaturesWithMetaData {

	String id;
	String[] featureArr;
	
	
	
	public FeaturesWithMetaData(String id, String[] featureArr) {
		super();
		this.id = id;
		this.featureArr = featureArr;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String[] getFeatureArr() {
		return featureArr;
	}
	public void setFeatureArr(String[] featureArr) {
		this.featureArr = featureArr;
	}
	
	
	
	
	
}
