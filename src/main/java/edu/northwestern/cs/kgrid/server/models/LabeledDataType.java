package edu.northwestern.cs.kgrid.server.models;

public enum LabeledDataType {
	TRAIN, TEST, EVAL_DATA
}
