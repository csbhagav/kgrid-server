package edu.northwestern.cs.kgrid.server.models;

public class PageTableItem {

	String tableId;
	String title;
	Integer rowCount;
	Integer colCount;
	String columns;
	public PageTableItem(String tableId, String title, Integer rowCount,
			Integer colCount, String columns) {
		super();
		this.tableId = tableId;
		this.title = title;
		this.rowCount = rowCount;
		this.colCount = colCount;
		this.columns = columns;
	}
	public String getTableId() {
		return tableId;
	}
	public void setTableId(String tableId) {
		this.tableId = tableId;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public Integer getRowCount() {
		return rowCount;
	}
	public void setRowCount(Integer rowCount) {
		this.rowCount = rowCount;
	}
	public Integer getColCount() {
		return colCount;
	}
	public void setColCount(Integer colCount) {
		this.colCount = colCount;
	}
	public String getColumns() {
		return columns;
	}
	public void setColumns(String columns) {
		this.columns = columns;
	}

	
	
	
}
