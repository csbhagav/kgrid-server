package edu.northwestern.cs.kgrid.server.models;

public class Posting {

	Integer wikiPageId;
	Double srMeasure;

	public Posting(Integer wikiPageId, Double srMeasure) {
		super();
		this.wikiPageId = wikiPageId;
		this.srMeasure = srMeasure;
	}

	public Integer getWikiPageId() {
		return wikiPageId;
	}

	public void setWikiPageId(Integer wikiPageId) {
		this.wikiPageId = wikiPageId;
	}

	public Double getSrMeasure() {
		return srMeasure;
	}

	public void setSrMeasure(Double srMeasure) {
		this.srMeasure = srMeasure;
	}

}