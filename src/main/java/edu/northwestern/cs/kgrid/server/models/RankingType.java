package edu.northwestern.cs.kgrid.server.models;

public enum RankingType {
	RANDOM, MATCH_PERC, CLSFR_AND_RANKER
}
