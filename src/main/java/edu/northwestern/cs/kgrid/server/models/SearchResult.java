package edu.northwestern.cs.kgrid.server.models;

import org.codehaus.jackson.annotate.JsonIgnore;

public class SearchResult {

	String text;
	String url;
	SearchResultTableDetails tableDet;
	Integer wikitableId;

	Integer mwPgId;
	Integer mwTableId;

	@JsonIgnore
	Double tempScore;
	@JsonIgnore
	Integer mwColIdx;;

	public SearchResult(String text, String url,
			SearchResultTableDetails tableDet, Integer tableId, Integer mwPgId,
			Integer mwTableId) {
		super();
		this.text = text + " / " + tableDet.getCaption();
		this.url = url;
		this.tableDet = tableDet;
		this.wikitableId = tableId;
		this.mwPgId = mwPgId;
		this.mwTableId = mwTableId;

	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public SearchResultTableDetails getTableDet() {
		return tableDet;
	}

	public void setTableDet(SearchResultTableDetails tableDet) {
		this.tableDet = tableDet;
	}

	public Integer getWikitableId() {
		return wikitableId;
	}

	public void setWikitableId(Integer wikitableId) {
		this.wikitableId = wikitableId;
	}

	public Integer getMwPgId() {
		return mwPgId;
	}

	public void setMwPgId(Integer mwPgId) {
		this.mwPgId = mwPgId;
	}

	public Integer getMwTableId() {
		return mwTableId;
	}

	public void setMwTableId(Integer mwTableId) {
		this.mwTableId = mwTableId;
	}

	public Double getTempScore() {
		return tempScore;
	}

	public void setTempScore(Double tempScore) {
		this.tempScore = tempScore;
	}

	public Integer getMwColIdx() {
		return mwColIdx;
	}

	public void setMwColIdx(Integer mwColIdx) {
		this.mwColIdx = mwColIdx;
	}

	
}
