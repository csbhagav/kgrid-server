package edu.northwestern.cs.kgrid.server.models;

import java.util.ArrayList;

public class SearchResultTableDetails {

	String caption;
	Integer numDataRows;
	Integer numColumns;
	Integer numHeaderRows;
	ArrayList<ArrayList<String>> headerRows;
	ArrayList<ArrayList<String>> sampleRows;

	public void addHeaderRow(ArrayList<String> hRow) {
		headerRows.add(hRow);
	}

	public void addSampleRow(ArrayList<String> sRow) {
		sampleRows.add(sRow);
	}

	public ArrayList<ArrayList<String>> getHeaderRows() {
		return headerRows;
	}

	public void setHeaderRows(ArrayList<ArrayList<String>> headerRows) {
		this.headerRows = headerRows;
	}

	public Integer getNumColumns() {
		return numColumns;
	}

	public void setNumColumns(Integer numColumns) {
		this.numColumns = numColumns;
	}

	public ArrayList<ArrayList<String>> getSampleRows() {
		return sampleRows;
	}

	public void setSampleRows(ArrayList<ArrayList<String>> sampleRows) {
		this.sampleRows = sampleRows;
	}

	public Integer getNumDataRows() {
		return numDataRows;
	}

	public void setNumDataRows(Integer numDataRows) {
		this.numDataRows = numDataRows;
	}

	public Integer getNumHeaderRows() {
		return numHeaderRows;
	}

	public void setNumHeaderRows(Integer numHeaderRows) {
		this.numHeaderRows = numHeaderRows;
	}

	public String getCaption() {
		return caption;
	}

	public void setCaption(String caption) {
		this.caption = caption;
	}

}
