package edu.northwestern.cs.kgrid.server.models;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Test {

	String name;
	Integer age;

	private Test() {
	}

	public Test(String name, Integer age) {
		super();
		this.name = name;
		this.age = age;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	};

}
