package edu.northwestern.cs.kgrid.server.models;

import java.util.ArrayList;

public class TitleSearchResult {

	String query;
	ArrayList<String> suggestions = new ArrayList<String>();
	ArrayList<String> data = new ArrayList<String>();

	public String getQuery() {
		return query;
	}

	public void setQuery(String query) {
		this.query = query;
	}

	public ArrayList<String> getSuggestions() {
		return suggestions;
	}

	public void setSuggestions(ArrayList<String> suggestions) {
		this.suggestions = suggestions;
	}

	public ArrayList<String> getData() {
		return data;
	}

	public void setData(ArrayList<String> data) {
		this.data = data;
	}

	public void addTitleDataPair(String title, String data) {

		suggestions.add(title);
		this.data.add(data);

	}

}
