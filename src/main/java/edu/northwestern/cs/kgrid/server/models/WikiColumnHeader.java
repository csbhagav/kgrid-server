package edu.northwestern.cs.kgrid.server.models;

public class WikiColumnHeader {

	Integer pgId;
	String pgTitle;
	Integer tableId;
	String tableTitle;
	String headerTitle;

	public WikiColumnHeader(Integer pgId, String pgTitle, Integer tableId,
			String tableTitle, String headerTitle) {
		super();
		this.pgId = pgId;
		this.pgTitle = pgTitle;
		this.tableId = tableId;
		this.tableTitle = tableTitle;
		this.headerTitle = headerTitle;
	}

	public Integer getPgId() {
		return pgId;
	}

	public void setPgId(Integer pgId) {
		this.pgId = pgId;
	}

	public String getPgTitle() {
		return pgTitle;
	}

	public void setPgTitle(String pgTitle) {
		this.pgTitle = pgTitle;
	}

	public Integer getTableId() {
		return tableId;
	}

	public void setTableId(Integer tableId) {
		this.tableId = tableId;
	}

	public String getTableTitle() {
		return tableTitle;
	}

	public void setTableTitle(String tableTitle) {
		this.tableTitle = tableTitle;
	}

	public String getHeaderTitle() {
		return headerTitle;
	}

	public void setHeaderTitle(String headerTitle) {
		this.headerTitle = headerTitle;
	};

}
