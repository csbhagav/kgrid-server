package edu.northwestern.cs.kgrid.server.models;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class WikiTable {
	private Integer id;
	private Integer pgId;
	private String tableId; // pg_id-id
	private String tableTitle;
	private ArrayList<WikiTableColumn> headers;
	private ArrayList<WikiTableColumn> data;
	private int numHeaders;
	private int numData;
	private int numColumns;

	public WikiTable() {
	}

	public String getTableId() {
		return tableId;
	}

	public void setTableId(Integer pg_id, Integer id) {
		this.tableId = pg_id + "-" + id;
		this.setPageId(pg_id);
		this.setId(id);
	}

	public String getTableTitle() {
		return tableTitle;
	}

	public void setTableTitle(String tableTitle) {
		this.tableTitle = tableTitle;
	}

	public ArrayList<WikiTableColumn> getHeaders() {
		return headers;
	}

	public void setHeaders(ArrayList<WikiTableColumn> headers) {
		this.headers = headers;
	}

	public ArrayList<WikiTableColumn> getData() {
		return data;
	}

	public void setData(ArrayList<WikiTableColumn> data) {
		this.data = data;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getPageId() {
		return pgId;
	}

	public void setPageId(Integer pg_id) {
		this.pgId = pg_id;
	}

	public int getNumHeaders() {
		return numHeaders;
	}

	public void setNumHeaders(int numHeaders) {
		this.numHeaders = numHeaders;
	}

	public int getNumData() {
		return numData;
	}

	public void setNumData(int numData) {
		this.numData = numData;
	}

	public int getNumColumns() {
		return numColumns;
	}

	public void setNumColumns(int numColumns) {
		this.numColumns = numColumns;
	}
}
