package edu.northwestern.cs.kgrid.server.models;

public class WikiTableAddedCell extends WikiTableCell {
	private Integer keyPageId;
	private Integer keyTableId;
	private Integer keyCellId;
	private String tableName;
	private String pageName;
	
	public WikiTableAddedCell() {
		super();
		this.setAdded(true);
	}

	public WikiTableAddedCell(WikiTableCell origin, 
			Integer keyCellId, Integer keyPageId, Integer keyTableId, 
			String tableName, String pageName){
		super();
		this.setAdded(true);
		this.setCellID(origin.getCellID());
		this.setColIdx(origin.getColIdx());
		this.setDisplay(origin.getDisplay());
		this.setEntityIds(origin.getEntityIds());
		this.setKeyCellId(keyCellId);
		this.setKeyPageId(keyPageId);
		this.setKeyTableId(keyTableId);
		this.setPageName(pageName);
		this.setRowIdx(origin.getRowIdx());
		this.setTableName(tableName);
		this.setText(origin.getText());
	}
	
	public Integer getKeyPageId() {
		return keyPageId;
	}

	public void setKeyPageId(Integer keyPageId) {
		this.keyPageId = keyPageId;
	}

	public Integer getKeyTableId() {
		return keyTableId;
	}

	public void setKeyTableId(Integer keyTableId) {
		this.keyTableId = keyTableId;
	}

	public Integer getKeyCellId() {
		return keyCellId;
	}

	public void setKeyCellId(Integer keyCellId) {
		this.keyCellId = keyCellId;
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public String getPageName() {
		return pageName;
	}

	public void setPageName(String pageName) {
		this.pageName = pageName;
	}
}
