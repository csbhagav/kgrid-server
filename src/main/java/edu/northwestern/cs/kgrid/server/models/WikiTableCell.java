package edu.northwestern.cs.kgrid.server.models;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class WikiTableCell {
	private String cellID;
	private String text;
	private String display;
	private int colIdx;
	private int rowIdx;
	private String subtableId;
	private String entityIds;
	private boolean isAdded;

	public WikiTableCell() {
		isAdded = false;
	}

	public String getCellID() {
		return cellID;
	}

	public void setCellID(String cellID) {
		this.cellID = cellID;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getDisplay() {
		return display;
	}

	public void setDisplay(String display) {
		this.display = display;
	}

	public int getColIdx() {
		return colIdx;
	}

	public void setColIdx(int colIdx) {
		this.colIdx = colIdx;
	}

	public int getRowIdx() {
		return rowIdx;
	}

	public void setRowIdx(int rowIdx) {
		this.rowIdx = rowIdx;
	}

	public String getSubtableId() {
		return subtableId;
	}

	public void setSubtableId(String subtableId) {
		this.subtableId = subtableId;
	}

	public String getEntityIds() {
		return entityIds;
	}

	public void setEntityIds(String entityIds) {
		this.entityIds = entityIds;
	}

	public boolean isAdded() {
		return isAdded;
	}

	public void setAdded(boolean isAdded) {
		this.isAdded = isAdded;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cellID == null) ? 0 : cellID.hashCode());
		return result;
	}

}
