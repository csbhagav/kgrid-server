package edu.northwestern.cs.kgrid.server.models;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class WikiTableColumn {
	private int columnIndex;
	private boolean isAdded;
	private List<WikiTableCell> data;

	public WikiTableColumn() {
		isAdded = false;
	}

	public WikiTableColumn(int index) {
		this.columnIndex = index;
		data = new ArrayList<WikiTableCell>();
	}

	public int getColumnIndex() {
		return columnIndex;
	}

	public void setColumnIndex(int columnIndex) {
		this.columnIndex = columnIndex;
	}

	public List<WikiTableCell> getData() {
		return data;
	}

	public void setData(List<WikiTableCell> data) {
		this.data = data;
	}

	public boolean isAdded() {
		return isAdded;
	}

	public void setAdded(boolean isAdded) {
		this.isAdded = isAdded;
	}
}
