package edu.northwestern.cs.kgrid.server.utils;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.HttpMethod;
import org.json.JSONException;
import org.json.JSONObject;

import edu.northwestern.cs.kgrid.lib.WikiArticleUtils;

public class ArticlesUtils {

	public static Integer getArticleID(Connection conn, String title)
			throws SQLException {
		PreparedStatement inWikiArticlesPS = conn
				.prepareStatement("SELECT * FROM wikiarticles WHERE title = ? ");
		inWikiArticlesPS.setBytes(1, title.getBytes());
		ResultSet inWikiArticlesRS = inWikiArticlesPS.executeQuery();
		Integer kgridWaID = 0;
		boolean found = false;
		while (inWikiArticlesRS.next()) {
			found = true;
			kgridWaID = inWikiArticlesRS.getInt("pg_id");
		}
		inWikiArticlesPS.close();
		inWikiArticlesRS.close();

		if (!found) {
			return -1;
		} else {
			return kgridWaID;
		}

	}

	public static void insertWikiarticle(Connection conn, Integer kgridWaID,
			HttpClient client, HttpMethod method) throws SQLException,
			JSONException, HttpException, IOException {

		PreparedStatement wikiArticleIdPs = conn
				.prepareStatement("select p.page_id id FROM wikiarticles w , page p "
						+ "WHERE w.title = p.page_title and p.page_namespace = 0 "
						+ "AND w.pg_id = ?");
		wikiArticleIdPs.setInt(1, kgridWaID);

		PreparedStatement articleHtmlInsPs = conn
				.prepareStatement("INSERT INTO articleHtml "
						+ "(id , length , text , revid) VALUES (? , ? , ? , ?)");

		ResultSet wikiArticleIdRs = wikiArticleIdPs.executeQuery();

		PreparedStatement processedPagePs = conn
				.prepareStatement("SELECT count(*) c FROM articleHtml where id = ? ");

		JSONObject parsedJsonObj;

		Integer revId = 0;
		String articleText = "";
		Integer pgId = 0;

		while (wikiArticleIdRs.next()) {

			pgId = wikiArticleIdRs.getInt("id");

			if (isPageProcessed(conn, processedPagePs, pgId))
				continue;

			parsedJsonObj = WikiArticleUtils.getArticleHtml(client, method,
					pgId);

			revId = parsedJsonObj.getInt("revid");
			articleText = new JSONObject(parsedJsonObj.getString("text"))
					.getString("*");
			articleHtmlInsPs.setInt(1, pgId);
			articleHtmlInsPs.setInt(2, articleText.length());
			articleHtmlInsPs.setBytes(3, articleText.getBytes("UTF-8"));
			articleHtmlInsPs.setInt(4, revId);
			articleHtmlInsPs.execute();
		}
		wikiArticleIdRs.close();
		wikiArticleIdPs.close();
		processedPagePs.close();
		articleHtmlInsPs.close();
	}

	private static boolean isPageProcessed(Connection conn,
			PreparedStatement processedPagePs, Integer pgId)
			throws SQLException {

		processedPagePs.setInt(1, pgId);
		ResultSet rs = processedPagePs.executeQuery();
		boolean done = false;
		while (rs.next()) {
			if (rs.getInt("c") > 0)
				done = true;
		}
		rs.close();
		return done;
	}
}
