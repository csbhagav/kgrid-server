package edu.northwestern.cs.kgrid.server.utils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class GraphUtils {

	public static ArrayList<String> getParentIds(Connection conn,
			String childTitle) throws SQLException {
		ArrayList<String> parentIds = new ArrayList<String>();

		try {
			PreparedStatement pageCategoryIdsPS = conn
					.prepareStatement("SELECT p2.page_id page_id FROM categorylinks cl , page p1 , page p2 "
							+ "WHERE p1.page_id = cl.cl_from AND p1.page_title = ? AND p1.page_namespace = 0 "
							+ "AND cl.cl_to = p2.page_title AND p2.page_namespace = 14");

			pageCategoryIdsPS.setBytes(1, childTitle.getBytes());
			ResultSet pageCategoryIdsRS = pageCategoryIdsPS.executeQuery();

			while (pageCategoryIdsRS.next()) {
				parentIds.add(pageCategoryIdsRS.getString("page_id"));
			}

		} catch (SQLException e) {
			throw e;
		} finally {
		}

		return parentIds;
	}

}
