package edu.northwestern.cs.kgrid.server.utils;

import ciir.umass.edu.features.Normalizer;
import ciir.umass.edu.features.ZScoreNormalizor;
import ciir.umass.edu.learning.DataPoint;
import ciir.umass.edu.learning.RankList;
import ciir.umass.edu.learning.Ranker;
import ciir.umass.edu.learning.RankerFactory;
import ciir.umass.edu.metric.ERRScorer;
import ciir.umass.edu.metric.MetricScorer;
import ciir.umass.edu.metric.MetricScorerFactory;
import edu.northwestern.cs.kgrid.core.ml.FeatureNormalizer;
import edu.northwestern.cs.kgrid.ml.DataLabels;
import edu.northwestern.cs.kgrid.ml.LogRegModel;
import edu.northwestern.cs.kgrid.server.models.AddedColumn;
import edu.northwestern.cs.kgrid.server.models.ColumnAttributes;
import edu.northwestern.cs.kgrid.server.models.ColumnMatchAttributes;
import edu.northwestern.cs.kgrid.server.models.LabeledDataType;
import weka.core.Attribute;
import weka.core.Instance;
import weka.core.Instances;

import javax.servlet.ServletContext;
import java.io.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

public class KGridModelUtils {

	protected static RankerFactory rFact = new RankerFactory();

	protected static MetricScorerFactory mFact = new MetricScorerFactory();

	public static Normalizer nml = new ZScoreNormalizor();

	public static String[] dataToFeatureValues(AddedColumn ac, Instances data) {
		int idx = 0;
		int size = data.numAttributes();

		String[] features = new String[size];
		Double val = 0.0;

		HashMap<String, String> pureFeatureValueMap = new HashMap<String, String>(
				size);

		HashMap<String, String> featureValueMap = new HashMap<String, String>(
				size);

		// matchPerc
		featureValueMap.put("matchPerc", ac.getMatchAttribs()
				.getValueMatchFraction().toString());
		pureFeatureValueMap.put("matchPerc", ac.getMatchAttribs()
				.getValueMatchFraction().toString());

		// avgNumKeyMap
		val = (1.0 / (double) ac.getMatchAttribs().getAvgNumOfKeyMappings());
		featureValueMap.put("avgNumKeyMap", val.toString());
		pureFeatureValueMap.put("avgNumKeyMap", ac.getMatchAttribs()
				.getAvgNumOfKeyMappings().toString());

		// srRelate -1.5503
		featureValueMap.put("srRelate", ac.getOrigPgTargetPgSRVal().toString());
		pureFeatureValueMap.put("srRelate", ac.getOrigPgTargetPgSRVal()
				.toString());

		// srcDistinctVal -0.0142
		featureValueMap.put("srcDistinctVal", ac.getOriginalCol()
				.getNumUniqVals().toString());
		pureFeatureValueMap.put("srcDistinctVal", ac.getOriginalCol()
				.getNumUniqVals().toString());

		// srcNumericValFrac
		featureValueMap.put("srcNumericValFrac", ac.getOriginalCol()
				.getFractionNumericInTable().toString());
		pureFeatureValueMap.put("srcNumericValFrac", ac.getOriginalCol()
				.getFractionNumericInTable().toString());

		// srcAvgLen -0.1049
		featureValueMap.put("srcAvgLen",
				FeatureNormalizer.averageLen(ac.getOriginalCol().getAvgLen())
						.toString());
		pureFeatureValueMap.put("srcAvgLen", ac.getOriginalCol().getAvgLen()
				.toString());

		// srcIsNum
		featureValueMap.put("srcIsNum", ac.getSrcCol().getIsNumeric() ? "1"
				: "0");
		pureFeatureValueMap.put("srcIsNum", ac.getSrcCol().getIsNumeric() ? "1"
				: "0");

		// srcRows 0.0095
		featureValueMap.put("srcRows", ac.getOriginalCol().getNumRows()
				.toString());
		pureFeatureValueMap.put("srcRows", ac.getOriginalCol().getNumRows()
				.toString());

		// srcDistinctValFrac
		val = ((double) ac.getOriginalCol().getNumUniqVals() / (double) ac
				.getOriginalCol().getNumRows());
		featureValueMap.put("srcDistinctValFrac", val.toString());
		pureFeatureValueMap.put("srcDistinctValFrac", val.toString());
		val = 0.0;
		// tgtDistinctVal 0.0008
		featureValueMap.put("tgtDistinctVal", ac.getTgtCol().getNumUniqVals()
				.toString());
		pureFeatureValueMap.put("tgtDistinctVal", ac.getTgtCol()
				.getNumUniqVals().toString());
		// tgtAvgLen -0.0097
		featureValueMap.put("tgtAvgLen",
				FeatureNormalizer.averageLen(ac.getTgtCol().getAvgLen())
						.toString());
		pureFeatureValueMap.put("tgtAvgLen", ac.getTgtCol().getAvgLen()
				.toString());

		// tgtIsNum -0.6114
		featureValueMap.put("tgtIsNum", ac.getTgtCol().getIsNumeric() ? "1"
				: "0");
		pureFeatureValueMap.put("tgtIsNum", ac.getTgtCol().getIsNumeric() ? "1"
				: "0");
		// tgtRows -0.001
		featureValueMap.put("tgtRows", ac.getTgtCol().getNumRows().toString());
		pureFeatureValueMap.put("tgtRows", ac.getTgtCol().getNumRows()
				.toString());
		// tgtDistinctValFrac
		val = (double) ac.getTgtCol().getNumUniqVals()
				/ (double) ac.getTgtCol().getNumRows();
		featureValueMap.put("tgtDistinctValFrac", val.toString());
		pureFeatureValueMap.put("tgtDistinctValFrac", val.toString());
		// inLink 0
		featureValueMap.put("inLink",
				FeatureNormalizer.inLinks(ac.getTgtCol().getPageInlinks())
						.toString());
		pureFeatureValueMap.put("inLink", ac.getTgtCol().getPageInlinks()
				.toString());

		// outLink 0.0018
		featureValueMap.put("outLink",
				FeatureNormalizer.outLinks(ac.getTgtCol().getPageOutlinks())
						.toString());
		pureFeatureValueMap.put("outLink", ac.getTgtCol().getPageOutlinks()
				.toString());

		// srcTableColIdx -0.6406
		featureValueMap.put("srcTableColIdx",
				ac.getOriginalCol().getColIdx() < 3 ? "1" : "0");
		pureFeatureValueMap.put("srcTableColIdx", ac.getOriginalCol()
				.getColIdx().toString());
		// matchedTableColIdx -0.1173
		featureValueMap.put("matchedTableColIdx",
				ac.getSrcCol().getColIdx() < 3 ? "1" : "0");
		pureFeatureValueMap.put("matchedTableColIdx", ac.getSrcCol()
				.getColIdx().toString());
		// matchedColRedundant 0.7234
		featureValueMap.put("matchedColRedundant", ac.getMatchAttribs()
				.getTgtColMatchesOtherCol() ? "1" : "0");
		pureFeatureValueMap.put("matchedColRedundant", ac.getMatchAttribs()
				.getTgtColMatchesOtherCol() ? "1" : "0");
		// hasMeaningfulCorel 0.0194
		featureValueMap.put("hasMeaningfulCorel", ac.getHasCorel() ? "1" : "0");
		pureFeatureValueMap.put("hasMeaningfulCorel", ac.getHasCorel() ? "1"
				: "0");
		// corelationCoeff 0.9217
		featureValueMap.put("corelationCoeff", ac.getCorelCoeff().toString());
		pureFeatureValueMap.put("corelationCoeff", ac.getCorelCoeff()
				.toString());

		@SuppressWarnings("unchecked")
		Enumeration<Attribute> attribs = data.enumerateAttributes();

		String fStr = "pureFeature::columnMatch|"
				+ ac.getOriginalCol().getPgId() + "~"
				+ ac.getOriginalCol().getTableId() + "\t";
		while (attribs.hasMoreElements()) {
			Attribute a = attribs.nextElement();
			features[idx++] = featureValueMap.get(a.name());
			fStr += pureFeatureValueMap.get(a.name()) + "\t";
		}
//		System.out.print(fStr);
		return features;
	}

	public static void setFeaturesForInstance(Instance inst, String[] features) {

		for (int i = 0; i < features.length; i++) {
			if (inst.attribute(i).isNominal())
				inst.setValue(i, Double.valueOf(features[i]));
			else if (inst.attribute(i).isNumeric())
				inst.setValue(i, Double.valueOf(features[i]));
		}

	}

	public static ArrayList<String[]> getColumnFeaturesForClassifer(
			Connection conn, Instances data, ArrayList<DataLabels> labelledData)
			throws UnsupportedEncodingException, SQLException {
		int size = data.numAttributes();
		ArrayList<String[]> allFeatures = new ArrayList<String[]>(
				labelledData.size());

//		System.out.println("******** Pure Features ");

		for (int i = 0; i < labelledData.size(); i++) {
			String[] features = KGridModelUtils.dataToFeatureValues(
					labelledData.get(i).getAc(), data);
			features[size - 1] = labelledData.get(i).getVote() >= 3 ? "1" : "0";
//			System.out.println(labelledData.get(i).getVote() + "\n");
			allFeatures.add(features);
		}

//		System.out.println("******** Pure Features ");

		return allFeatures;
	}

	public static ArrayList<ArrayList<String>> getColumnFeaturesForRanker(
			Connection conn, Instances data, ArrayList<DataLabels> labelledData)
			throws UnsupportedEncodingException, SQLException {

		String[] allFeatures = new String[labelledData.size()];
		String fStr = "";
		int fNum = 0;
		int size = data.numAttributes();

		ArrayList<ArrayList<String>> rankingLists = new ArrayList<ArrayList<String>>(
				labelledData.size());
		boolean first = true;
		int lastId = 0;
		int currId = 0;
		ArrayList<String> bucket = new ArrayList<String>();
		for (int i = 0; i < labelledData.size(); i++) {
			fStr = "";
			String[] features = KGridModelUtils.dataToFeatureValues(
					labelledData.get(i).getAc(), data);
			features[size - 1] = labelledData.get(i).getVote().toString();

			fStr = features[size - 1] + " " + "qid:"
					+ labelledData.get(i).getAc().getOriginalCol().getTid()
					+ " ";
			for (int j = 0; j < features.length - 1; j++) {
				fNum = j + 1;
				fStr += fNum + ":" + features[j] + " ";
			}

			currId = labelledData.get(i).getAc().getOriginalCol().getTid();

			if (first) {
				bucket = new ArrayList<String>();
				bucket.add(fStr);
				first = false;
			} else {

				if (lastId == currId) {
					bucket.add(fStr);
				} else {
					rankingLists.add(bucket);
					bucket = new ArrayList<String>();
					bucket.add(fStr);
				}

			}

			lastId = currId;

			allFeatures[i] = fStr;

		}

		return rankingLists;
	}

	private static ArrayList<DataLabels> getVotesData(Connection conn,
			LabeledDataType type) throws UnsupportedEncodingException,
			SQLException {
		ArrayList<DataLabels> labelledData = new ArrayList<DataLabels>();

		String tableName = "ratings";

		String query = "SELECT * FROM ratings";
		if (type == LabeledDataType.TEST) {
			tableName = "test_ratings";
			query = "select r.pg_id , r.table_id , col_idx , match_pg_id , "
					+ "match_table_id , match_col_idx , added_col_idx , rank , "
					+ "round(avg(rating)) as rating , src_ip , insertTime "
					+ "FROM "
					+ tableName
					+ " r , table_id_map tim WHERE r.pg_id = tim.pg_id "
					+ "AND r.table_id = tim.table_id GROUP BY tim.id , rank ASC";
		} else if (type == LabeledDataType.EVAL_DATA) {
			query = "SELECT * FROM train_set_used_for_eval";
		}
		
		PreparedStatement ps = conn.prepareStatement(query);

		ResultSet rs = ps.executeQuery();

		HashMap<Integer, HashMap<Integer, Double>> sourcePageSRMeasures = new HashMap<Integer, HashMap<Integer, Double>>();

		HashMap<Integer, Integer> kgridIdToWikiIdMap = new HashMap<Integer, Integer>();

		ColumnAttributes originalCol = null; // Original table's column
												// attributes
		ColumnAttributes srcCol = null; // New Table's column that matches
										// the keys
		// in
		// matchedColIdx column of the original
		// table
		ColumnAttributes tgtCol = null; // Other columns from the new table.

		ColumnMatchAttributes cma;

		Integer sPgId, sTableId, sColId;
		Integer pgId, tableId, keyColId, addedColId;

		Double val = 0.0;
		Double corel = 0.0;
		Double avgNumKeyMaps = 0.0;
		boolean isRedundant = false;
		PreparedStatement corelPs = conn
				.prepareStatement("SELECT cim1.col_idx cid , corel_coeff FROM corels c , col_id_map cim1 , col_id_map cim2 "
						+ "WHERE (cim1.pg_id = ? AND cim1.table_id = ? AND cim1.id = c.src_col_id "
						+ "AND cim2.pg_id = ? AND cim2.table_id = ? AND cim2.col_idx = ? "
						+ "AND cim2.id = c.tgt_col_id) "
						+ "OR (cim1.pg_id = ? AND cim1.table_id = ? AND cim1.col_idx = ? AND cim1.id = c.src_col_id "
						+ "AND cim2.pg_id = ? AND cim2.table_id = ? AND cim2.id = c.tgt_col_id)  "
						+ "ORDER BY ABS(corel_coeff) DESC LIMIT 1;");
		while (rs.next()) {
			sPgId = rs.getInt("pg_id");
			sTableId = rs.getInt("table_id");
			sColId = rs.getInt("col_idx");
			pgId = rs.getInt("match_pg_id");
			tableId = rs.getInt("match_table_id");
			keyColId = rs.getInt("match_col_idx");
			addedColId = rs.getInt("added_col_idx");

			originalCol = KGridUtils.getColumnAttributes(conn, sPgId, sTableId,
					sColId);
			srcCol = KGridUtils.getColumnAttributes(conn, pgId, tableId,
					keyColId);
			tgtCol = KGridUtils.getColumnAttributes(conn, pgId, tableId,
					addedColId);

			isRedundant = KGridUtils.isColRedundant(conn, sPgId, sTableId,
					pgId, tableId, addedColId, tgtCol.getNumRows());
			if (isRedundant)
				continue;

			if (!sourcePageSRMeasures.containsKey(sPgId))
				sourcePageSRMeasures.put(sPgId,
						KGridUtils.getSRMeasureForPage(conn, sPgId));

			avgNumKeyMaps = KGridUtils.getAvgOneToMany(conn, sPgId, sTableId,
					sColId, pgId, tableId, keyColId);

			if (!kgridIdToWikiIdMap.containsKey(pgId)) {
				kgridIdToWikiIdMap.put(pgId,
						KGridUtils.kgridIdToWikiId(conn, pgId));
			}

			cma = KGridUtils.getPairMatchAttributes(conn,
					originalCol.getPgId(), originalCol.getTableId(),
					originalCol.getColIdx(), srcCol.getPgId(),
					srcCol.getTableId(), srcCol.getColIdx(), avgNumKeyMaps);

			cma.setTgtColMatchesOtherCol(false);

			val = 0.0;
			if (sourcePageSRMeasures.containsKey(sPgId)
					&& kgridIdToWikiIdMap.containsKey(pgId)) {
				if (sourcePageSRMeasures.get(sPgId).containsKey(
						kgridIdToWikiIdMap.get(pgId)))
					val = sourcePageSRMeasures.get(sPgId).get(
							kgridIdToWikiIdMap.get(pgId));
			}

			if (cma.getValueMatchPercentage() <= 0)
				continue;
			AddedColumn ac = new AddedColumn(originalCol, srcCol, tgtCol, cma);
			corel = KGridUtils.getCorelCoeff(conn, corelPs, sPgId, sTableId,
					pgId, tableId, addedColId);
			if (corel < -1) {
				ac.setHasCorel(false);
			} else {
				ac.setCorelCoeff(corel);
			}

			DataLabels dl = new DataLabels(ac, rs.getInt("rating"));
			dl.getAc().setOrigPgTargetPgSRVal(val);
			labelledData.add(dl);
		}

		rs.close();
		ps.close();

		return labelledData;
	}

	public static ArrayList<DataLabels> getLabeledData(Connection conn,
			LabeledDataType type) throws SQLException,
			UnsupportedEncodingException {

		ArrayList<DataLabels> labelledData = getVotesData(conn, type);

		if (labelledData.size() == 0) {
			System.out.println("System fall back on heuristic values ");
			LogRegModel.isModelReady = false;
			return new ArrayList<DataLabels>();
		}

		return labelledData;
	}

	public static Ranker learnTableSearchRanker(Connection conn,
			HashSet<String> rankerFeatures)
			throws UnsupportedEncodingException, SQLException {

		System.out.println("Learning ranker ");

		Ranker r = rFact
				.createRanker(ciir.umass.edu.learning.RANKER_TYPE.COOR_ASCENT);

		nml = new ZScoreNormalizor();
		PreparedStatement qidsPS = conn
				.prepareStatement("SELECT qid, query FROM queries WHERE type = ? ");
		qidsPS.setString(1, "train");
		ResultSet qidRS = qidsPS.executeQuery();
		String query = "";
		Integer qid = 0;
		List<RankList> lrl = new ArrayList<RankList>();

		while (qidRS.next()) {
			query = qidRS.getString("query");
			qid = qidRS.getInt("qid");
			lrl.add(SearchUtils.getTrainingRankList(conn, query, qid, "train",
					rankerFeatures));
		}

		int[] features = new int[lrl.get(0).get(0).getFeatureCount()];
		System.out.println("length = " + lrl.get(0).get(0).getFeatureCount());
		for (int i = 0; i < lrl.get(0).get(0).getFeatureCount(); i++) {
			features[i] = i + 1;
		}

//		for (int i = 0; i < lrl.size(); i++) {
//			RankList rl = lrl.get(i);
//			for (int j = 0; j < rl.size(); j++) {
//				System.out.println("PureFeatures::Ranker"
//						+ rl.get(j).toString());
//			}
//		}

		for (int i = 0; i < lrl.size(); i++) {
			nml.normalize(lrl.get(i), features);
		}

		r.set(lrl, features);
		r.set(mFact.createScorer("NDCG@10"));
		ERRScorer.MAX = 5.0;
		r.init();
		r.learn();

		return r;
	}

	public static Ranker learnColumnRankerRanker(Connection conn,
			HashSet<String> rankerFeatures,
			ArrayList<ArrayList<String>> columnRankerTrainData) {
		System.out.println("Learning column ranker ");

		Ranker r = rFact
				.createRanker(ciir.umass.edu.learning.RANKER_TYPE.COOR_ASCENT);

		nml = new ZScoreNormalizor();

		List<RankList> lrl = convertColumnRankerDataToRankList(columnRankerTrainData);

		int[] features = new int[rankerFeatures.size()];
		for (int i = 0; i < rankerFeatures.size(); i++) {
			features[i] = i + 1;
		}

		// for (int i = 0; i < lrl.size(); i++) {
		// nml.normalize(lrl.get(i), features);
		// }

		r.set(lrl, features);
		MetricScorer scorer = mFact.createScorer("NDCG@4");
		// scorer.setK(5);
		r.set(scorer);

		r.init();
		r.learn();

		return r;

	}

	public static List<RankList> convertColumnRankerDataToRankList(
			ArrayList<ArrayList<String>> columnRankerTrainData) {
		List<RankList> lrl = new ArrayList<RankList>();

		for (int i = 0; i < columnRankerTrainData.size(); i++) {
			RankList rl = new RankList();
			for (int j = 0; j < columnRankerTrainData.get(i).size(); j++) {
				rl.add(new DataPoint(columnRankerTrainData.get(i).get(j)));
				System.out.println(columnRankerTrainData.get(i).get(j));
			}
			lrl.add(rl);
		}

		return lrl;
	}

	public static Instance convertAddedColumnToInstance(AddedColumn ac,
			Instances data) {

		int size = data.numAttributes();
		Instance inst = new Instance(size);
		inst.setDataset(data);
		String[] features = KGridModelUtils.dataToFeatureValues(ac, data);
		features[size - 1] = "1";
		KGridModelUtils.setFeaturesForInstance(inst, features);
		return inst;

	}

	public static void storeFeatureSetInContext(ServletContext context,
			InputStream is) throws IOException {

		BufferedReader reader = new BufferedReader(new InputStreamReader(is));

		Instances data = new Instances(reader);

		context.setAttribute("featureSet", data);
	}

	public static void storeColumnRankerFeatures(ServletContext context,
			InputStream is) throws IOException {

		BufferedReader reader = new BufferedReader(new InputStreamReader(is));

		String line = "";
		HashSet<String> rankerFeatures = new HashSet<String>();
		while ((line = reader.readLine()) != null) {
			if (!line.startsWith("#"))
				rankerFeatures.add(line);
			System.out.println(line);
		}
		context.setAttribute("columRankFeatures", rankerFeatures);
	}

}
