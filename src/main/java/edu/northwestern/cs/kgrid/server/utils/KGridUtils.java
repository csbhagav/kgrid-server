package edu.northwestern.cs.kgrid.server.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map.Entry;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import edu.northwestern.cs.kgrid.DAO.KeyColumnData;
import edu.northwestern.cs.kgrid.lib.KgridColumnUtils;
import edu.northwestern.cs.kgrid.server.models.AddableColumnData;
import edu.northwestern.cs.kgrid.server.models.ColumnAttributes;
import edu.northwestern.cs.kgrid.server.models.ColumnMatchAttributes;
import edu.northwestern.cs.kgrid.server.models.RankingType;

public class KGridUtils {

	private static final DecimalFormat twoDForm = new DecimalFormat("#.##");

	public ArrayList<String> getPagesForCategoryList(Connection conn,
			ArrayList<Integer> catIDList) throws SQLException,
			NumberFormatException, UnsupportedEncodingException {

		String origQ = "select wa.title as title from page p1 , categorylinks cl , "
				+ "page p2 , wikiarticles wa where p1.page_namespace = 14 "
				+ "AND p1.page_title = cl.cl_to AND cl.cl_from = p2.page_id "
				+ "AND p2.page_namespace = 0 AND wa.title = p2.page_title AND (";

		ArrayList<String> pageTitleList = new ArrayList<String>();

		String q = " p1.page_id = ? ";
		String conditionQ = "";

		boolean first = true;
		for (int i = 0; i < catIDList.size(); i++) {
			if (first) {
				conditionQ += q;
				first = false;
			} else {
				conditionQ += "OR" + q;
			}
		}

		origQ += conditionQ + ")";

		PreparedStatement pagesForCategoriesPS = conn.prepareStatement(origQ);
		for (int i = 0; i < catIDList.size(); i++) {
			pagesForCategoriesPS.setInt(i + 1, catIDList.get(i));
		}

		ResultSet rs = pagesForCategoriesPS.executeQuery();
		while (rs.next()) {
			byte[] bytes = rs.getBytes("title");
			pageTitleList.add(new String(bytes, "UTF-8"));
		}

		return pageTitleList;
	}

	public ArrayList<String> getCategoryIDsForPage(Connection conn, Integer pgId)
			throws SQLException {
		ArrayList<String> catList = new ArrayList<String>();

		PreparedStatement categoryForPagePS = conn
				.prepareStatement("SELECT c.page_id id FROM "
						+ "categorylinks b , page c WHERE "
						+ "b.cl_from = ? AND b.cl_to = c.page_title AND c.page_namespace = 14 ");
		categoryForPagePS.setInt(1, pgId);

		ResultSet rs = categoryForPagePS.executeQuery();
		while (rs.next()) {
			catList.add(rs.getString("id"));
		}

		return catList;

	}

	public void addVoteOld(Connection conn, Integer srcPgId,
			Integer srcTableId, Integer srcColId, Integer matchPgId,
			Integer matchTableId, Integer matchColId, Integer addedColId,
			Integer vote, String ip) throws SQLException {

		if (isTestPageVote(conn, srcPgId)) {

			PreparedStatement insertPairPS = conn
					.prepareStatement("INSERT IGNORE INTO test_votes "
							+ "(pg_id , table_id , col_idx , match_pg_id , match_table_id , match_col_idx , added_col_idx , up_vote_count , down_vote_count , src_ip) "
							+ "VALUES (? , ? , ? , ? , ? , ? , ? , ? , ? , ?)");

			insertPairPS.setInt(1, srcPgId);
			insertPairPS.setInt(2, srcTableId);
			insertPairPS.setInt(3, srcColId);
			insertPairPS.setInt(4, matchPgId);
			insertPairPS.setInt(5, matchTableId);
			insertPairPS.setInt(6, matchColId);
			insertPairPS.setInt(7, addedColId);
			if (vote == 1) {
				insertPairPS.setInt(8, 1);
				insertPairPS.setInt(9, 0);
			} else {
				insertPairPS.setInt(8, 0);
				insertPairPS.setInt(9, 1);
			}
			insertPairPS.setString(10, ip);
			insertPairPS.execute();
			insertPairPS.close();

			return;

		}

		PreparedStatement insertPairPS = conn
				.prepareStatement("INSERT IGNORE INTO votes "
						+ "(pg_id , table_id , col_idx , match_pg_id , match_table_id , match_col_idx , added_col_idx , up_vote_count , down_vote_count) "
						+ "VALUES (? , ? , ? , ? , ? , ? , ? , ? , ?)");
		insertPairPS.setInt(1, srcPgId);
		insertPairPS.setInt(2, srcTableId);
		insertPairPS.setInt(3, srcColId);
		insertPairPS.setInt(4, matchPgId);
		insertPairPS.setInt(5, matchTableId);
		insertPairPS.setInt(6, matchColId);
		insertPairPS.setInt(7, addedColId);
		insertPairPS.setInt(8, 0);
		insertPairPS.setInt(9, 0);

		insertPairPS.execute();
		insertPairPS.close();

		if (vote == 1) {

			PreparedStatement updateVotePS = conn
					.prepareStatement("UPDATE votes SET up_vote_count = up_vote_count + 1 "
							+ "WHERE pg_id = ? AND table_id = ? AND col_idx = ? AND match_pg_id = ? "
							+ "AND match_table_id = ? AND match_col_idx = ? AND added_col_idx = ? ");

			updateVotePS.setInt(1, srcPgId);
			updateVotePS.setInt(2, srcTableId);
			updateVotePS.setInt(3, srcColId);
			updateVotePS.setInt(4, matchPgId);
			updateVotePS.setInt(5, matchTableId);
			updateVotePS.setInt(6, matchColId);
			updateVotePS.setInt(7, addedColId);
			updateVotePS.execute();
			updateVotePS.close();

		} else if (vote == -1) {
			PreparedStatement updateVotePS = conn
					.prepareStatement("UPDATE votes SET down_vote_count = down_vote_count + 1 "
							+ "WHERE pg_id = ? AND table_id = ? AND col_idx = ? AND match_pg_id = ? "
							+ "AND match_table_id = ? AND match_col_idx = ? AND added_col_idx = ? ");
			updateVotePS.setInt(1, srcPgId);
			updateVotePS.setInt(2, srcTableId);
			updateVotePS.setInt(3, srcColId);
			updateVotePS.setInt(4, matchPgId);
			updateVotePS.setInt(5, matchTableId);
			updateVotePS.setInt(6, matchColId);
			updateVotePS.setInt(7, addedColId);
			updateVotePS.execute();
			updateVotePS.close();

		}

	}

	public void addVote(Connection conn, Integer srcPgId, Integer srcTableId,
			Integer srcColId, Integer matchPgId, Integer matchTableId,
			Integer matchColId, Integer addedColId, Integer vote, String ip,
			Integer order) throws SQLException {

		String tableName = "votes";
		if (isTestPageVote(conn, srcPgId)) {
			tableName = "test_votes";
		}
		PreparedStatement insertPairPS = conn
				.prepareStatement("INSERT IGNORE INTO "
						+ tableName
						+ " (pg_id , table_id , col_idx , match_pg_id , match_table_id , "
						+ "match_col_idx , added_col_idx , up_vote_count , down_vote_count , src_ip , rank) "
						+ "VALUES (? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ?)");

		insertPairPS.setInt(1, srcPgId);
		insertPairPS.setInt(2, srcTableId);
		insertPairPS.setInt(3, srcColId);
		insertPairPS.setInt(4, matchPgId);
		insertPairPS.setInt(5, matchTableId);
		insertPairPS.setInt(6, matchColId);
		insertPairPS.setInt(7, addedColId);
		if (vote == 1) {
			insertPairPS.setInt(8, 1);
			insertPairPS.setInt(9, 0);
		} else {
			insertPairPS.setInt(8, 0);
			insertPairPS.setInt(9, 1);
		}
		insertPairPS.setString(10, ip);
		insertPairPS.setInt(11, order);
		insertPairPS.execute();
		insertPairPS.close();

		return;

	}

	public void addRating(Connection conn, Integer srcPgId, Integer srcTableId,
			Integer srcColId, Integer matchPgId, Integer matchTableId,
			Integer matchColId, Integer addedColId, Integer rating, String ip,
			Integer order) throws SQLException {

		String tableName = "ratings";
		if (isTestPageVote(conn, srcPgId)) {
			tableName = "test_ratings";
		}
		PreparedStatement insertPairPS = conn
				.prepareStatement("INSERT IGNORE INTO "
						+ tableName
						+ " (pg_id , table_id , col_idx , match_pg_id , match_table_id , "
						+ "match_col_idx , added_col_idx , rating , src_ip , rank) "
						+ "VALUES (? , ? , ? , ? , ? , ? , ? ,  ? , ? , ?)");

		insertPairPS.setInt(1, srcPgId);
		insertPairPS.setInt(2, srcTableId);
		insertPairPS.setInt(3, srcColId);
		insertPairPS.setInt(4, matchPgId);
		insertPairPS.setInt(5, matchTableId);
		insertPairPS.setInt(6, matchColId);
		insertPairPS.setInt(7, addedColId);
		insertPairPS.setInt(8, rating);
		insertPairPS.setString(9, ip);
		insertPairPS.setInt(10, order);
		insertPairPS.execute();
		insertPairPS.close();

		return;

	}

	private boolean isTestPageVote(Connection conn, Integer srcPgId)
			throws SQLException {

		PreparedStatement ps = conn
				.prepareStatement("SELECT count(*) as c FROM test_set WHERE id = ? ");
		ps.setInt(1, srcPgId);
		ResultSet rs = ps.executeQuery();
		boolean isTest = false;
		while (rs.next()) {
			if (rs.getInt("c") > 0)
				isTest = true;
		}
		rs.close();
		ps.close();

		return isTest;
	}

	public AddableColumnData getAddableColumnData(Connection conn,
			Integer pgId, Integer tableId, Integer keyColId, Integer valColId)
			throws SQLException, UnsupportedEncodingException {

		PreparedStatement ps = conn
				.prepareStatement("SELECT col_idx cid, GROUP_CONCAT(cd.text ORDER BY td.row_idx SEPARATOR '\\t') vals "
						+ "FROM table_data td , cell_details cd where td.pg_id = ? AND td.table_id = ? "
						+ "AND (td.col_idx = ? OR td.col_idx = ?) AND td.cellID = cd.cellID group by col_idx");

		ps.setInt(1, pgId);
		ps.setInt(2, tableId);
		ps.setInt(3, keyColId);
		ps.setInt(4, valColId);
		ResultSet rs = ps.executeQuery();

		String[] keyColVals = new String[0];
		String[] valColVals = new String[0];
		byte[] bytes;
		while (rs.next()) {

			int cid = rs.getInt("cid");
			bytes = rs.getBytes("vals");
			if (cid == keyColId) {
				keyColVals = new String(bytes, "UTF-8").split("\t");
			} else {
				valColVals = new String(bytes, "UTF-8").split("\t");
			}

		}

		// for (int i = 0; i < keyColVals.length; i++) {
		// System.out.println(keyColVals[i]);
		// }

		AddableColumnData acd = new AddableColumnData();

		for (int i = 0; i < keyColVals.length && i < valColVals.length; i++) {
			if (!keyColVals[i].trim().equalsIgnoreCase(""))
				acd.addKeyValPair(keyColVals[i], valColVals[i]);
		}

		return acd;

	}

	public AddableColumnData getAddableColumnData(Connection conn,
			Integer origPgId, Integer origTableId, Integer origColId,
			Integer pgId, Integer tableId, Integer keyColId, Integer valColId)
			throws SQLException, UnsupportedEncodingException {

		KeyColumnData sCol = KgridColumnUtils.getKeyColumn(conn, origPgId,
				origTableId, origColId);

		sCol.setCellIdRowIdx(getCellIdRowIdxMap(conn, origPgId, origTableId,
				origColId));

		KeyColumnData tCol = KgridColumnUtils.getKeyColumn(conn, pgId, tableId,
				keyColId);
		tCol.setCellIdRowIdx(getCellIdRowIdxMap(conn, pgId, tableId, keyColId));

		HashMap<Integer, ArrayList<Integer>> indexMap = KgridColumnUtils
				.createKeyColumnsMap(sCol, tCol);

		HashMap<Integer, String> data = rowWiseColumnData(conn, pgId, tableId,
				valColId);
		HashMap<Integer, String> keyData = rowWiseColumnData(conn, origPgId,
				origTableId, origColId);

		AddableColumnData acd = new AddableColumnData();
		for (Entry<Integer, ArrayList<Integer>> e : indexMap.entrySet()) {
			for (int i = 0; i < e.getValue().size(); i++) {
				acd.addKeyValPair(keyData.get(e.getKey()),
						data.get(e.getValue().get(i)));
			}
		}

		PreparedStatement corelPs = conn
				.prepareStatement("SELECT cim1.col_idx cid , corel_coeff FROM corels c , col_id_map cim1 , col_id_map cim2 "
						+ "WHERE (cim1.pg_id = ? AND cim1.table_id = ? AND cim1.id = c.src_col_id "
						+ "AND cim2.pg_id = ? AND cim2.table_id = ? AND cim2.col_idx = ? "
						+ "AND cim2.id = c.tgt_col_id) "
						+ "OR (cim1.pg_id = ? AND cim1.table_id = ? AND cim1.col_idx = ? AND cim1.id = c.src_col_id "
						+ "AND cim2.pg_id = ? AND cim2.table_id = ? AND cim2.id = c.tgt_col_id)  "
						+ "ORDER BY ABS(corel_coeff) DESC LIMIT 1;");
		HashMap<Integer, Double> corel = KGridUtils.getCorelCoeffWithColId(
				conn, corelPs, origPgId, origTableId, pgId, tableId, valColId);
		acd.setCorrelCoeff(corel);
		corelPs.close();

		return acd;

	}

	public static HashMap<Integer, String> rowWiseColumnData(Connection conn,
			Integer pgId, Integer tableId, Integer colId) throws SQLException,
			UnsupportedEncodingException {
		PreparedStatement keyDataPs = conn
				.prepareStatement("SELECT td.row_idx r, cd.text t FROM table_data td , cell_details cd "
						+ "WHERE td.pg_id = ? AND td.table_id = ? AND td.col_idx = ? "
						+ "AND td.cellID = cd.cellID ORDER BY cd.cellID;");
		keyDataPs.setInt(1, pgId);
		keyDataPs.setInt(2, tableId);
		keyDataPs.setInt(3, colId);
		ResultSet keyDataRs = keyDataPs.executeQuery();
		HashMap<Integer, String> keyData = new HashMap<Integer, String>();

		while (keyDataRs.next()) {

			keyData.put(keyDataRs.getInt("r"),
					new String(keyDataRs.getBytes("t"), "UTF-8"));

		}
		keyDataRs.close();
		keyDataPs.close();

		return keyData;
	}

	public static HashMap<Integer, Integer> getCellIdRowIdxMap(Connection conn,
			Integer pgId, Integer tableId, Integer colId) throws SQLException {

		HashMap<Integer, Integer> m = new HashMap<Integer, Integer>();
		PreparedStatement ps = conn
				.prepareStatement("SELECT cellID , row_idx FROM table_data WHERE pg_id = ? AND table_id = ? AND col_idx = ? ");
		ps.setInt(1, pgId);
		ps.setInt(2, tableId);
		ps.setInt(3, colId);
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {

			m.put(rs.getInt("cellID"), rs.getInt("row_idx"));

		}

		return m;
	}

	public static ColumnAttributes getColumnAttributes(Connection conn,
			Integer pgId, Integer tableId, Integer colId) throws SQLException,
			UnsupportedEncodingException {

		Double numeColumnFraction = getNumericColumnFraction(conn, pgId,
				tableId);
		String columnAttrSelectQ = "SELECT td.pg_id pid, td.table_id tid, td.col_idx cid, "
				+ "ca.isNumeric isNumeric , ca.dataAvgLen dataAvgLen , ca.numUniqVals numUniqVals ,  ca.numRows numRows , "
				+ "GROUP_CONCAT(cd.text ORDER BY td.row_idx SEPARATOR '\\t') vals, "
				+ "GROUP_CONCAT(cd.entityIds ORDER BY td.row_idx SEPARATOR '\\t') entities, "
				+ "wlc.inlinks inlinks , wlc.outlinks outlinks, tim.id timId "
				+ "FROM table_data td , cell_details cd , column_attrs ca , "
				+ "wikiarticles_link_counts wlc , table_id_map tim "
				+ "WHERE td.pg_id =  ?  AND td.table_id = ? and td.col_idx = ? "
				+ "AND td.cellID  = cd.cellID AND ca.pg_id = td.pg_id AND ca.table_id = td.table_id "
				+ "AND ca.col_idx = td.col_idx AND wlc.pg_id = td.pg_id "
				+ "AND tim.pg_id = td.pg_id AND tim.table_id = td.table_id "
				+ "GROUP BY td.pg_id , td.table_id , td.col_idx";

		PreparedStatement ps = conn.prepareStatement(columnAttrSelectQ);
		ps.setInt(1, pgId);
		ps.setInt(2, tableId);
		ps.setInt(3, colId);

		ResultSet rs = ps.executeQuery();
		rs.next();
		ColumnAttributes ca = getColumnAttributes(rs, numeColumnFraction);
		rs.close();
		ps.close();

		return ca;

	}

	public static ColumnAttributes getColumnAttributes(ResultSet rs,
			Double numeColumnFraction) throws UnsupportedEncodingException,
			SQLException {
		Integer tgtPgId = rs.getInt("pid");
		Integer tgtTableId = rs.getInt("tid");
		Integer tgtColIdx = rs.getInt("cid");
		Boolean isNumeric = rs.getBoolean("isNumeric");
		Double avgLen = rs.getDouble("dataAvgLen");
		Integer numUniqVals = rs.getInt("numUniqVals");
		Integer numRows = rs.getInt("numRows");
		String[] vals = new String(rs.getBytes("vals"), "UTF-8").split("\t");
		String[] entities = new String(rs.getBytes("entities"), "UTF-8")
				.split("\t");
		Integer inlinks = rs.getInt("inlinks");
		Integer outlinks = rs.getInt("outlinks");
		Integer timId = rs.getInt("timId");

		ColumnAttributes columnAttr = new ColumnAttributes(tgtPgId, tgtTableId,
				tgtColIdx, isNumeric, avgLen, numUniqVals, numRows, vals,
				entities, inlinks, outlinks, timId, numeColumnFraction);
		return columnAttr;
	}

	public static ColumnMatchAttributes getPairMatchAttributes(Connection conn,
			Integer srcPgId, Integer srcTableId, Integer srcColId,
			Integer tgtPgId, Integer tgtTableId, Integer tgtColId,
			Double avgNumKeyMaps) throws SQLException {

		String q = "SELECT cm.numEntityMatches numEntityMatches, cm.numValueMatches numValueMatches, "
				+ "cm.totalMatches totalMatches, cm.totalMatchPercent totalMatchPercent, "
				+ "cm.entityMatchPercent entityMatchPercent, cm.valueMatchPercentage valueMatchPercentage "
				+ "FROM column_matches cm "
				+ "WHERE cm.src_pg_id = ? AND cm.src_table_id = ? AND cm.src_col_idx = ? "
				+ "AND cm.tgt_pg_id = ? AND cm.tgt_table_id = ? AND cm.tgt_col_idx = ? ";

		PreparedStatement ps = conn.prepareStatement(q);

		ps.setInt(1, srcPgId);
		ps.setInt(2, srcTableId);
		ps.setInt(3, srcColId);
		ps.setInt(4, tgtPgId);
		ps.setInt(5, tgtTableId);
		ps.setInt(6, tgtColId);

		ResultSet rs = ps.executeQuery();
		if (rs.next()) {
			ColumnMatchAttributes cma = getColumnMatchAttributes(rs,
					avgNumKeyMaps);
			return cma;
		}

		return new ColumnMatchAttributes(0, 0, 0, 0.0, 0.0, 0.0, 0.0);
	}

	public static ColumnMatchAttributes getColumnMatchAttributes(ResultSet rs,
			Double avgNumKeyMaps) throws SQLException {

		Integer numEntityMatches = rs.getInt("numEntityMatches");
		Integer numValueMatches = rs.getInt("numValueMatches");
		Integer totalMatches = rs.getInt("totalMatches");
		Double totalMatchPercent = rs.getDouble("totalMatchPercent");
		Double entityMatchPercent = rs.getDouble("entityMatchPercent");
		Double valueMatchPercentage = rs.getDouble("valueMatchPercentage");

		ColumnMatchAttributes matchAttr = new ColumnMatchAttributes(
				numEntityMatches, numValueMatches, totalMatches,
				totalMatchPercent, entityMatchPercent, valueMatchPercentage,
				avgNumKeyMaps);

		return matchAttr;
	}

	public static HashMap<Integer, Integer> kgridIdToWikiId(Connection conn,
			HashSet<Integer> kgridIds) throws SQLException {

		HashMap<Integer, Integer> idMap = new HashMap<Integer, Integer>();

		if (kgridIds.size() == 0)
			return idMap;
		String qPart = " wa.pg_id = ? ";
		String q = "SELECT wa.pg_id kgridId , p.page_id wikiId FROM wikiarticles wa , page p "
				+ "WHERE wa.title = p.page_title AND p.page_namespace = 0 AND ( ";

		for (int i = 0; i < kgridIds.size(); i++) {
			q += qPart + " OR ";
		}

		q = q.substring(0, q.length() - 3);
		q += " )";

		PreparedStatement ps = conn.prepareStatement(q);
		Iterator<Integer> it = kgridIds.iterator();
		int paramIdx = 1;
		while (it.hasNext()) {
			ps.setInt(paramIdx++, it.next());
		}

		ResultSet rs = ps.executeQuery();

		while (rs.next()) {
			idMap.put(rs.getInt("kgridId"), rs.getInt("wikiId"));
		}

		return idMap;
	}

	public static Integer kgridIdToWikiId(Connection conn, Integer kgridId)
			throws SQLException {

		String q = "SELECT wa.pg_id kgridId , p.page_id wikiId FROM wikiarticles wa , page p "
				+ "WHERE wa.title = p.page_title AND p.page_namespace = 0 AND wa.pg_id = ?  ";

		PreparedStatement ps = conn.prepareStatement(q);
		ps.setInt(1, kgridId);

		ResultSet rs = ps.executeQuery();
		Integer wikiId = 0;
		while (rs.next()) {
			wikiId = rs.getInt("wikiId");
		}
		rs.close();
		ps.close();

		return wikiId;
	}

	public static HashMap<Integer, Double> getSRMeasureForPage(Connection conn,
			Integer pgId) {

		HashMap<Integer, Double> srRelations = new HashMap<Integer, Double>();
		try {
			PreparedStatement ps = conn
					.prepareStatement("SELECT p.page_id wikiID FROM page p , wikiarticles wa "
							+ "WHERE wa.title = p.page_title and p.page_namespace= 0 "
							+ "AND wa.pg_id = ? ");
			ps.setInt(1, pgId);

			ResultSet rs = ps.executeQuery();
			Integer wikiPgId = -1;
			while (rs.next()) {
				wikiPgId = rs.getInt("wikiID");
			}
			rs.close();
			ps.close();

			if (wikiPgId == -1)
				return new HashMap<Integer, Double>();

			URL u = new URL(
					"http://downey-n2.cs.northwestern.edu:8080/wikisr/sr/sID/"
							+ wikiPgId + "/langID/1");

			HttpURLConnection uc = (HttpURLConnection) u.openConnection();

			int responseCode = uc.getResponseCode();

			String responseString = "";
			if (200 == responseCode || 401 == responseCode
					|| 404 == responseCode) {
				BufferedReader rd = new BufferedReader(new InputStreamReader(
						responseCode == 200 ? uc.getInputStream()
								: uc.getErrorStream()));
				StringBuffer sb = new StringBuffer();
				String line;
				while ((line = rd.readLine()) != null) {
					sb.append(line);
				}
				rd.close();

				responseString = sb.toString();
			}

			JSONObject jo = new JSONObject(responseString);
			Integer relatedWikiID = 0;
			Double srMeasure = 0.0;
			JSONArray ja = jo.getJSONArray("result");

			srRelations = new HashMap<Integer, Double>(ja.length());

			for (int i = 0; i < ja.length(); i++) {
				relatedWikiID = ja.getJSONObject(i).getInt("wikiPageId");
				srMeasure = ja.getJSONObject(i).getDouble("srMeasure");

				srRelations.put(relatedWikiID, srMeasure);

			}
			rs.close();

		} catch (SQLException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			// srRelations = new HashMap<Integer, Double>();
		}
		return srRelations;
	}

	public static Boolean isColRedundant(Connection conn, Integer pgId,
			Integer tableId, Integer tgtPgId, Integer tgtTableId,
			Integer tgtColIdx, Integer numRows) throws SQLException {

		PreparedStatement ps = conn
				.prepareStatement("SELECT id FROM col_id_map WHERE pg_id = ? AND table_id = ?");
		ps.setInt(1, pgId);
		ps.setInt(2, tableId);
		ResultSet rs = ps.executeQuery();

		PreparedStatement matchPS = conn
				.prepareStatement("SELECT count(*) as c "
						+ "FROM text_col_revidx t1 , text_col_revidx t2 , col_id_map cim "
						+ "WHERE t1.colID = ? AND t2.colID = cim.id AND cim.pg_id = ? "
						+ "AND cim.table_id = ? AND cim.col_idx = ? AND t1.textID = t2.textID ");

		matchPS.setInt(2, tgtPgId);
		matchPS.setInt(3, tgtTableId);
		matchPS.setInt(4, tgtColIdx);

		ResultSet matchRs;
		int numMatches = 0;
		boolean redundant = false;
		while (rs.next()) {

			matchPS.setInt(1, rs.getInt("id"));
			matchRs = matchPS.executeQuery();

			while (matchRs.next()) {
				numMatches = matchRs.getInt("c");
				if (numMatches > numRows / 5)
					redundant = true;
				break;
			}
			matchRs.close();
			if (redundant)
				break;

		}
		matchPS.close();
		rs.close();
		ps.close();

		return redundant;
	}

	public static Double getCorelCoeff(Connection conn,
			PreparedStatement corelPs, int originalTablePgId,
			int originalTableTableId, int targetColPgId, int targetColTableId,
			int targetColId) throws SQLException {

		corelPs.setInt(1, originalTablePgId);
		corelPs.setInt(2, originalTableTableId);
		corelPs.setInt(3, targetColPgId);
		corelPs.setInt(4, targetColTableId);
		corelPs.setInt(5, targetColId);

		corelPs.setInt(6, targetColPgId);
		corelPs.setInt(7, targetColTableId);
		corelPs.setInt(8, targetColId);
		corelPs.setInt(9, originalTablePgId);
		corelPs.setInt(10, originalTableTableId);

		ResultSet rs = corelPs.executeQuery();
		Double ret = -200.0;
		while (rs.next()) {
			ret = rs.getDouble("corel_coeff");
		}

		return ret;
	}

	public static HashMap<Integer, Double> getCorelCoeffWithColId(
			Connection conn, PreparedStatement corelPs, int originalTablePgId,
			int originalTableTableId, int targetColPgId, int targetColTableId,
			int targetColId) throws SQLException {

		corelPs.setInt(1, originalTablePgId);
		corelPs.setInt(2, originalTableTableId);
		corelPs.setInt(3, targetColPgId);
		corelPs.setInt(4, targetColTableId);
		corelPs.setInt(5, targetColId);

		corelPs.setInt(6, targetColPgId);
		corelPs.setInt(7, targetColTableId);
		corelPs.setInt(8, targetColId);
		corelPs.setInt(9, originalTablePgId);
		corelPs.setInt(10, originalTableTableId);

		ResultSet rs = corelPs.executeQuery();
		HashMap<Integer, Double> colCorelMap = new HashMap<Integer, Double>();
		while (rs.next()) {
			colCorelMap.put(rs.getInt("cid"), Double.valueOf(twoDForm.format(rs
					.getDouble("corel_coeff"))));

		}

		return colCorelMap;

	}

	public static String getOrderByStr(RankingType type) {

		String q = "ORDER BY ";
		if (type == RankingType.RANDOM) {
			q += "RAND() ";
		} else if (type == RankingType.MATCH_PERC) {
			q += "orderVal2 DESC ";
		} else if (type == RankingType.CLSFR_AND_RANKER) {
			q += "(orderVal1 * orderVal2) / (orderVal1 + orderVal2) DESC ";
		}

		q += "LIMIT 100";

		return q;
	}

	public static Double getAvgOneToMany(Connection conn, Integer origPgId,
			Integer origTableId, Integer origColId, Integer pgId,
			Integer tableId, Integer keyColId) throws SQLException,
			UnsupportedEncodingException {
		KeyColumnData sCol = KgridColumnUtils.getKeyColumn(conn, origPgId,
				origTableId, origColId);

		sCol.setCellIdRowIdx(KGridUtils.getCellIdRowIdxMap(conn, origPgId,
				origTableId, origColId));

		KeyColumnData tCol = KgridColumnUtils.getKeyColumn(conn, pgId, tableId,
				keyColId);
		tCol.setCellIdRowIdx(KGridUtils.getCellIdRowIdxMap(conn, pgId, tableId,
				keyColId));

		HashMap<Integer, ArrayList<Integer>> indexMap = KgridColumnUtils
				.createKeyColumnsMap(sCol, tCol);
		int total = 0;

		for (Entry<Integer, ArrayList<Integer>> e : indexMap.entrySet()) {
			total += e.getValue().size();
		}

		return (double) total / (double) indexMap.size();

	}

	public static Double getNumericColumnFraction(Connection conn,
			Integer pgId, Integer tableId) throws SQLException {

		PreparedStatement ps = conn
				.prepareStatement("SELECT sum(isNumeric) num , count(*) tot "
						+ "FROM column_attrs WHERE pg_id = ? AND table_id = ? ");
		ps.setInt(1, pgId);
		ps.setInt(2, tableId);
		ResultSet rs = ps.executeQuery();
		Double countNum = 0.0;
		Double total = 0.0;
		while (rs.next()) {

			countNum = rs.getDouble("num");
			total = rs.getDouble("tot");

		}

		Double fraction = countNum / total;
		return fraction;
	}

}
