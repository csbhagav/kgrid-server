package edu.northwestern.cs.kgrid.server.utils;

import edu.northwestern.cs.WIG.lib.MySQLQueryHandler;
import edu.northwestern.cs.kgrid.config.ConfigReader;

public class MysqlUtils {
	public static MySQLQueryHandler getMysqlHandler(ConfigReader conf,
			String namespace) throws InstantiationException,
			IllegalAccessException, ClassNotFoundException {
		String host = conf.getValue(namespace + ":dbhost");
		String db = conf.getValue(namespace + ":dbname");
		String user = conf.getValue(namespace + ":user");
		String pwd = conf.getValue(namespace + ":password");

		MySQLQueryHandler handler = new MySQLQueryHandler(host, db, user, pwd);
		return handler;
	}
}
