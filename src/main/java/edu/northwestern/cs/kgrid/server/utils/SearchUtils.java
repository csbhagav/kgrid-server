package edu.northwestern.cs.kgrid.server.utils;

import java.io.UnsupportedEncodingException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;

import ciir.umass.edu.learning.DataPoint;
import ciir.umass.edu.learning.RankList;

import edu.northwestern.cs.kgrid.server.models.SearchResultTableDetails;
import edu.northwestern.edu.cs.tableSearch.TableSearchFeatures;
import edu.northwestern.edu.cs.tableSearch.TableSearchUtils;

public class SearchUtils {

	public static String getTrainingFeatures(Connection conn, String type)
			throws SQLException, UnsupportedEncodingException {

		String trainingSetFeatures = "";

		PreparedStatement qidsPS = conn
				.prepareStatement("SELECT qid, query FROM queries WHERE type = ?");

		PreparedStatement tableIdsPS = conn
				.prepareStatement("SELECT rank , table_id , rating FROM ts_train WHERE qid = ? ");
		ResultSet tableIdsRS;

		qidsPS.setString(1, type);

		ResultSet qidsRS = qidsPS.executeQuery();
		Integer qid = 0;
		String query = "";
		Integer tableId = 0;
		Double rating = 0.0;
		Integer origRank = 0;
		while (qidsRS.next()) {

			qid = qidsRS.getInt("qid");
			query = qidsRS.getString("query");

			tableIdsPS.setInt(1, qid);
			tableIdsRS = tableIdsPS.executeQuery();
			while (tableIdsRS.next()) {
				tableId = tableIdsRS.getInt("table_id");

				origRank = tableIdsRS.getInt("rank");
				rating = tableIdsRS.getDouble("rating");
				TableSearchFeatures tsf = TableSearchUtils.generateFeatures(
						conn, query, tableId, origRank);
				// trainingSetFeatures += rating + " " + "qid:" + qid + " "
				// + tsf.getFeatureString() + "\n";

			}

		}
		tableIdsPS.close();
		qidsPS.close();

		return trainingSetFeatures;
	}

	public static RankList getTrainingRankList(Connection conn, String query,
			Integer qid, String type, HashSet<String> rankerFeatures)
			throws SQLException, UnsupportedEncodingException {

		RankList rl = new RankList();

		PreparedStatement tableIdsPS = conn
				.prepareStatement("SELECT rank , table_id , rating FROM ts_train WHERE qid = ? limit 5");
		ResultSet tableIdsRS;

		Integer tableId = 0;
		Double rating = 0.0;
		Integer origRank = 0;

		tableIdsPS.setInt(1, qid);
		tableIdsRS = tableIdsPS.executeQuery();
		while (tableIdsRS.next()) {
			tableId = tableIdsRS.getInt("table_id");

			origRank = tableIdsRS.getInt("rank");
			rating = tableIdsRS.getDouble("rating");
			TableSearchFeatures tsf = TableSearchUtils.generateFeatures(conn,
					query, tableId, origRank);
			rl.add(new DataPoint(rating + " " + "qid:" + qid + " "
					+ tsf.getFeatureString(rankerFeatures)));
		}
		tableIdsRS.close();
		tableIdsPS.close();

		return rl;

	}

	public static SearchResultTableDetails getTableDetails(Connection conn,
			Integer tableId, Integer offSet) throws SQLException,
			UnsupportedEncodingException {

		SearchResultTableDetails tableDet = new SearchResultTableDetails();

		PreparedStatement tableMetaDataPS = conn
				.prepareStatement("SELECT wa.pg_id , id , caption , numHeaderRows , numDataRows , numColumns "
						+ "FROM wikitables wa , table_attrs ta  "
						+ "WHERE table_id = ? and table_id = wikitable_id");

		tableMetaDataPS.setInt(1, tableId);

		ResultSet tableMetaDataRS = tableMetaDataPS.executeQuery();
		String caption = "";
		Integer numHeader = 0;
		Integer numData = 0;
		Integer numCol = 0;
		Integer pgId = 0;
		Integer tId = 0;
		while (tableMetaDataRS.next()) {
			caption = new String(tableMetaDataRS.getBytes("caption"), "UTF-8");
			numHeader = tableMetaDataRS.getInt("numHeaderRows");
			numData = tableMetaDataRS.getInt("numDataRows");
			numCol = tableMetaDataRS.getInt("numColumns");
			pgId = tableMetaDataRS.getInt("pg_id");
			tId = tableMetaDataRS.getInt("id");
		}

		tableDet.setNumColumns(numCol);
		tableDet.setNumDataRows(numData);
		tableDet.setNumHeaderRows(numHeader);
		tableDet.setHeaderRows(getRows(conn, pgId, tId, 0, 1, "table_headers"));
		tableDet.setSampleRows(getRows(conn, pgId, tId, offSet, 2, "table_data"));
		tableDet.setCaption(caption);

		tableMetaDataPS.close();
		tableMetaDataRS.close();

		return tableDet;
	}

	public static ArrayList<ArrayList<String>> getRows(Connection conn,
			Integer pgId, Integer tableId, Integer offset, Integer numRows,
			String table) throws SQLException, UnsupportedEncodingException {
		ArrayList<ArrayList<String>> rows = new ArrayList<ArrayList<String>>();
		PreparedStatement tableHeaderRowDetailsPS = conn
				.prepareStatement("select td.row_idx , GROUP_CONCAT(cd.text ORDER BY td.col_idx SEPARATOR  '\\t') vals "
						+ "FROM "
						+ table
						+ " td , cell_details cd where td.pg_id = ? "
						+ "AND td.table_id  = ? and td.cellID = cd.cellID "
						+ "AND td.row_idx >= ? AND td.row_idx <= ? "
						+ "GROUP BY td.row_idx ;");

		tableHeaderRowDetailsPS.setInt(1, pgId);
		tableHeaderRowDetailsPS.setInt(2, tableId);
		tableHeaderRowDetailsPS.setInt(3, offset - 1);
		tableHeaderRowDetailsPS.setInt(4, offset < 1 ? 3 : offset + 1);
		ResultSet tableHeaderRowDetailsRS = tableHeaderRowDetailsPS
				.executeQuery();
		while (tableHeaderRowDetailsRS.next()) {
			ArrayList<String> row = new ArrayList<String>();
			String[] parts = new String(
					tableHeaderRowDetailsRS.getBytes("vals"), "UTF-8")
					.split("\t");

			for (int i = 0; i < parts.length; i++) {
				row.add(parts[i]);
			}
			rows.add(row);
		}
		tableHeaderRowDetailsPS.close();
		tableHeaderRowDetailsRS.close();
		return rows;
	}

	public static SearchResultTableDetails getTableDetails(Connection conn,
			Integer pgId, Integer tableId, Integer offSet) throws SQLException,
			UnsupportedEncodingException {

		PreparedStatement ps = conn
				.prepareStatement("SELECT * FROM table_attrs WHERE pg_id = ? AND id = ? ");
		ps.setInt(1, pgId);
		ps.setInt(2, tableId);

		ResultSet rs = ps.executeQuery();

		Integer wikitablesTableId = 0;

		while (rs.next()) {
			wikitablesTableId = rs.getInt("wikitable_id");
		}

		return getTableDetails(conn, wikitablesTableId, offSet);

	}

}
