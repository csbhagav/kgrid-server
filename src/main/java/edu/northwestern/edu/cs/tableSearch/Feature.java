package edu.northwestern.edu.cs.tableSearch;

public class Feature {

	Integer id;
	String key;
	Double value;
	
	public Feature(Integer id, String key, Double value) {
		super();
		this.id = id;
		this.key = key;
		this.value = value;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public Double getValue() {
		return value;
	}
	public void setValue(Double value) {
		this.value = value;
	}
	
}
