package edu.northwestern.edu.cs.tableSearch;

import java.util.ArrayList;

public class QueryFeatures {

	ArrayList<Feature> features = new ArrayList<Feature>();

	public void addFeature(Feature f) {
		features.add(f);
	}

	public ArrayList<Feature> getFeatures() {
		return features;
	}

	public void setFeatures(ArrayList<Feature> features) {
		this.features = features;
	}

}
