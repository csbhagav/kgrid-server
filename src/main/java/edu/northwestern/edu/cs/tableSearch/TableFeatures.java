package edu.northwestern.edu.cs.tableSearch;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class TableFeatures {
	ArrayList<Feature> features;

	public TableFeatures(Connection conn, Integer tableId) throws SQLException {
		features = new ArrayList<Feature>();

		PreparedStatement tableFeaturePS = conn
				.prepareStatement("SELECT wlc.inlinks inlinks, wlc.outlinks outlinks, wlc.pageViews pv "
						+ "FROM wikiarticles_link_counts wlc , wikiarticles wa , table_attrs ta "
						+ "WHERE ta.wikitable_id = ? AND ta.pg_id = wa.pg_id AND wa.pg_id = wlc.pg_id ORDER BY numDataRows DESC LIMIT 1");
		tableFeaturePS.setInt(1, tableId);
		ResultSet tableFeaturesRs = tableFeaturePS.executeQuery();
		int id = 1;
		Double inlinks = 0.0;
		Double outlinks = 0.0;
		Double pv = 0.0;
		while (tableFeaturesRs.next()) {
			inlinks = tableFeaturesRs.getDouble("inlinks");
			outlinks = tableFeaturesRs.getDouble("outlinks");
			pv = tableFeaturesRs.getDouble("pv");

		}
		addFeature(new Feature(id++, "inlinks", inlinks));
		addFeature(new Feature(id++, "outlinks", outlinks));
		addFeature(new Feature(id++, "pageViews", pv));

		tableFeaturePS.close();
		tableFeaturesRs.close();

		tableFeaturePS = conn
				.prepareStatement("SELECT numDataRows , numColumns , section "
						+ "FROM table_attrs , wikitables WHERE wikitable_id = ? AND wikitable_id = table_id ORDER BY numDataRows DESC LIMIT 1");
		tableFeaturePS.setInt(1, tableId);
		tableFeaturesRs = tableFeaturePS.executeQuery();
		Double rows = 0.0;
		Double cols = 0.0;
		Double section = 0.0;
		while (tableFeaturesRs.next()) {

			rows = tableFeaturesRs.getDouble("numDataRows");
			cols = tableFeaturesRs.getDouble("numColumns");
			section = tableFeaturesRs.getDouble("section");

		}
		addFeature(new Feature(id++, "numRows", rows));
		addFeature(new Feature(id++, "numCols", cols));
		addFeature(new Feature(id++, "sectionNumber", section));

		tableFeaturePS.close();
		tableFeaturesRs.close();

		tableFeaturePS = conn
				.prepareStatement("SELECT count(*) c FROM table_attrs ta1 , table_attrs ta2 "
						+ "WHERE ta1.wikitable_id = ? AND ta1.pg_id = ta2.pg_id ");
		tableFeaturePS.setInt(1, tableId);
		tableFeaturesRs = tableFeaturePS.executeQuery();
		while (tableFeaturesRs.next()) {
			addFeature(new Feature(id++, "tableImportance",
					1.0 / tableFeaturesRs.getDouble("c")));
		}
		tableFeaturePS.close();
		tableFeaturesRs.close();

		tableFeaturePS = conn
				.prepareStatement("SELECT count(*) c FROM table_attrs ta , table_data td , cell_details cd "
						+ "WHERE ta.wikitable_id = ? AND ta.pg_id = td.pg_id  AND ta.id = td.table_id "
						+ "AND td.cellID = cd.cellID AND cd.text = \"\"");
		tableFeaturePS.setInt(1, tableId);
		tableFeaturesRs = tableFeaturePS.executeQuery();
		while (tableFeaturesRs.next()) {
			if (rows * cols != 0)
				addFeature(new Feature(id++, "emptyCellPercent",
						tableFeaturesRs.getDouble("c") / (rows * cols)));
			else
				addFeature(new Feature(id++, "emptyCellPercent", 0.0));
		}
		tableFeaturePS.close();
		tableFeaturesRs.close();

		tableFeaturePS = conn
				.prepareStatement("SELECT wt.length s , ah.length l "
						+ "FROM table_attrs ta , articleHtml ah , page p , wikiarticles wa , wikitables wt "
						+ "WHERE ta.wikitable_id = ? AND wt.table_id = ta.wikitable_id "
						+ "AND ta.pg_id = wa.pg_id AND wa.title = p.page_title AND p.page_namespace = 0 "
						+ "AND p.page_id = ah.id ORDER BY ta.numDataRows DESC LIMIT 1");
		tableFeaturePS.setInt(1, tableId);
		tableFeaturesRs = tableFeaturePS.executeQuery();
		while (tableFeaturesRs.next()) {
			addFeature(new Feature(id++, "tablePageFraction",
					tableFeaturesRs.getDouble("s")
							/ tableFeaturesRs.getDouble("l")));
		}
		tableFeaturePS.close();
		tableFeaturesRs.close();

	}

	public void addFeature(Feature f) {
		features.add(f);
	}

	public ArrayList<Feature> getFeatures() {
		return features;
	}

	public void setFeatures(ArrayList<Feature> features) {
		this.features = features;
	}

}
