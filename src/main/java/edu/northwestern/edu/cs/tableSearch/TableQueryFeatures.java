package edu.northwestern.edu.cs.tableSearch;

import java.io.UnsupportedEncodingException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.tartarus.snowball.ext.porterStemmer;

import weka.core.stemmers.SnowballStemmer;
import weka.core.stemmers.Stemming;

import edu.northwestern.cs.WIG.lib.WIGArrayUtils;

public class TableQueryFeatures {

	ArrayList<Feature> features;

	public TableQueryFeatures(Connection conn, String query, Integer tableId)
			throws SQLException, UnsupportedEncodingException {
		features = new ArrayList<Feature>();

		PreparedStatement tqFeaturePS = conn
				.prepareStatement("SELECT wa.title p , wt.title s , ta.caption c "
						+ "FROM table_attrs ta , wikiarticles wa , wikitables wt "
						+ "WHERE ta.wikitable_id = ? AND wa.pg_id = ta.pg_id "
						+ "AND wt.table_id = ta.wikitable_id ;");
		tqFeaturePS.setInt(1, tableId);
		String pageTitle = "";
		String sectionTitle = "";
		String tableCaption = "";

		ResultSet tqFeatureRS = tqFeaturePS.executeQuery();
		while (tqFeatureRS.next()) {
			pageTitle = new String(tqFeatureRS.getBytes("p"), "UTF-8");
			sectionTitle = new String(tqFeatureRS.getBytes("s"), "UTF-8");
			tableCaption = new String(tqFeatureRS.getBytes("c"), "UTF-8");
			break;
		}
		tqFeaturePS.close();
		tqFeatureRS.close();

		PreparedStatement headerPS = conn
				.prepareStatement("SELECT GROUP_CONCAT(cd.text SEPARATOR ' ') h FROM "
						+ "table_attrs ta , table_headers th , cell_details cd "
						+ "WHERE ta.pg_id = th.pg_id AND ta.id = th.table_id "
						+ "AND th.cellID = cd.cellID AND ta.wikitable_id = ? "
						+ "GROUP BY th.pg_id , th.table_id ;");
		headerPS.setInt(1, tableId);
		ResultSet headerRS = headerPS.executeQuery();
		String headerStr = "";
		while (headerRS.next()) {
			headerStr = new String(headerRS.getBytes("h"), "UTF-8");
		}

		headerStr = headerStr.toLowerCase();

		Double headersContainQuery = WIGArrayUtils.intersectionSize(
				splitNStem(headerStr, " "), splitNStem(query, " ")) > 0 ? 1.0
				: 0.0;

		Double pageContainsQuery = (double) WIGArrayUtils.intersectionSize(
				splitNStem(pageTitle, "_"), splitNStem(query, " "))
				/ (double) query.split(" ").length;
		Double sectionTitleContainsQuery = (double) WIGArrayUtils
				.intersectionSize(splitNStem(sectionTitle, " "),
						splitNStem(query, " "))
				/ (double) query.split(" ").length;
		Double tableCaptionContainsQuery = (double) WIGArrayUtils
				.intersectionSize(splitNStem(tableCaption, " "),
						splitNStem(query, " "))
				/ (double) query.split(" ").length;

		addFeature(new Feature(1, "pageContainsQuery", pageContainsQuery));
		addFeature(new Feature(2, "sectionTitleContainsQuery",
				sectionTitleContainsQuery));
		addFeature(new Feature(3, "headersContainQuery", headersContainQuery));
		addFeature(new Feature(4, "tableCaptionContainsQuery",
				tableCaptionContainsQuery));

		Double titleStartsWithListOf = pageTitle.toLowerCase().startsWith(
				"list_of_") ? 1.0 : 0.0;
		addFeature(new Feature(5, "titleStartsWithListOf",
				titleStartsWithListOf));

	}

	private String[] splitNStem(String str, String splitStr) {
		str = str.toLowerCase().replaceAll("\\+", " ").replaceAll("-", " ")
				.trim();
		String parts[] = str.split(splitStr);
		String[] stemmed = new String[parts.length];

		porterStemmer p = new porterStemmer();
		for (int i = 0; i < parts.length; i++) {
			p.setCurrent(parts[i]);
			p.stem();
			stemmed[i] = p.getCurrent();
		}

		return stemmed;
	}

	public void addFeature(Feature f) {
		features.add(f);
	}

	public ArrayList<Feature> getFeatures() {
		return features;
	}

	public void setFeatures(ArrayList<Feature> features) {
		this.features = features;
	}

}
