package edu.northwestern.edu.cs.tableSearch;

import java.io.UnsupportedEncodingException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;

public class TableSearchFeatures {

	TableFeatures tf;
	TableQueryFeatures tqf;
	QueryFeatures qf = new QueryFeatures();
	Integer origRank;

	public TableFeatures getTableFeatures() {
		return tf;
	}

	public void setTableFeatures(TableFeatures tf) {
		this.tf = tf;
	}

	public void setTableFeatures(Connection conn, Integer tableId)
			throws SQLException {
		this.tf = new TableFeatures(conn, tableId);
	}

	public TableQueryFeatures getTableQueryFeatures() {
		return tqf;
	}

	public void setTableQueryFeatures(TableQueryFeatures tqf) {
		this.tqf = tqf;
	}

	public void setTableQueryFeatures(Connection conn, String query,
			Integer tableId) throws SQLException, UnsupportedEncodingException {
		this.tqf = new TableQueryFeatures(conn, query, tableId);
	}

	public QueryFeatures getQueryFeatures() {
		return qf;
	}

	public void setQueryFeatures(QueryFeatures qf) {
		this.qf = qf;
	}

	public String getFeatureString(HashSet<String> rankerFeature) {

		int featureNumber = 1;
		String featureTestStr = "";
		String featureString = "";
		Double rf = 1.0 / origRank;
		featureString += featureNumber + ":" + rf + " ";
		featureTestStr += "rankImportance " + rf;
		featureNumber++;
		ArrayList<Feature> features;

		features = getTableFeatures().getFeatures();
		for (int i = 0; i < features.size(); i++) {
			if (rankerFeature.contains(features.get(i).getKey())) {
				featureString += featureNumber + ":"
						+ features.get(i).getValue() + " ";
				featureTestStr += features.get(i).getKey() + " - "
						+ features.get(i).getValue();
				featureNumber++;
			}
		}

		features = getTableQueryFeatures().getFeatures();
		for (int i = 0; i < features.size(); i++) {
			if (rankerFeature.contains(features.get(i).getKey())) {
				featureString += featureNumber + ":"
						+ features.get(i).getValue() + " ";
				featureTestStr += features.get(i).getKey() + " - "
						+ features.get(i).getValue();
				featureNumber++;
			}
		}


		return featureString;
	}

	public Integer getOrigRank() {
		return origRank;
	}

	public void setOrigRank(Integer origRank) {
		this.origRank = origRank;
	}

}
