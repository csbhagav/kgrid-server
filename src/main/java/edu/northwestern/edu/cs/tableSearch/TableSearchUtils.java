package edu.northwestern.edu.cs.tableSearch;

import java.io.UnsupportedEncodingException;
import java.sql.Connection;
import java.sql.SQLException;

public class TableSearchUtils {

	public static TableSearchFeatures generateFeatures(Connection conn,
			String query, Integer tableID, Integer origRank)
			throws SQLException, UnsupportedEncodingException {

		TableSearchFeatures tsf = new TableSearchFeatures();
		tsf.setTableFeatures(conn, tableID);
		tsf.setTableQueryFeatures(conn, query.replaceAll("\\+", " ").trim(),
				tableID);
		tsf.setOrigRank(origRank);
		return tsf;
	}

}
